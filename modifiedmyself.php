<?php
    session_start();
    //包含需求檔案 ------------------------------------------------------------------------
    include("./bcontroller/class/common_lite.php");
    //宣告變數 ----------------------------------------------------------------------------
    $ODb = new run_db("mysql",3306);      //建立資料庫物件
    $online_people_num = $ODb->get_online_num();

    // 已送出表單
    if(isset($_POST['type']) && $_POST['type']='modified'){

        // 確認是否輸入正確
        if($_POST['change1']!=$_POST['change2']){
            header("Content-type:text/html; charset=utf-8");
		    echo   "<script charset='UTF-8'>alert('密碼輸入不正確,請重新輸入!!');history.go(-1)</script>";
        	exit;
        }else{
            $passwd = $_POST['change1'];
            // 更新密碼
            $datetime =  date("Y-m-d H:i",time());
            $sql = "UPDATE `front_member` SET ";
            $sql .= "`c_pw`='".base64_encode($passwd)."',";
            $sql .= "`up_date`='".$datetime."'";
            $sql .= "WHERE `num`='".$_SESSION['user_id']."'";

            $res=$ODb->query($sql) or die("更新資料出錯，請聯繫管理員。");

            header("Content-type:text/html; charset=utf-8");
            echo   "<script charset='UTF-8'>alert('已成功修改密碼!!')</script>";
            header("Refresh: 0; url=index.php");       
            exit;    
        }
        
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include 'template/header.inc'; ?>
    </head>
    <body>
        <div id="wrapper">
            <div id="topbar">    
                <?php include 'template/counter.php'; ?>
            </div>
            <div id="container">
                <?php include 'template/sidebar.php'; ?>

                <div id="main">
                    <?php include 'template/nav.php'?>

                    <section>
                        <div class="info">
                            <div id="path">首頁 > <a href="">修改密碼</a>

                                <div class="infor">
                                    <form id="modifypsd" method="post" enctype="multipart/form-data" >
                                        <ul>
                                            <li> * 親愛的用戶您好，您可以修改您的密碼。
                                                <ol>
                                                    <li><label>輸入新密碼</label><input name="change1" type="password"></li>
                                                    <li><label>確認新密碼</label><input name="change2" type="password"></li>
                                                    <input name="type" type="hidden" value="modified">
                                                </ol>
                                            </li>
                                            <li>
                                                <div class="buttonbox">
                                                    <button type="submit" class="top">送出</button>　
                                                    <button type="reset">重填</button>
                                                </div>
                                            </li>
                                        </ul>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>

            <?php include 'template/footer.php'; ?>
        </div>
    </body>
</html>