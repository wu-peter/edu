<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-status-bar-style" content="blank" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge，chrome=1">
<meta charset="utf-8" />
<title>教育部全國大專教師人才網</title>

<meta name="description" content="教育部全國大專教師人才網">
<meta name="keywords" content="教育部,大專教師,人才網,找工作">
<meta name="author" content="Taiwan Cloud-www.taiwan-cloud.com">


<!-- css -->
<link rel="stylesheet" type="text/css" href="css/StyleSheet.css">
<!-- css end -->


<script type="text/javascript" src="js/fade.js"></script>


<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="js/html5-ie8.js"></script>
<!-- html5 for ie8-->


<!-- jquery -->
<link rel="stylesheet" href="css/jquery-ui.css">
<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery-ui.js"></script>

<!-- datepicker end -->
<script>
    $(function() {
      if($(".datepicker").size()>0){
        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true
        });
      }
    });
</script>
<!-- datepicker end -->

<!-- address -->
<script type="text/javascript" src="js/aj-address.js"></script>
<script type="text/javascript">
  $(function() {
    if($('.address-zone').size()>0){
      $('.address-zone').ajaddress();
    }
  });
</script>
<!-- address end -->


<!-- accordion -->
  <script>
  $(function() {
    if($( "#accordion" ).size()>0){
      $( "#accordion" ).accordion({
        heightStyle: "content"
      });
    }
  });
  </script>
 <!-- accordion end -->
<!-- stupidtable -->
<script type="text/javascript" src="js/stupidtable.js"></script>
  <script>
  $(function() {
    if($( "#sampleTable" ).size()>0){
      $('#sampleTable').stupidtable();      
    }
  });
  </script>
 <!-- stupidtable end -->
 
<!-- jquery end -->