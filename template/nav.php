<nav>
    <ul>
        <li><a href="index.php"><img src="images/nav_home.png" alt="回首頁" /></a></li>
        <li><a href="contact.php"><img src="images/nav_contact.png" alt="聯絡我們" /></a></li>
        <li><a href="jobshow.php"><img src="images/nav_infor.png" alt="職缺訊息" /></a></li>
        <li><a href="faq.php"><img src="images/nav_faq.png" alt="網站Q&A" /></a></li>
        <li><a href="visitorrpt.php"><img src="images/nav_count.png" alt="訪客流覽統計" /></a></li>
        <li><a href="explanation.php"><img src="images/nav_directions.png" alt="留言版" /></a></li>
        <li><a href="manual.php"><img src="images/nav_manual.png" alt="操作手冊" /></a></li>
    </ul>
</nav>
