<?php
	session_start();
	
	$times = 0;    
	$sortv="⇅";
//这里要注意，因为要“真正的"改变$sql的值，所以用引用传值  
function bindParam(&$sql, $location, $var, $type) {    
    global $times;    
    //确定类型    
    switch ($type) {    
       //字符串    
       default:                    //默认使用字符串类型    
      case 'STRING' :    
           $var = addslashes($var);  //转义    
           $var = "'".$var."'";      //加上单引号.SQL语句中字符串插入必须加单引号    
           break;    
       case 'INTEGER' :    
       case 'INT' :    
           $var = (int)$var;         //强制转换成int    
       //还可以增加更多类型..    
   }    
  //寻找问号的位置    
   for ($i=1, $pos = 0; $i<= $location; $i++) {    
       $pos = strpos($sql, '?', $pos+1);    
   }    
   //替换问号    
   $sql = substr($sql, 0, $pos) . $var . substr($sql, $pos + 1);   
}    

//包含需求檔案 ------------------------------------------------------------------------
	include("./bcontroller/class/common_lite.php");
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	$online_people_num = $ODb->get_online_num();
 //mod by 志豪 20160721 不知為何PHP要轉double型態就會卡住 因此先做is_integet的判斷 
	$is_integer = @$_GET['pg'];
	$is_integer_check = $is_integer + 0;
	if(isset($_GET['pg'])&& is_integer($is_integer_check)){					
		if($_GET['pg']>0&&$_GET['pg']<1000){
			$page=$_GET['pg'];
			$page = htmlspecialchars($page);
		}else{
			$_GET['pg']=1;
			$page=1;				
		}
	}else{
		$_GET['pg']=1;
		$page=1;	
	}
	
	
	$selectnum=($page-1)*12;
	
	
	
	//取出學校
	$sql_dsc = "SELECT * FROM  `school_data` order by `num` ";
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res)){
		$school_data_array[$row['num']] = $row['school_name'];
		$school_location_array[$row['num']] = $row['tw_counties_num'];
	}
	
	
	//取出系所
	$sql_dsc = "SELECT * FROM  `back_group_data` order by `num` ";
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res)){
		$college_depart_array[$row['num']] = $row['c_name'];
	}	
	
	$where_dsc = '';
	if(isset($_GET['gtc']) && $_GET['gtc'] !=''){
		$_GET['gtc'] = htmlspecialchars($_GET['gtc']);
		$where_dsc = " and `post_data`.`tw_counties_num`=?";
	 	bindParam($where_dsc, 1, $_GET['gtc'], 'STRING');	
	}

	if(isset($_GET['stadate']) && $_GET['stadate'] !='') {
		$sta = base64_decode($_GET['stadate']);
		$stadate = date('Y-m-d',strtotime($sta));
		
		if($_GET['enddate'] !='') {
			$end = base64_decode($_GET['enddate']);
			$enddate = date('Y-m-d',strtotime($end));
			$where_dsc .= " and (`c_postdate` BETWEEN '".$stadate."' AND '".$enddate."'";
			$where_dsc .= " or `c_enddate` BETWEEN '".$stadate."' AND '".$enddate."')";
		}else {
			$where_dsc .=" and (`c_postdate`>= '".$stadate."'";
			$where_dsc .=" or `c_enddate`>='".$stadate."')";
		}
	}else if(isset($_GET['enddate']) && $_GET['enddate'] !=''){
		$end = base64_decode($_GET['enddate']);
		$enddate = date('Y-m-d',strtotime($end));
		$where_dsc .= " and `c_postdate`<='".$enddate."'";
		$where_dsc .= " or `c_enddate` <='".$enddate."')";
	}
	
	if(isset($_GET['unit']) && $_GET['unit'] != '') { 
		$unit = rawurldecode(urlencode(urldecode($_GET['unit'])));		
		$unit = base64_decode($unit);
		$where_dsc .= " and (`school_depart_num` LIKE '%".$unit."%'";
		$where_dsc .= " or `school_name` LIKE '%".$unit."%'";
		$where_dsc .= " or `back_group_data`.`c_name` LIKE '%".$unit."%' )";

	}

	if(isset($_GET['title']) && $_GET['title'] != '') { 
		$title = rawurldecode(urlencode(urldecode($_GET['title'])));		
		$title = base64_decode($title);
		$where_dsc .= " and (`c_title` LIKE '%".$title."%'";
		$where_dsc .= " or `school_depart_num` LIKE '%".$title."%' )";
	}
	
	$nowdate =  date("Y-m-d",time());
	//取出資料	
	$sql_dsc = "SELECT * FROM `post_data` 
	LEFT JOIN `school_data` ON `post_data`.`c_school_num` = `school_data`.`num` 
	LEFT JOIN `back_member` ON `post_data`.`b_member_num` = `back_member`.`num` 
	LEFT JOIN `back_group_data` ON `back_member`.`c_login_name` = `back_group_data`.`c_class_id`";
	$sql_dsc .= " where `c_enddate`>= ?".$where_dsc;
	//$sql_dsc .= " Group by `post_data`.`num`";
	bindParam($sql_dsc, 1, $nowdate, 'STRING');

	$resjob=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	$total_num=mysql_num_rows($resjob);
	$show_data_num = 10;//一頁顯示多少筆資料
	if($page== '1' ){
		$now_pg = 1;
		$pg_dsc = ' limit '.$show_data_num;
	}else{
		$page = htmlspecialchars($page);
		$now_pg = $page;
		$pg_dsc = ' limit '.(($page-1) * $show_data_num) .','.$show_data_num ;
	}
	
	// 20190311 修改以報名截止日期排序(a.`c_enddate`)
	//20190603 修正撈取職缺num值以避免資料抓錯
        //20190606 排序改以新增日期排序
	$sql_dsc = "SELECT *,`post_data`.`num`as postNum FROM `post_data` 
	LEFT JOIN `school_data` ON `post_data`.`c_school_num` = `school_data`.`num` 
	LEFT JOIN `back_member` ON `post_data`.`b_member_num` = `back_member`.`num` 
	LEFT JOIN `back_group_data` ON `back_member`.`c_login_name` = `back_group_data`.`c_class_id`";
	$sql_dsc .= "where `c_enddate`>=?".$where_dsc."GROUP BY `post_data`.`num` order by `post_data`.`c_postdate` desc ".$pg_dsc;

	//$sql_dsc = "SELECT a.`num`,a.`c_title`,a.`c_dsc`,a.`c_postdate`,a.`tw_counties_num`,a.`location`,a.`c_enddate`,a.`b_member_num`,
	//a.`c_school_num`,a.`b_member_group_num`,a.`up_date`,a.`school_depart_num` 
	//FROM `post_data` as a LEFT JOIN `school_data` as b ON a.`c_school_num` = b.`num` 
	//LEFT JOIN `back_member` as c ON a.`b_member_num` = c.`num` LEFT JOIN `back_group_data` as d ON c.`c_login_name` = d.`c_class_id` ";
	//$sql_dsc .= "where a.`c_enddate`>=?".$where_dsc." order by `post_data`.`num` desc ".$pg_dsc." GROUP BY a.`num` ";
	bindParam($sql_dsc, 1, $nowdate, 'STRING');
//die($sql_dsc);
	$resjob=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	
	// while($row = mysql_fetch_array($resjob)){
	while($row= mysql_fetch_assoc($resjob)){
		$sql_array['num']               = $row['postNum'];
		$sql_array['c_title']           = $row['c_title'];
		$sql_array['c_dsc']		        = $row['c_dsc'];
		$sql_array['c_postdate']        = $row['c_postdate']; 
		$sql_array['tw_counties_num']   = $row['tw_counties_num']; 
		$sql_array['location']          = $row['location']; 
		$sql_array['c_enddate']	        = $row['c_enddate'];
		$sql_array['b_member_num']       = $row['b_member_num'];
		$sql_array['c_school_num']      = $row['c_school_num'];
		$sql_array['b_member_group_num']= $row['b_member_group_num'];
		$sql_array['up_date']           = $row['up_date']	;
		$sql_array['school_depart_num'] = $row['school_depart_num'];

		$data_array[] = $sql_array;
	}
	//print_r($data_array);exit;
	//取出縣市
	$sql_dsc = "SELECT * FROM  `tw_counties` order by `num`";
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res)){
		$tw_counties_array[$row['num']] = $row['counties_name'];
	}
	
	//back_member編輯者資料
	$sql_dsc = "select * from `back_member`";
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res)){
		$back_member[$row['num']] = $row['c_group_num'];
	}	
	//計算總頁數
	$total_pg = 0;
	$sql_dsc = "SELECT count(*) as `get_num` FROM `post_data` 
	LEFT JOIN `school_data` ON `post_data`.`c_school_num` = `school_data`.`num` 
	LEFT JOIN `back_member` ON `post_data`.`b_member_num` = `back_member`.`num` 
	LEFT JOIN `back_group_data` ON `back_member`.`c_login_name` = `back_group_data`.`c_class_id`";
	$sql_dsc .= " where `c_enddate`>=?".$where_dsc;
	bindParam($sql_dsc, 1, $nowdate, 'STRING');	
	$restotal=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($restotal)){
	
	}

	//$totalnews=mysql_num_rows($restotal);
  	$totalnews=$total_num;
	$totalpage=$totalnews/$show_data_num;
	if($totalpage<1){$totalpage=1;}else{
	  $totalpage=ceil($totalpage);
  	}
  //mod by 志豪 20160721
  if($page>$totalpage){
	$page=$totalpage;
  }
	
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include 'template/header.inc'; ?>
		<script type="text/javascript" src="js/jquery.base64.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(
				function (){
				$( "#box_floor" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
				$( "#box_floor li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );

				if($(".nano").size()>0){if($(".nano").size()>0){$(".nano").nanoScroller();}}
			});
			
			function MM_jumpMenu(targ,selObj,restore){ //v3.0
				eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
				if (restore) selObj.selectedIndex=0;
			}

			function sw(){	
				
				var link = "jobshow.php?pg=1";
				if($('#tw_counties_num').val() != '') {
					
					link = link + "&gtc="+$('#tw_counties_num').val();
				}
				
				if($('#c_startdate').val() != '') {
					var stadate = $.trim($('#c_startdate').val());
					var stadate1 = encodeBase64(stadate);
					link = link + "&stadate="+stadate1;
				}
				if($('#c_enddate').val() != '') {
					var enddate = $.trim($('#c_enddate').val());
					var enddate1 = encodeBase64(enddate);
					link = link + "&enddate="+enddate1;
				}
				
				if($('#c_unit').val() != '') {
					var unit = $.trim($('#c_unit').val());
					var unit1 = encodeBase64(unit);
					link = link + "&unit="+unit1;
				}

				if($('#c_title').val() != '') {
					var title = $.trim($('#c_title').val());
					var title1 = encodeBase64(title);
					link = link + "&title="+title1;
				}

				location.replace(link);
			}

			//加密方法。没有过滤首尾空格，即没有trim.
			function encodeBase64(mingwen, times) {
				var code = "";
				var num = 1;
				if (typeof times == 'undefined' || times == null || times == "") {
					num = 1;
				} else {
					var vt = times + "";
					num = parseInt(vt);
				}

				if (typeof mingwen == 'undefined' || mingwen == null || mingwen == "") {

				} else {
					code = mingwen;
					for (var i = 0; i < num; i++) {
						code = $.base64.encode(code);
					}
				}
				return code;
			}
			//解密方法。没有过滤首尾空格，即没有trim
			//加密可以加密N次，对应解密N次就可以获取明文
			function decodeBase64(mi,times){

				var mingwen="";

				var num=1;

				if(typeof times=='undefined'||times==null||times==""){

					num=1;

				}else{

					var vt=times+"";

					num=parseInt(vt);

				}

				if(typeof mi=='undefined'||mi==null||mi==""){

				}else{

					$.base64.utf8encode = true;

					mingwen=mi;

					for(var i=0;i<num;i++){

						mingwen=$.base64.atob(mingwen);
					}
				}
				return mingwen;
			}

			function formReset() {
				// document.getElementById("form1").reset()
				$(':input') .val('');  //将input元素的value设为空值
			}

		</script>

		<script>
			function job(number){
				$('#jobth1').empty().append('公告單位');
				$('#jobth2').empty().append('用人單位');
				$('#jobth3').empty().append('公告主旨');
				$('#jobth4').empty().append('工作地點');
				$('#jobth5').empty().append('職缺公告日期');
				$('#jobth6').empty().append('報名截止日期');
				switch(number){
					case '1' :	
						if($("#jobth1").hasClass('sorting-asc'))  
						$('#jobth1').empty().append('公告單位▼');
						else 
						$('#jobth1').empty().append('公告單位▲');
						break;
					case '2' :	
						if($("#jobth2").hasClass('sorting-asc'))  
						$('#jobth2').empty().append('用人單位▼');
						else 
						$('#jobth2').empty().append('用人單位▲');
						break;
					case '3' :	
						if($("#jobth3").hasClass('sorting-asc'))  
						$('#jobth3').empty().append('公告主旨▼');
						else 
						$('#jobth3').empty().append('公告主旨▲');
						break;
					case '4' :	
						if($("#jobth4").hasClass('sorting-asc'))  
						$('#jobth4').empty().append('工作地點▼');
						else 
						$('#jobth4').empty().append('工作地點▲');
						break;
					case '5' :	
						if($("#jobth5").hasClass('sorting-asc'))  
						$('#jobth5').empty().append('職缺公告日期▼');
						else 
						$('#jobth5').empty().append('職缺公告日期▲');
						break;
					case '6' :	
						if($("#jobth6").hasClass('sorting-asc'))  
						$('#jobth6').empty().append('報名截止日期▼');
						else 
						$('#jobth6').empty().append('報名截止日期▲');
						break;}}
		</script>
				<!--mod by PETER 20200310 排序圖示化-->
</head>
    <body>

        <div id="wrapper">

			<div id="topbar">
				
				<?php include 'template/counter.php'; ?>

			</div><!-- topbar end -->

        
            <div id="container">

                <?php include 'template/sidebar.php'; ?>

                <div id="main">
                    <?php include 'template/nav.php'?>

					<style>
						#search {
							padding: 5px 0;
							margin: 10px 0 3px 0;
							border-top: 3px double #e6e6e6;
							border-bottom: 3px double #e6e6e6;
							background: #f0f0f0;
							
						}
						#search ul li {
							display:inline;
							padding: 5px;
							line-height: 15px;
						}
						#search button {
							padding: 5px 10px;
						}
						#search button {
							background: #b7b5b5;
						}
						#search button.top {
							background: #b82629;
							color: white;
						}
						
					</style>
					<section>
						<div class="info">
							<div class="title">
								<img src="images/jobshow_title.png" alt="職缺訊息標題" class="news-title">
							</div><!-- title end -->
							<div id="path">首頁 > <a href="">職缺訊息</a><!--<span><a onclick="history.back()" class="button btnback"><i class="fa fa-reply"></i>　BACK 回上一頁</a></span>--></div>
							<div id="search">
								<form id="form1" method="post" enctype="multipart/form-data">
									<ul>
										<li><label>日期區間</label>
											<input type="text" class="datepicker" name="c_startdate" id="c_startdate" value="<?php if(isset($_GET['stadate'])) {echo base64_decode($_GET['stadate']);}?>">　~　
											<input type="text" class="datepicker" name="c_enddate" id="c_enddate" value="<?php if(isset($_GET['enddate'])) {echo base64_decode($_GET['enddate']);}?>">
											
										</li>
										<br>
										<li><label>用人單位</label>
											<input type="text" class="subject" name="c_unit"  id="c_unit" value="<?php if(isset($_GET['unit'])) {echo base64_decode(rawurldecode(urlencode(urldecode($_GET['unit']))));}?>">
										</li>
										<li><label>公告主旨</label>
											<input type="text" class="subject" name="c_title"  id="c_title" value="<?php if(isset($_GET['title'])) {echo base64_decode(rawurldecode(urlencode(urldecode($_GET['title']))));}?>">
										</li>
										<li><label>工作地點</label>
											<select name="tw_counties_num" id="tw_counties_num">
												<option value=""></option>
												<?php 
													if(is_array($tw_counties_array)){
															foreach($tw_counties_array as $key => $value){		
																if($_GET['gtc'] == $key){
																	echo '<option value="'.$key.'" selected>'.$value.'</option>';
																}else{										
																	echo '<option value="'.$key.'">'.$value.'</option>';
																}
															
															}
													}
												?>						
											</select>
										</li>
										<li style="margin-left:-10px">
											<button type="button" value="reset" onclick="formReset()">清空</button>
											<button type="button" class="top" onclick="sw()">搜尋</button>
										</li>
										<!-- <div style="float:right">
											
											<img src="images/btn_search.jpg" alt="搜尋按鈕" onclick="sw()" >
										</div> -->
									</ul>
								</form>
								
							</div><!-- search end -->
							
							<div class="list">
								<p style="color:red; text-align:end;">*點擊標題列可以進行排序</p>
								<table cellspacing="2" id="sampleTable">
									<thead>
										<tr class="list-title" >
											<th width="120" data-sort="string" onclick="job('1')" id="jobth1">公告單位⇅</a></th>
											<th width="80" data-sort="string" onclick="job('2')" id="jobth2">用人單位⇅</a></th>
											<th width="187" data-sort="string" onclick="job('3')" id="jobth3">公告主旨⇅</a></th>
											<th width="50" data-sort="string" onclick="job('4')" id="jobth4">工作地點⇅</a></th>
											<th width="60" data-sort="string" onclick="job('5')" id="jobth5">職缺公告日期▲</th>
											<th width="60" data-sort="string" onclick="job('6')" id="jobth6">報名截止日期⇅</th>
											<th width="30" data-sort="string">瀏覽</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											if(is_array($data_array)){	
											foreach($data_array as $my_data){	
										?>
										<tr>
											<td><?php echo $school_data_array[$my_data['c_school_num']].$college_depart_array[$back_member[$my_data['b_member_num']]];?></td>
											<td><?php 
													//如果用人系所有值
													if($my_data['school_depart_num'] !=''){
														echo $my_data['school_depart_num'];
													}else{
														//如果用人系所沒有值
														echo $school_data_array[$my_data['c_school_num']].$college_depart_array[$my_data['b_member_group_num']];
													}										
												?>
											</td>
											<td class="left"><?php echo $my_data['c_title']; ?> </td>
											<td><?php echo $tw_counties_array[$my_data['tw_counties_num']].$my_data['location'];?></td>
											<td><font size="1px"><?php echo $my_data['c_postdate']; ?></font></td>
											<td><font size="1px"><?php echo $my_data['c_enddate']; ?></font></td>
											<td><a href="jobshow-detail.php?num=<?php echo $my_data['num']; ?>"><img src="images/btn_view.png" alt="觀看詳細"></a></td>
										</tr>
										<?php 
											}
										}else{	?>
										<td colspan="7" align="center">無資料！！</td>
										<?php } ?>
									</tbody>
								</table>
							</div>
							<!-- 分頁機制改寫 20180125志誠 s -->
							<div id="pages">目前頁數：<?php echo $page;?>/<?php echo $totalpage;?>頁　　總筆數：<?php echo $totalnews;?>筆　
								<a href="jobshow.php?pg=1<?php if(isset($_GET['k'])&&$_GET['k']!=''){echo "&k=".$_GET['k'];}?>" class="btn"><i class="fa fa-angle-left"></i><i class="fa fa-angle-left"></i>　最新</a>　

								<?php if(($page-2)>0){?>
									<a href="jobshow.php?pg=<?php echo ($page-2);?><?php if(isset($_GET['k'])&&$_GET['k']!=''){echo "&k=".$_GET['k'];}?>" class="opt"><?php echo ($page-2);?></a>　	
								<?php }?>
								<?php if(($page-1)>0){?>
									<a href="jobshow.php?pg=<?php echo ($page-1);?><?php if(isset($_GET['k'])&&$_GET['k']!=''){echo "&k=".$_GET['k'];}?>" class="opt"><?php echo ($page-1);?></a>　	
								<?php }?>

								<select name="jumpMenu" id="jumpMenu" onchange="MM_jumpMenu('parent',this,0)">
									<option value="jobshow.php?pg=<?php echo $page;?><?php if(isset($_GET['k'])&&$_GET['k']!=''){echo "&k=".$_GET['k'];}?>"><?php echo $page;?></option>
									<?php for($x=1;$x<=$totalpage;$x++){
											if($x!=$page){
									?>
										<option value="jobshow.php?pg=<?php echo $x;?><?php if(isset($_GET['k'])&&$_GET['k']!=''){echo "&k=".$_GET['k'];}?>"><?php echo $x;?></option>
									<?php }}?>
								</select> 
								&nbsp;&nbsp;

								<?php if(($page+1)<=$totalpage){?>
									<a href="jobshow.php?pg=<?php echo ($page+1);?><?php if(isset($_GET['k'])&&$_GET['k']!=''){echo "&k=".$_GET['k'];}?>" class="opt"><?php echo ($page+1);?></a>　	
								<?php }?>

								<?php if(($page+2)<=$totalpage){?>
									<a href="jobshow.php?pg=<?php echo ($page+2);?><?php if(isset($_GET['k'])&&$_GET['k']!=''){echo "&k=".$_GET['k'];}?>" class="opt"><?php echo ($page+2);?></a>　	
								<?php }?>
									
								<a href="jobshow.php?pg=<?php echo $totalpage;?><?php if(isset($_GET['k'])&&$_GET['k']!=''){echo "&k=".$_GET['k'];}?>" class="btn">最後　<i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
							</div>
							<!-- 分頁機制改寫 20180125志誠 e -->
						</div><!-- info end -->
					</section><!-- section end -->
				</div>

            </div><!-- container end -->

            <?php include 'template/footer.php'; ?>

        </div><!-- wrapper ebd -->

    </body>
</html>
