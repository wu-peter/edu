<?php
	session_start();
	if($_SESSION['login_ok']!=1){
	header("Location: index.php");
	}
//包含需求檔案 ------------------------------------------------------------------------
	include("./bcontroller/class/common_lite.php");
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	$online_people_num = $ODb->get_online_num();
	

	
	//取出縣市
	$sql_dsc = "SELECT * FROM  `tw_counties` order by `num` ";
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res))
	{
		$tw_counties_array[$row['num']] = $row['counties_name'];
	}
	//取出學術專長資料
	$sql_dsc = "SELECT * FROM  `code_table` where MainCode='Academic_expertise' order by SubCode";
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res))
	{
		$Academic_expertise_array['MainCode'] = $row['MainCode'];
		$Academic_expertise_array['SubCode'] = $row['SubCode'];
		$Academic_expertise_array['SubDesc'] = $row['SubDesc'];
		$Academic_expertise_type_array[] = $Academic_expertise_array;
		
	}
	
	//取出個人資料
	
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'template/header.inc'; ?>
<script type="text/javascript">
function chk(){
	if($('#email').val().length >=5 && $('#email').val().match(/^[0-9a-zA-Z]([-._]*[0-9a-zA-Z])*@[0-9a-zA-Z]([-._]*[0-9a-zA-Z])*\.+[a-zA-Z]+$/)){
	//判斷Email長度及符合Email格式，必須符合 xxx@xxxx.xxx 並且不包括其他特殊字元
	$('#form1').submit();
	}else{
	alert('email輸入錯誤,請重新輸入');
	}	

}
function cle(X) {
	document.getElementById(X).value="";

}

</script>
</head>
    <body>

        <div id="wrapper">

        <div id="topbar">
            
            <?php include 'template/counter.php'; ?>

        </div><!-- topbar end -->

        
            <div id="container">
                <?php include 'template/sidebar.php'; ?>
                <div id="main">
				<?php include 'template/nav.php'?>
                <section>
                    <div id="path">首頁 > <a href="">職缺訊息</a> > 職缺通知設定<span><a onclick="history.back()" class="button btnback"><i class="fa fa-reply"></i>　BACK 回上一頁</a></span></div>
                    <div class="infor">
                        <ul>
                            <li class="center"><i class="fa fa-search"></i> 篩選條件</li>
                            <li>職缺通知篩選設定</li>
							<form action="jobshow-notes-edit.php" method="POST" name="form1" id="form1">
							<?php
							$sql_dsc = "SELECT * FROM  `jobshow_notes` where c_num='".$_SESSION['user_id']."'";
							$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
							$row = mysql_fetch_assoc($res);
							?>
                            <li>
                                <ol>
									<li>	
									<label>通知週期設定</label>
									<div class="address-zone">
										<select name="notes_date" id="notes_date" onchange="Submit4()">
										<option value="1" <?php if($row["notes_date"]=="1"){echo "selected";};?> >一個月</option>
										<option value="3" <?php if($row["notes_date"]=="3"){echo "selected";};?> >三個月</option>
										<option value="6" <?php if($row["notes_date"]=="6"){echo "selected";};?> >半年</option>
										<option value="12" <?php if($row["notes_date"]=="12"){echo "selected";};?> >一年</option>										
										</select>
									</div>
									<div id="dt">
									</div>
									</li>	
                                    <li>
									<label>職缺公告日期區間</label>
									<input type="text" class="datepicker" name="c_postdate_s" id="c_postdate_s" value="<?php if($row["c_postdate_s"]!="1970-01-01"){echo $row["c_postdate_s"];}?>" ><button type="button" style="border-radius:0px;padding:2px 4px 2px 4px;font-size:4px;" onclick="cle('c_postdate_s')">X</button>　~　<input type="text" class="datepicker" name="c_postdate_c" id="c_postdate_c" value="<?php if($row["c_postdate_c"]!="1970-01-01"){echo $row["c_postdate_c"];}?>"><button type="button" style="border-radius:0px;padding:2px 4px 2px 4px;font-size:4px;" onclick="cle('c_postdate_c')">X</button>
									</li>
                                    <li><label>報名截止日期區間</label>
									<input type="text" class="datepicker" name="c_enddate_s" id="c_enddate_s" value="<?php if($row["c_enddate_s"]!="1970-01-01"){echo $row["c_enddate_s"];}?>" ><button type="button" style="border-radius:0px;padding:2px 4px 2px 4px;font-size:4px;" onclick="cle('c_enddate_s')">X</button>　~　<input type="text" class="datepicker" name="c_enddate_c" id="c_enddate_c" value="<?php if($row["c_enddate_c"]!="1970-01-01"){echo $row["c_enddate_c"];}?>" ><button type="button" style="border-radius:0px;padding:2px 4px 2px 4px;font-size:4px;" onclick="cle('c_enddate_c')">X</button>
									</li>
                                </ol>
                                <ol>
							<li>	
							<label>工作地點</label>							
								<select name="tw_counties_num" id="tw_counties_num">
								<option value=""></option>
								<?php 
									if(is_array($tw_counties_array)){
										foreach($tw_counties_array as $key => $value)
										{if ( $row["tw_counties_num"]==$key )
						            		{
						            			echo '<option value="'.$key.'" selected>'.$value.'</option>';
						            		}
						            		else 
						            		{
						            		    echo '<option value="'.$key.'" >'.$value.'</option>';
						            		}							
						            	}
										
									}
								?>						
								</select>
								或	
								<select name="tw_counties_num2" id="tw_counties_num2">
								<option value=""></option>
								<?php 
									if(is_array($tw_counties_array)){
										foreach($tw_counties_array as $key => $value)
										{if ( $row["tw_counties_num2"]==$key )
						            		{
						            			echo '<option value="'.$key.'" selected>'.$value.'</option>';
						            		}
						            		else 
						            		{
						            		    echo '<option value="'.$key.'" >'.$value.'</option>';
						            		}							
						            	}
										
									}
								?>						
								</select>
								<font color="#FF0000">&nbsp;&nbsp;&nbsp;&nbsp;可選兩個適合的工作地點</font >
							</li>						
                            <li>	
							<label>學術專長一</label>
						            <select name="Academic_expertise" id="Academic_expertise" >	
						            <?php
						            	foreach ( $Academic_expertise_type_array as $value ) 
						            	{if ( $row["academic_expertise"]==$value['SubCode'] )
						            		{
						            			echo '<option value="'.$value['SubCode'].'" selected>'.$value['SubDesc'].'</option>';
						            		}
						            		else 
						            		{
						            		    echo '<option value="'.$value['SubCode'].'" >'.$value['SubDesc'].'</option>';
						            		}							
						            	}
						            ?>
							</select>		
							</br></br>	
							<label>學術專長二</label>
						            <select name="Academic_expertise2" id="Academic_expertise2" >	
						            <?php
						            	foreach ( $Academic_expertise_type_array as $value ) 
						            	{if ( $row["academic_expertise2"]==$value['SubCode'] )
						            		{
						            			echo '<option value="'.$value['SubCode'].'" selected>'.$value['SubDesc'].'</option>';
						            		}
						            		else 
						            		{
						            		    echo '<option value="'.$value['SubCode'].'" >'.$value['SubDesc'].'</option>';
						            		}							
						            	}
						            ?>
							</select>
							</br></br><font color="#FF0000">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;可選擇兩個學術專長以利職缺媒合</font >
							</li>
							<li>	
							<label>收件信箱</label>
						            <input type="text"  name="email" id="email" style="width:212px" value="<?php if($row["email"]==""){echo $_SESSION['c_email'];}else{echo $row["email"];}?>" >
									<font color="#FF0000">&nbsp;&nbsp;&nbsp;&nbsp;可輸入常用之電子信箱</font >
							</li>
							<li>
							提示說明：輸入期望之篩選條件後，若系統有新增符合條件的職缺時，將會自動寄出通知信件給您參閱
							</li>
							<?php 
							$date=date("Y-m-d");
							if($date>$row["notes_date_end"]){
								
							?>
							<li class="buttonbox">
									<button type="button" onclick="chk()">條件保存</button>
							</li>
							<?php							
							}else{
							?>
							<li class="buttonbox">
									<button type="button" onclick="chk()">條件修改</button>
							</li>
							<?php 
							}
							?>
							</ol>
                            </li>
							</form>
                        </ul>
                    </div><!-- infor end -->
                </section><!-- section end -->
                    

                </div>
                

            </div><!-- container end -->


            <?php include 'template/footer.php'; ?>



        </div><!-- wrapper ebd -->
		
<script>
	$("#c_postdate_s").datepicker({dateFormat:"yy-mm-dd"});
	$("#c_postdate_c").datepicker({dateFormat:"yy-mm-dd"});
	$("#c_enddate_s").datepicker({dateFormat:"yy-mm-dd"});
	$("#c_enddate_c").datepicker({dateFormat:"yy-mm-dd"});
	
	
    $(document).ready(function () {
    
       
		
    var URLs="jobshow-notes-date.php?ed=<?php echo $row["notes_date_end"];?>&st=<?php echo $row["status"];?>";
            $.ajax({
                url: URLs,
                type:"GET",
                dataType:'html',
				async: true,

                success: function(msg){
                  document.getElementById('dt').innerHTML=msg;
                },

                 error:function(xhr, ajaxOptions, thrownError){ 
                    alert(xhr.status); 
                    alert(thrownError); 
                 }
            });         
        		
    });
	
	var Submit4=function(){
			
			var URLs="jobshow-notes-date.php?st=<?php echo $row["status"];?>&sd="+ document.getElementById('notes_date').value;
            $.ajax({
                url: URLs,
                type:"GET",
                dataType:'html',
				async: true,

                success: function(msg){
                  document.getElementById('dt').innerHTML=msg;
                },

                 error:function(xhr, ajaxOptions, thrownError){ 
                    alert(xhr.status); 
                    alert(thrownError); 
                 }
            });      
		}

   
</script>		

    </body>
</html>
