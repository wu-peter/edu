<?php
	session_start();
//包含需求檔案 ------------------------------------------------------------------------
	include("./bcontroller/class/common_lite.php");
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <?php include 'template/header.inc'; ?>
       
    </head>
    <body>

        <div id="wrapper">

        <div id="topbar">
            
            <?php include 'template/counter.php'; ?>

        </div><!-- topbar end -->

        
            <div id="container">

                <?php include 'template/sidebar.php'; ?>

                <div id="main">
                    <?php include 'template/nav.php'?>


                <section>
                    <div class="info">
                    <div id="path">首頁 > <a href="">法令下載</a> > 瀏覽<span><a onclick="history.back()" class="button btnback"><i class="fa fa-reply"></i>　BACK 回上一頁</a></span></div>
                    <div class="main-title">公告主旨：辦理教師甄試切結期間案<span>刊載日期：2014-01-01</span>
                    </div>
                    <div id="content">
                        <!-- 文字編輯區 -->
                        <p>一、為協助當年度參加教師資格檢定考試通過者報名教師甄試，本部於102年4月15日臺教師(二)字第1020054869號函示（如附件2，略以），應考人於教師證書核發期間參加教師甄試之切結期間以102年7月31日前為限。另為協助已持有教師證者參加教師甄試之機會，本部於95年8月8日臺中(三)字第0950117800B號函示（如附件3）已取得教師證書之教師擬以尚在辦理另一任教學科、領域專長參加教師甄試者，得檢附相關證書暨8月底前能取得教師證書之切結書報名參加教師甄試。</p>
<p>　　二、查102年度部分辦理教師甄試單位，未依本部前函意旨自行訂定教師甄試切結報名資格期限，影響當年度通過教師資格檢定考試者參加教師甄試之權益，並影響本部發給教師證書作業期程。鑑於當年度通過教師資格檢定考試者申請發給第1張教師書，須經由原就讀之師資培育之大學備妥相關修課證明文件後，報請本部委託單位國立臺灣師範大學辦理教師證書審查及製證，並送請本部復核通過始得
取得教師證書；為保障渠等工作權益，請　貴單位及其轄屬學校依本部102年4月15日臺教師(二)字第1020054869號函示辦理教師甄試切結期間得至102年7月31日為限。</p>
<p>　　三、至當年度通過教師資格檢定考試取得教師證書者、應屆畢業修畢另一類科師資職前教育課程及完成教師在職進修第2專長學分班者，均須為7月初始得申請辦理另一類科教師證書、其他任教學科（領域、群科）教師證書；為保障渠等工作權益，建請　貴單位依本部95年8月8日臺中(三)字0950117800B號函示辦理教師甄試切結期間得至102年8月31日為限。</p>
                    </div>
                    <div id="download">檔案下載：
                        <ul>
                            <li><a href=""><i class="fa fa-cloud-download"></i> 教育部102年7月24日臺教師(二)字第1020111072號函</a></li>
                            <li><a href=""><i class="fa fa-cloud-download"></i> 附件1、附件2及附件3</a></li>
                        </ul>
                    </div>
                    <div class="infor">
                        <ul>
                            <li class="center"><i class="fa fa-bookmark"></i> 發佈資訊</li>
                            <li><label>類別：</label>B其他釋例</li>
                            <li><label>公文日期文號：</label>教育部102年7月24日臺教師(二)字第1020111072號函</li>
                            <li><label>頒布單位：</label>教育部</li>
                            <li><label>參考網址：</label><a href="" target="_blank">http://www.com.tw</a></li>
                        </ul>
                    </div><!-- infor end -->
                    </div><!-- info end -->
                </section><!-- section end -->


                </div>
                

            </div><!-- container end -->


            <?php include 'template/footer.php'; ?>



        </div><!-- wrapper ebd -->

    </body>
</html>
