<?php
	session_start();
//包含需求檔案 ------------------------------------------------------------------------
	include("./bcontroller/class/common_lite.php");
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	$online_people_num = $ODb->get_online_num();
	

	
	//取出縣市
	$sql_dsc = "SELECT * FROM  `tw_counties` order by `num` ";
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res))
	{
		$tw_counties_array[$row['num']] = $row['counties_name'];
	}
	//取出學術專長資料
	$sql_dsc = "SELECT * FROM  `code_table` where MainCode='Academic_expertise' order by SubCode";
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res))
	{
		$Academic_expertise_array['MainCode'] = $row['MainCode'];
		$Academic_expertise_array['SubCode'] = $row['SubCode'];
		$Academic_expertise_array['SubDesc'] = $row['SubDesc'];
		$Academic_expertise_type_array[] = $Academic_expertise_array;
		
	}
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'template/header.inc'; ?>
<script language="javascript">
function chk(){
	var obj_array = new Array("c_postdate_s", "c_postdate_c", "c_enddate_s","c_enddate_c","lack","tw_counties_num","Academic_expertise","c_title");
	var val_dsc = "";
	var key_dsc = "";
	for(var x=0;x<obj_array.length;x++){
		key_dsc  += obj_array[x]+"||";	
		val_dsc  += $('#'+obj_array[x]).val()+"||";
	}
	
	var url_dsc = "";
	 $.ajax({
		url: './bcontroller/js_function/value_encode_array.php',
		data: {keyDSC:key_dsc,ValDsc:val_dsc},
		error: function(xhr) {
			alert('Ajax request 發生錯誤');
		},
		success: function(response) {
			location.href = "jobshow-search-result.php?pg=<?php echo  base64_encode(1);?>&"+response;
		}
	  });
}
</script>
</head>
    <body>

        <div id="wrapper">

        <div id="topbar">
            
            <?php include 'template/counter.php'; ?>

        </div><!-- topbar end -->

        
            <div id="container">
                <?php include 'template/sidebar.php'; ?>
                <div id="main">
				<?php include 'template/nav.php'?>
                <section>
                    <div id="path">首頁 > <a href="">職缺訊息</a> > 職缺查詢<span><a onclick="history.back()" class="button btnback"><i class="fa fa-reply"></i>　BACK 回上一頁</a></span></div>
                    <div class="infor">
                        <ul>
                            <li class="center"><i class="fa fa-search"></i> 查詢條件</li>
                            <li>職缺屬性查詢</li>
                            <li>
                                <ol>
                                    <li>
									<label>職缺公告日期區間</label>
									<input type="text" class="datepicker" name="c_postdate_s" id="c_postdate_s">　~　<input type="text" class="datepicker" name="c_postdate_c" id="c_postdate_c">
									</li>
                                    <li><label>報名截止日期區間</label>
									<input type="text" class="datepicker" name="c_enddate_s" id="c_enddate_s">　~　<input type="text" class="datepicker" name="c_enddate_c" id="c_enddate_c">
									</li>
                                    <!--<li><label>考試開始日期區間</label><input type="text" class="datepicker" name="c_testdate_s">　~　<input type="text" class="datepicker" name="c_testdate_c"></li>-->
                                    <!--<li><label>錄取公告日期區間</label><input type="text" class="datepicker" name="c_pastdate_s">　~　<input type="text" class="datepicker" name="c_pastdate_c"></li>-->
                                    
                                    <!--<li><label>職缺類別</label>
                                        <select name="lack" id="lack">
										<option value=""></option>									
										<?php 
											if(is_array($lack_type_array)){
												foreach($lack_type_array as $key => $value){			
													echo '<option value="'.$key.'">'.$value.'</option>';
												}
											}
										?>
                                        </select>
                                    </li>-->
                                </ol>
                                <ol>
							<li>	
							<label>工作地點</label>
							<div class="address-zone">
								<select name="tw_counties_num" id="tw_counties_num">
								<option value=""></option>
								<?php 
									if(is_array($tw_counties_array)){
										foreach($tw_counties_array as $key => $value){															
											echo '<option value="'.$key.'">'.$value.'</option>';
										
										}
									}
								?>						
								</select>
							</div>			
                            </li>						
                            <li>	
							<label>學術專長</label>
						            <select name="Academic_expertise" id="Academic_expertise">	
						            <?php
						            	foreach ( $Academic_expertise_type_array as $value ) 
						            	{if ( '0'==$value['SubCode'] )
						            		{
						            			echo '<option value="'.$value['SubCode'].'" selected>'.$value['SubDesc'].'</option>';
						            		}
						            		else 
						            		{
						            		    echo '<option value="'.$value['SubCode'].'" >'.$value['SubDesc'].'</option>';
						            		}							
						            	}
						            ?>
						</select>		
							</li>
							<li>
								<label>公告主旨</label>
								<input type="text" class="subject" name="c_title"  id="c_title">
							</li>
							<li class="buttonbox">
									<button onclick="chk()">開始查詢</button>
							</li>
							</ol>
                            </li>
                        </ul>
                    </div><!-- infor end -->
                </section><!-- section end -->
                    

                </div>
                

            </div><!-- container end -->


            <?php include 'template/footer.php'; ?>



        </div><!-- wrapper ebd -->

    </body>
</html>
