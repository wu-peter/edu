<?php
session_start();
//包含需求檔案 ------------------------------------------------------------------------
include("./bcontroller/class/common_lite.php");
//宣告變數 ----------------------------------------------------------------------------
$ODb = new run_db("mysql",3306);      //建立資料庫物件
$online_people_num = $ODb->get_online_num();
if(is_array($_GET)){
	foreach($_GET as $key => $value){
	$_GET[$key] = decode_dowith_sql($value);
	}
}

/*
查詢步驟說明：
1.拉出職缺名稱資料
2.拉出各縣市資料
3.根據時間區間調出職缺資料並將職缺名額放進對應的數量資料內	
*/

$sql_dsc = "select * from `lack_type` ";
$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");		
while($row = mysql_fetch_array($res)){
	$lack_type_array[$row['num']] =$row['c_name'];
}	

$sql_dsc = "select * from `tw_counties` ";
$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");		
while($row = mysql_fetch_array($res)){
	$counties_array[$row['num']] =$row['counties_name'];
	foreach($lack_type_array as $key =>$value){
		$counties_number_array[$row['num']][$key] =0;//各縣市各職缺的資料 $counties_number_array[縣市的num][職缺的num]
	}	
}	


$date_dsc = "2014-01-01 ~";
//如果有選擇區間
if($_GET['sw_d_1'] !='' && $_GET['sw_d_2'] !=''){
$sw_d_1_array = explode("/",$_GET['sw_d_1']);
$sw_d_2_array = explode("/",$_GET['sw_d_2']);
$date_dsc = $sw_d_1_array[2]."-".$sw_d_1_array[1]."-".$sw_d_1_array[0]." ~ ".$sw_d_2_array[2]."-".$sw_d_2_array[1]."-".$sw_d_2_array[0];
$where_dsc = " `p_d`.`c_postdate`>='".$sw_d_1_array[2]."-".$sw_d_1_array[1]."-".$sw_d_1_array[0]."' and  `p_d`.`c_enddate`<='".$sw_d_2_array[2]."-".$sw_d_2_array[1]."-".$sw_d_2_array[0]."' ";

}else{
$where_dsc =  " `p_d`.`c_postdate`>='2014-01-01' ";
}

//開始撈資料了
$sql_dsc = "
select `p_d`.`c_school_num`,`p_d`.`num`,`s_d`.`tw_counties_num`,`l_d`.`lack_type_num`,`l_d`.`people_number`    
from `post_data` as `p_d` 
left join `school_data` as `s_d` on `s_d`.`num` = `p_d`.`c_school_num` 
left join `lack_data` as `l_d` on `l_d`.`post_data_num` = `p_d`.`num`  
where ".$where_dsc." and `p_d`.`c_school_num` > '' and `l_d`.`people_number` > '' ";	
$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");	
while($row = mysql_fetch_array($res)){				
	$counties_number_array[$row['tw_counties_num']][$row['lack_type_num']] += $row['people_number'];
}			
?>
<!DOCTYPE html>
<html lang="en">
<head>

<?php include 'template/header.inc'; ?>
<script language="javascript">
function go_search(){
if($('#sw_d_1').val() !='' && $('#sw_d_2').val() !=''){
location.replace("?sw_d_1="+$('#sw_d_1').val()+"&sw_d_2="+$('#sw_d_2').val());		
}
}
</script>
</head>
<body>

<div id="wrapper">

<div id="topbar">

<?php include 'template/counter.php'; ?>

</div><!-- topbar end -->


<div id="container">

<?php include 'template/sidebar.php'; ?>

<div id="main">
<?php include 'template/nav.php'?>


<section>
<div id="path">首頁 > <a href="">統計資料</a> > 各縣市職缺數<span><a onclick="history.back()" class="button btnback"><i class="fa fa-reply"></i>　BACK 回上一頁</a></span></div>
<div class="infor">
<ul>
<li class="center"><i class="fa fa-search"></i> 查詢條件</li>
<li>職缺屬性查詢</li>
<li>
<ol>
<li><label>職缺公告日期區間</label><input type="text" class="datepicker" id="sw_d_1" value="<?php echo $_GET['sw_d_1'];?>">　~　<input type="text" class="datepicker" id="sw_d_2" value="<?php echo $_GET['sw_d_2'];?>"></li>
<li class="buttonbox"><button onclick="go_search()">開始查詢</button></li>
</ol>
</li>
<li>統計區間　<?php echo $date_dsc;?></li>
<li>
<div class="list">
<table>
<tr>
<td></td>
<?php 
foreach($lack_type_array as $key=>$value){
echo '<td>'.$value.'</td>';
}
?>
</tr>
<?php 
foreach($counties_number_array as $key=>$value){
	echo '<tr><td>'.$counties_array[$key].'</td>';
	foreach($value as $peopler_num){
		echo '<td><span>'.$peopler_num.'</span>名</td>';	
	}
	echo '</tr>';
}
?>
<!-- 帶出全縣市資料 -->
</table>
</div>
</li>
</ul>
</div><!-- infor end -->
</section><!-- section end -->
</div>
</div><!-- container end -->
<?php include 'template/footer.php'; ?>
</div><!-- wrapper ebd -->
</body>
</html>
