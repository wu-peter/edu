<?php

		$times = 0;    
//这里要注意，因为要“真正的"改变$sql的值，所以用引用传值  
function bindParam(&$sql, $location, $var, $type) {    
    global $times;    
    //确定类型    
    switch ($type) {    
       //字符串    
       default:                    //默认使用字符串类型    
      case 'STRING' :    
           $var = addslashes($var);  //转义    
           $var = "'".$var."'";      //加上单引号.SQL语句中字符串插入必须加单引号    
           break;    
       case 'INTEGER' :    
       case 'INT' :    
           $var = (int)$var;         //强制转换成int    
       //还可以增加更多类型..    
   }    
  //寻找问号的位置    
   for ($i=1, $pos = 0; $i<= $location; $i++) {    
       $pos = strpos($sql, '?', $pos+1);    
   }    
   //替换问号    
   $sql = substr($sql, 0, $pos) . $var . substr($sql, $pos + 1);   
} 
   
//包含需求檔案 ------------------------------------------------------------------------
	include("./bcontroller/class/common_lite.php");
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	$online_people_num = $ODb->get_online_num();
    //取出內容
	//mod by 志豪 20160721 不知為何PHP要轉double型態就會卡住 因此先轉型並做is_integet的判斷
	$is_integer = $_GET['num'];
	$is_integer_check = $is_integer + 0;
	if(isset($_GET['num'])&&is_integer($is_integer_check)){
        if($_GET['num']>0&&$_GET['num']<1000)
		{		
		$_GET['num']=$_GET['num'];
        $_GET['num'] = htmlspecialchars($_GET['num']);	
        }		
		else
		{
		$_GET['num']=1;	
		}		
	}else{
		 $_GET['num'] =1;
	}	
	  $up_dscnews ="select * from `new_data` where `num`= ?";
	  bindParam($up_dscnews, 1,$_GET['num'], 'STRING');
	  
	 //die($up_dscnews);
		$resnews = $ODb->query($up_dscnews) or die("更新資料出錯，請聯繫管理員。");
		while($rownews = mysql_fetch_array($resnews)){
				$up_date = $rownews['up_date'];	
				$num = $rownews['num'];	
				$c_title = $rownews['c_title'];
				$c_url = $rownews['c_url'];
				$c_dsc = $rownews['c_dsc'];
				$c_type = $rownews['c_type'];
				
				
			}
			
			//取出檔案
			 $up_dscfile ="select * from `file_data` where `table_num`=? and `table_name`='new_data'";
			  bindParam($up_dscfile, 1,$_GET['num'], 'STRING');
		$resfile = $ODb->query($up_dscfile) or die("更新資料出錯，請聯繫管理員。");
		while($rowfile = mysql_fetch_array($resfile)){
				$up_datefile[] = $rowfile['up_date'];	
				$num_file[] = $rowfile['num'];	
				$c_save_dir[] = $rowfile['c_save_dir'];
				$file_name[] = $rowfile['file_name'];
				$save_name[] = $rowfile['save_name'];
				$table_num[] = $rownews['table_num'];
				$file_size[] = $rowfile['file_size'];
				
				
			}
			
		
	$total_num=mysql_num_rows($resfile);
	 	//取出次數
		for($x=0;$x<$total_num;$x++){
	 $up_dscnum ="select * from `click_num` where `file_num`=?";
	 bindParam($up_dscnum, 1,$num_file[$x], 'STRING');
		$resnum = $ODb->query($up_dscnum) or die("更新資料出錯，請聯繫管理員。");
		while($rownum = mysql_fetch_array($resnum)){
				$click_num[$x] = $rownum['total_num'];	
				
		}
				
			}
	
	
	
	?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <?php include 'template/header.inc'; ?>
       
    </head>
    <body>

        <div id="wrapper">

        <div id="topbar">
            
            <?php include 'template/counter.php'; ?>

        </div><!-- topbar end -->

        
            <div id="container">

                <?php include 'template/sidebar.php'; ?>

                <div id="main">
                    <?php include 'template/nav.php'?>


                <section>
                    <div id="path">首頁 > <a href="news.php">公告</a><span><a onclick="history.back()" class="button btnback"><i class="fa fa-reply"></i>　BACK 回上一頁</a></span>
                    </div>
                    <div class="main-title">公告主旨：<?php echo $c_title;?><span>刊載日期：<?php echo substr($up_date,0,4)."-".substr($up_date,6,1)."-".substr($up_date,8,2);?></span>
                    </div>
                    <div id="content">
                        <!-- 文字編輯區 -->
                        <?php echo $c_dsc;?>
                    </div>
                    <div id="download">附檔下載：<!-- 沒有則顯示「無」 -->
                        <ul>
                        <?php if($total_num>0){
							for($i=0;$i<$total_num;$i++){
							?>
                        
                            <li><a href="./download.php?file=<?php echo str_replace("%2F","/",$c_save_dir[$i]).str_replace("%2F","/",$save_name[$i]);?>&name=<?php echo str_replace("%2F","/",$save_name[$i]);?>&num=<?php echo $num_file[$i];?>;"><i class="fa fa-cloud-download"></i> <strong><?php echo $file_name[$i];?></strong><span>檔案大小:<?php echo ceil($file_size[$i]/1024/1024);?>m</span><span>下載次數:<?php if(isset($click_num[$i])&&$click_num[$i]>0){echo $click_num[$i];}else{echo "0";}?>次</span></a></li>
                            
                            <?php }}else{echo "無";}?>
                        </ul>
                    </div>
                 <?php /*?>   <div class="infor">
                        <ul>
                            <li class="center"><i class="fa fa-bookmark"></i> 發佈資訊</li>
                            <li><label>有效日期：</label>有效起始日 2014-01-01 ~ 有效截止日 2014-01-08</li>
                            <li><label>教師級別：</label>Level</li>
                            <li><label>任教單位：</label>大學</li>
                            <li><label>聘期：</label>三個月</li>
                            <li><label>名額：</label>正取3名 備取1名</li>
                            <li><label>參考網址：</label><a href="" target="_blank">http://www.com.tw</a></li>
                            <li><label>工作地點：</label>10051臺北市中正區中山南路5號</li>
                            <li><label>最後編修日期：</label>2014-01-01</li>
                        </ul>
                    </div><!-- infor end --><?php */?>
                </section><!-- section end -->


                </div>
                

            </div><!-- container end -->


            <?php include 'template/footer.php'; ?>



        </div><!-- wrapper ebd -->

    </body>
</html>
