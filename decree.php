<?php
	session_start();
//包含需求檔案 ------------------------------------------------------------------------
	include("./bcontroller/class/common_lite.php");
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <?php include 'template/header.inc'; ?>
       
    </head>
    <body>

        <div id="wrapper">

        <div id="topbar">
            
            <?php include 'template/counter.php'; ?>

        </div><!-- topbar end -->

        
            <div id="container">

                <?php include 'template/sidebar.php'; ?>

                <div id="main">
                    <?php include 'template/nav.php'?>


                <section>
                    <div class="info">
                            <div class="title">
                                <img src="images/decree_title.png" alt="相關法令標題">
                            </div><!-- title end -->
                    <div id="path">首頁 > <a href="">法令下載</a></div>
                    <div class="list">
                    <table cellspacing="2">
                        <tr class="list-title">
                            <th width="80">類別</th>
                            <th>主旨</th>
                            <th width="200">公文日期文號</th>
                            <th width="30">瀏覽</th>
                        </tr>
                        <tr>
                            <td>B其他釋例</td>
                            <td class="left">辦理教師甄試切結期間案</td>
                            <td class="left">教育部102年7月24日臺教師(二)字第1020111072號函</td>
                            <td><a href="decree-detail.php"><img src="images/btn_view.png" alt="觀看詳細"></a></td>
                        </tr>
                        <tr>
                            <td>B其他釋例</td>
                            <td class="left">辦理教師甄試切結期間案</td>
                            <td class="left">教育部102年7月24日臺教師(二)字第1020111072號函</td>
                            <td><a href="decree-detail.php"><img src="images/btn_view.png" alt="觀看詳細"></a></td>
                        </tr>
                        <tr>
                            <td>B其他釋例</td>
                            <td class="left">辦理教師甄試切結期間案</td>
                            <td class="left">教育部102年7月24日臺教師(二)字第1020111072號函</td>
                            <td><a href="decree-detail.php"><img src="images/btn_view.png" alt="觀看詳細"></a></td>
                        </tr>
                        <tr>
                            <td>B其他釋例</td>
                            <td class="left">辦理教師甄試切結期間案</td>
                            <td class="left">教育部102年7月24日臺教師(二)字第1020111072號函</td>
                            <td><a href="decree-detail.php"><img src="images/btn_view.png" alt="觀看詳細"></a></td>
                        </tr>
                        <tr>
                            <td>B其他釋例</td>
                            <td class="left">辦理教師甄試切結期間案</td>
                            <td class="left">教育部102年7月24日臺教師(二)字第1020111072號函</td>
                            <td><a href="decree-detail.php"><img src="images/btn_view.png" alt="觀看詳細"></a></td>
                        </tr>
                    </table>
                    </div>
                    <div id="pages">目前頁數：1/2頁　　總筆數：13筆　<a href="" class="btn"><i class="fa fa-angle-left"></i>　上一頁</a>　<a href="" class="opt">1</a>　2　3　<a href="" class="btn">下一頁　<i class="fa fa-angle-right"></i></a>　跳至 <select><option>2</option></select> 頁</div>
                    </div><!-- info end -->
                </section><!-- section end -->


                </div>
                

            </div><!-- container end -->


            <?php include 'template/footer.php'; ?>



        </div><!-- wrapper ebd -->

    </body>
</html>
