<?php
  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	$upFileFload = "./upFile/".date("Ymd",time());
	$upFile = $upFileFload."/";
	
	//解SQL Injection
	foreach($_GET as $key => $value){
		$_GET[$key] = decode_dowith_sql($value);
	}

	//解base64
	foreach($_GET as $key => $value)
	{
		$_GET[$key] = base64_decode($value);
	}
	
	
	if($_POST['send_data']=='HasPostValue' ){
		
		$_POST['c_a'] = str_replace("twRRtw",">",$_POST['c_a']);
		$_POST['c_a'] = str_replace("twLLtw","<",$_POST['c_a']);
		//$nowdate =  date("Y-m-d H:i",time());		
		$up_dsc ="update `que_and_ans_data` set `c_type`     ='".$_POST['c_type'].  "',
												`c_q`        ='".decode_dowith_sql($_POST['c_q'])  .  "',
												`c_a`        ='".decode_dowith_sql($_POST['c_a'])  .  "',
												`c_url`      ='".decode_dowith_sql($_POST['c_url']) .  "',
												`up_date`    ='".decode_dowith_sql($_POST['up_date']).  "'
										 		where `num`  ='".$_GET['num']    .  "'";
		$res=$ODb->query($up_dsc) or die("更新資料出錯，請聯繫管理員。");
		ri_jump("c_que_and_ans_data.php?pg=".base64_encode($_POST['pg'])."&s=".base64_encode($_POST['s']));
	}
	
	if($_GET['num'] !=''){
		//取出內文資料	
		$sql_dsc = "SELECT * FROM `que_and_ans_data` WHERE `num`='".$_GET['num']."'";
		$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
		while($row = mysql_fetch_array($res))
		{
			$user_array['num']     =     $row['num'];
			$user_array['c_type']  =     $row['c_type'];	
			$user_array['c_q']     =     $row['c_q'];
			$user_array['c_a']     =     $row['c_a'];
			$user_array['up_date'] =     $row['up_date'];				
		}
		
	}else{
		ri_jump("c_que_and_ans_data.php");
	}	
	
	$que_and_ans_type ="select * from `que_and_ans_type` order by `num` ";
	$res=$ODb->query($que_and_ans_type) or die("更新資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res))
	{
		$sql_array['num'] = $row['num'];				
		$sql_array['c_name'] = $row['c_name'];		
		$que_and_ans_type_array[] = $sql_array;
	}
?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>一般會員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>

<!--月曆-->
<script src="js/jquery/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/jquery/custom_language_zh.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.16.custom.css" />


<script type="text/javascript">
	

</script>
<script type="text/javascript">
	//-----------------------------------------
	// Confirm Actions (delete, uninstall)
	//-----------------------------------------
	$(document).ready
	(
		function()
		{
			
		  $('#ulcssmenu ul').hide();
		  
		  $('#ulcssmenu li a').click
		  (
				function() 
				{
					var openMe = $(this).next();
					var mySiblings = $(this).parent().siblings().find('ul');
					if (openMe.is(':visible')) 
					{
						openMe.slideUp('normal');  
					} 
					else 
					{
						mySiblings.slideUp('normal');  
						openMe.slideDown('normal');
					}
			    }
			);
			
		  $('#ulcssmenu li[id="que_and_ans"] ul').slideDown('fast');
			
		}
	);
	
	var nowFileNum=2;

function ck_value(){
var isGo = true;
var err_dsc = '';
var ck_array   =  ["c_q"  ,  "c_a"     ,  "up_date"   ,"c_type"  ];
var err_array  =  ["請輸入標題!" , "請輸入內文！"   , "請輸入更新日期！！" ,"請選擇分類"];
var type_array =  ["text"     , "ckedit"      ,    "text"      ,"number"];

for(var x=0;x< ck_array.length;x++){
	switch(type_array[x]){
		case "text":
				if($('#'+ck_array[x]).val() ==''){
				err_dsc = err_dsc + err_array[x] +'\r\n';
				isGo = false;
				}
		break;
		case "ckedit":
				var dsc =encodeURI(editor.document.getBody().getText());
				if(dsc == '%0A'){//cdedit 無輸入時自動會補入%0A
					err_dsc = err_dsc + err_array[x] +'\r\n';
					isGo = false;
				}
		break;
		case "number":
			if(!$.isNumeric($('#'+ck_array[x]).val()) ){
				err_dsc = err_dsc + err_array[x] +'\r\n';
				isGo = false;				
			}		
		break;
	}
}	
	if(isGo){
		$('#form').submit();
	}
	
	if(err_dsc !=''){
		alert(err_dsc);
	}
}


function addOtherFile(){
var addnum = $('#addNum').val();
if(!$.isNumeric(addnum)) {
	$('#addNum').focus();
    alert('只能輸入數字');
}else{
for(var x=0;x<addnum;x++){
nowFileNum++;
var newobj = '<br><input type="file" name="c_file'+nowFileNum+'" id="c_file'+nowFileNum+'">';
$('#file_area').append(newobj);
}
}
}

jQuery(function($){
  $('#up_date').datepicker({dateFormat: 'yy-mm-dd',changeYear : true,changeMonth : true});
});

function del(num,num2){
var dsc_value = $('#del_dsc').val();
$('#c_del_area_'+num).remove();
	if( dsc_value == ''){
		$('#del_dsc').val(num2);
	}else{
		$('#del_dsc').val(dsc_value+','+num2);
	}
}
</script>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
	  <div class="breadcrumb">
			 <a href="c_que_and_ans_data.php">分類管理</a> 
	  </div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" />關於Q&A::Q&A管理</h1>
     <div class="buttons"><a onclick="ck_value()" class="button">存檔</a><a class="button" onclick="history.back();">取消</a></div>
    </div>
     <div class="content">
	    <form action="" method="post" enctype="multipart/form-data" id="form">
			<div id="tab-general">
				<table class="form">
				<tr>
					<td>分類</td>
					<td>
						<select name="c_type" id="c_type" >
							<option value=""></option>
							<?php
								/**
								 * 做分類選擇
								 */ 
								foreach($que_and_ans_type_array as $value)
								{
									if($user_array['c_type'] == $value['num'])
									{
										echo '<option value="'.$value['num'].'" selected>'.$value['c_name'].'</option>';
									}else
									{
										echo '<option value="'.$value['num'].'" >'.$value['c_name'].'</option>';
									}
								}
							?>
						</select>
					</td>
				</tr>				
				<tr>
					<td>標題</td>
					<td>
						<input type="text" name="c_q" id="c_q" size="100" value="<?php echo $user_array['c_q']; ?>">						
					</td>
				</tr>
				<tr>
					<td>更新日期</td>
					<td><input type="text" name="up_date" id="up_date" value="<?php echo date("Y-m-d",time());?>" ></td>
				</tr>	
				<tr>
					<td>內文</td>
					<td>
						<textarea name="c_a" id="c_a"><?php echo $user_array['c_a']; ?></textarea>						
					</td>
				</tr>
				</table>
			</div>
			<input type="hidden" name="send_data" value="HasPostValue">
			<input type="hidden" name="del_dsc" id="del_dsc" value="">
			<input type="hidden" name="num"  value="<?php echo $user_array['num'];  ?>">
		</form>	
    </div>
  </div>
</div>
</div>
<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script> 
<script type="text/javascript">
var editor = CKEDITOR.replace('c_a', {});
</script> 
<script language="javascript">
<?php
if($mg !=''){
echo 'alert("存檔完畢！！");';
}
?>
</script> 

<?php include("./layout/footer.php");?>
</body></html>