<?php
  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	

	//上傳資料	
	if($_POST['send_data']=='HasPostValue' ){
		$nowdate =  date("Y-m-d H:i",time());		
		$up_dsc ="insert into `member_type` set `c_type`='".decode_dowith_sql($_POST['c_type'])."'";
		
		//die($up_dsc);
		$res=$ODb->query($up_dsc) or die("更新資料出錯，請聯繫管理員。");
		ri_jump("c_member_type.php");
	}
	
?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>一般會員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
	//-----------------------------------------
	// Confirm Actions (delete, uninstall)
	//-----------------------------------------
	$(document).ready
	(
		function()
		{
			
		  $('#ulcssmenu ul').hide();
		  
		  $('#ulcssmenu li a').click
		  (
				function() 
				{
					var openMe = $(this).next();
					var mySiblings = $(this).parent().siblings().find('ul');
					if (openMe.is(':visible')) 
					{
						openMe.slideUp('normal');  
					} 
					else 
					{
						mySiblings.slideUp('normal');  
						openMe.slideDown('normal');
					}
			    }
			);
			
		  $('#ulcssmenu li[id="job"] ul').slideDown('fast');
			
		}
	);
	
	function ck_value()
	{
		if($('#c_type').val() =='')
		{
			alert('請輸入名稱！');
			$('#c_type').focus();	
		}
		else
		{
			$('#form').submit();
		}
		
	}
</script>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
	  <div class="breadcrumb">
			 <a href="c_member_type.php">分類管理</a> 
	  </div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" />人員類別::分類管理</h1>
     <div class="buttons"><a onclick="ck_value()" class="button">存檔</a><a href="c_member_type.php" class="button">取消</a></div>
    </div>
     <div class="content">
	    <form action="" method="post" enctype="multipart/form-data" id="form">
			<div id="tab-general">
				<table class="form">
				<tr>
					<td>類別名稱</td>
					<td><input type="text" name="c_type" id="c_type" value=""></td>
				</tr>
				</table>
			</div>
			<input type="hidden" name="send_data" value="HasPostValue">
		</form>	
    </div>
  </div>
</div>
</div>
<script language="javascript">
<?php
if($mg !=''){
echo 'alert("存檔完畢！！");';
}
?>
</script> 

<?php include("./layout/footer.php");?>
</body></html>