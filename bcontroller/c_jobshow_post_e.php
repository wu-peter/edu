<?php

  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	
	if($_GET['pg']=='' || !is_numeric($_GET['pg'])){	
			ri_jump("?pg=1");
	}

	$show_data_num = 50;//一頁顯示多少筆資料
	if($_GET['pg']== '1' ){
		$now_pg = 1;
		$pg_dsc = ' limit '.$show_data_num;
	}else{
		$now_pg = $_GET['pg'];
		$pg_dsc = ' limit '.(($_GET['pg']-1) * $show_data_num) .','.$show_data_num ;
	}
	
		$sql_dsc = "select * from `job_post` where `post_data_num` = '".$_GET['num']."'";
		$res = $ODb -> query($sql_dsc)or die("載入資料出錯，請聯繫管理員。");
		while($row = mysql_fetch_array($res))
		{
			$sql['num'] = $row['num'];
			$sql['post_data_num'] = $row['post_data_num'];
			$sql['front_mem_num'] = $row['front_mem_num'];
			$sql['c_name']        = $row['c_name'];
			$sql['c_dsc']         = $row['c_dsc'];
			$sql['phone_number']  = $row['phone_number'];
			$sql['email']         = $row['email'];
			$sql['update']        = $row['update'];
			$data_array[] = $sql;
		}

		
		//計算總頁數
		$total_pg = 0;
		
		$sql_dsc = "SELECT count(*) as `get_num` FROM `post_data` ";
		$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
		while($row = mysql_fetch_array($res)){
			if($row['get_num'] > 0 ){
				$num1 = $row['get_num']/$show_data_num;
				$num1_array = explode('.',$num1);
				$total_pg = $num1_array[0];
				if(count($num1_array)>1){
				$total_pg++;
				}
			}
		}

	
	
	//上傳檔案
?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>一般會員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
  $('#ulcssmenu ul').hide();
	$('#ulcssmenu li a').click(
		function() {
			var openMe = $(this).next();
			var mySiblings = $(this).parent().siblings().find('ul');
			if (openMe.is(':visible')) {
				openMe.slideUp('normal');  
			} else {
				mySiblings.slideUp('normal');  
				openMe.slideDown('normal');
			}
	  }
	);
	  $('#ulcssmenu li[id="job"] ul').slideDown('fast');

});

function ck_value(){
	$('#form').submit();
}

</script>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
	  <div class="breadcrumb">
			 <a href="c_jobshow_post.php">應徵人員</a> 
	  </div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" />應徵職缺::應徵人員管理</h1>
    </div>
      <div class="content">
			<div id="tab-general">
				<table class="list">
				<tr>
					<td>應徵帳號</td>
					<td>姓名</td>
					<td>上傳日期</td>
					<td>Email</td>
				</tr>
				<?php 
					if(is_array($data_array)){	
					foreach($data_array as $my_data){	
				?>
				<tr>
					<td><?php echo $my_data['front_mem_num'];?></td>
					<td><?php echo $my_data['c_name']?></td>
					<td><?php echo $my_data['update']?></td>
					<td><?php echo $my_data['email']?></td>
				</tr>
				<?php 
					}
				}else{	?>
				<td colspan="4" align="center">無資料！！</td>
				<?php } ?>
				</table>
			</div>
			<div class="page" align="center">
			 <?php
			  
			 	/**
			 	 * 分頁製作
			 	 */
			 
				if($total_pg >0){
						if(($now_pg-1) > 0){
						echo '
						<a href="c_jobshow_post_e.php?num='.$_GET['num'].'&pg='.($now_pg-1).'" > << </a>&nbsp;&nbsp;
						';
						}else{
						echo '
						<a href="c_jobshow_post_e.php?num='.$_GET['num'].'&pg=1" > << </a>&nbsp;&nbsp;';
						}
					 
					  if(($now_pg-5)>0){
						echo '<a href="c_jobshow_post_e.php?num='.$_GET['num'].'&pg='.($now_pg-5).'">'.($now_pg-5).'</a>';
					  }
					   if(($now_pg-4)>0){
						echo '<a href="c_jobshow_post_e.php?num='.$_GET['num'].'&pg='.($now_pg-4).'">'.($now_pg-4).'</a>';
					  }
					   if(($now_pg-3)>0){
						echo '<a href="c_jobshow_post_e.php?num='.$_GET['num'].'&pg='.($now_pg-3).'">'.($now_pg-3).'</a>';
					  }
					   if(($now_pg-2)>0){
						echo '<a href="c_jobshow_post_e.php?num='.$_GET['num'].'&pg='.($now_pg-2).'">'.($now_pg-2).'</a>';
					  }
					   if(($now_pg-1)>0){
						echo '<a href="c_jobshow_post_e.php?num='.$_GET['num'].'&pg='.($now_pg-1).'">'.($now_pg-1).'</a>';
					  }
					  
						echo $now_pg;//目前頁面
						
					   if(($now_pg+1)<=$total_pg){
						echo '<a href="c_jobshow_post_e.php?num='.$_GET['num'].'&pg='.($now_pg+1).'">'.($now_pg+1).'</a>';
					  }			  
					   if(($now_pg+2)<=$total_pg){
						echo '<a href="c_jobshow_post_e.php?num='.$_GET['num'].'&pg='.($now_pg+2).'">'.($now_pg+2).'</a>';
					  }			  
					   if(($now_pg+3)<=$total_pg){
						echo '<a href="c_jobshow_post_e.php?num='.$_GET['num'].'&pg='.($now_pg+3).'">'.($now_pg+3).'</a>';
					  }			  
					   if(($now_pg+4)<=$total_pg){
						echo '<a href="c_jobshow_post_e.php?num='.$_GET['num'].'&pg='.($now_pg+4).'">'.($now_pg+4).'</a>';
					  }			  
					   if(($now_pg+5)<=$total_pg){
						echo '<a href="c_jobshow_post_e.php?num='.$_GET['num'].'&pg='.($now_pg+5).'">'.($now_pg+5).'</a>';
					  }			  
					  
						if(($total_pg-$now_pg) > 0){
						echo '&nbsp;&nbsp;
						<a href="c_jobshow_post_e.php?num='.$_GET['num'].'&pg='.($now_pg+1).'" > >> </a>';
						}else{
						echo '&nbsp;&nbsp;
						<a href="c_jobshow_post_e.php?num='.$_GET['num'].'&pg='.$now_pg.'" > >> </a>';
						}
				}	  
		  ?>
		</div>		
    </div>
  </div>
</div>
</div>
<script language="javascript">
<?php
if($mg !=''){
echo 'alert("存檔完畢！！");';
}
?>
</script> 

<?php include("./layout/footer.php");?>
</body></html>