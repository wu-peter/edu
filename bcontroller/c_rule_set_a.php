<?php
  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN')
	{
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	$upFileFload = "./upFile/".date("Ymd",time());
	$upFile = $upFileFload."/";
	$upload_img_name_array = array('c_file_name');
	
	if($_POST['send_data']=='HasPostValue' )
	{
		if (!is_dir($upFileFload)) 
		{      //檢察upload資料夾是否存在
		  if (!mkdir($upFile))
		    { //不存在的話就創建upload資料夾
			//die ("上傳目錄不存在，並且創建失敗");
			}
		}
		
		$img_sql='';
		
		//處理上傳檔案
		foreach($upload_img_name_array as $weight_name)
		{
			$file_type_dsc = explode(".",basename($_FILES[$weight_name]['name']));
			$mtime = explode(" ", microtime()); 
			$startTime = $mtime[1].substr($mtime[0],2);			
			$new_name =  $startTime.".".$file_type_dsc[1];
			$uploadfile = $upFile.$new_name;
			
			if (move_uploaded_file($_FILES[$weight_name]['tmp_name'], $uploadfile)) 
			{
				$img_sql .=",`c_save_dir`='".$upFile."',`c_save_name`='".$new_name."',`c_file_name`='".$_FILES[$weight_name]['name']."'";
			}
		}
			
		$up_dsc ="insert into `rule_data` set `c_title`='".$_POST['c_title']."'".$img_sql;
		
		$res=$ODb->query($up_dsc) or die("更新資料出錯，請聯繫管理員。");
		ri_jump("c_rule_set.php");
	}
	
?>
<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>一般會員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
  $('#ulcssmenu ul').hide();
	$('#ulcssmenu li a').click(
		function() {
			var openMe = $(this).next();
			var mySiblings = $(this).parent().siblings().find('ul');
			if (openMe.is(':visible')) {
				openMe.slideUp('normal');  
			} else {
				mySiblings.slideUp('normal');  
				openMe.slideDown('normal');
			}
	  }
	);
	  $('#ulcssmenu li[id="front_member"] ul').slideDown('fast');

});

function ck_value(){
var isGo = true;
var err_dsc = '';
var ck_array =  [ "c_title","c_file_name"];
var err_array =  [ "請輸入法規標題!","請選擇檔案!"];
var type_array =  ["text","file"];

for(var x=0;x< ck_array.length;x++){
	switch(type_array[x]){
		case "text":
		case "file":
			if($('#'+ck_array[x]).val() ==''){
			err_dsc = err_dsc + err_array[x] +'\r\n';
			isGo = false;
			}
		break;
		
	}
}	
	if(isGo){
		$('#form').submit();
	}
	
	if(err_dsc !=''){
		alert(err_dsc);
	}
}
</script>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
	  <div class="breadcrumb">
			 <a href="">相關法令管理</a> 
	  </div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" /> 相關法規::<a href="c_rule_set.php">項目管理</a>::<a href="c_rule_set_a.php">新增法規</a></h1></h1>
      <div class="buttons"><a onclick="ck_value()" class="button">存檔</a><a  class="button" onclick="history.back();">取消</a></div>
    </div>
     <div class="content">
			<form action="c_rule_set_a.php" method="post" enctype="multipart/form-data" id="form">
			<div id="tab-general">
				<table class="form">
				<tr>
					<td>法規名稱</td>
					<td><input type="text" name="c_title" id="c_title" value="" ></td>
				</tr>				
				<tr>
					<td>檔案</td>
					<td><input type="file" name="c_file_name" id="c_file_name" ><br><input type="button" value="重選" onclick="$('#c_file_name').val('')"></td>
				</tr>				
				</table>
			</div>
			<input type="hidden" name="send_data" value="HasPostValue">
		</form>	
    </div>
  </div>
</div>
</div>
<script language="javascript">
<?php
if($mg !=''){
echo 'alert("存檔完畢！！");';
}
?>
</script> 

<?php include("./layout/footer.php");?>
</body></html>