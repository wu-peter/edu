<?php
	//包含需求檔案 ------------------------------------------------------------------------
	include("../class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		exit();
	}

	//宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	if(is_array($_POST)){
		foreach($_POST as $key => $value){
		$_POST[$key] = decode_dowith_sql($value);
		}	
	}
	
	/*
	查詢步驟說明：
	1.拉出職缺名稱資料
	2.拉出教育缺類別資料
	3.根據時間區間調出職缺資料並將職缺名額放進對應的數量資料內	
	*/

	$sql_dsc = "select * from `lack_type` ";
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");		
	while($row = mysql_fetch_array($res)){
	$lack_type_array[$row['num']] =$row['c_name'];
	$lack_type_total_peoper[$row['num']] =0;
	}	

	$sql_dsc = "select * from `teach_type` ";
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");		
	while($row = mysql_fetch_array($res)){
		$teach_type_array[$row['num']] =$row['teach_type'];
		foreach($lack_type_array as $key =>$value){
			$teach_type_number_array[$row['num']][$key] =0;//$teach_type_number_array[教育缺類別的num][職缺的num]
		}	
	}	
		
	
	if($_POST['start_dt'] !=''){
	require_once('../plugins/phpexcel/Classes/PHPExcel.php');
	require_once('../plugins/phpexcel/Classes/PHPExcel/IOFactory.php');
	
	$begin_date = $_POST['start_dt'];	
	$end_date = $_POST['end_dt']; 
	
	if($end_date !=''){
		$where_dsc = " `p_d`.`c_postdate` between '".$begin_date."' and  '".$end_date."' ";
	}else{
		$where_dsc = " `p_d`.`c_postdate`>='".$begin_date."' ";
	}
	
	//開始撈資料
	$sql_dsc ="
	select `p_d`.`teach_type`,`p_d`.`num`,`l_d`.`lack_type_num`,`l_d`.`people_number`    
	from `post_data` as `p_d` 
	left join `lack_data` as `l_d` on `l_d`.`post_data_num` = `p_d`.`num`  
	where ".$where_dsc." and `p_d`.`c_school_num` > '' and `l_d`.`people_number` > '' ";	
	
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");	
	while($row = mysql_fetch_array($res)){				
	$teach_type_number_array[$row['teach_type']][$row['lack_type_num']] += $row['people_number'];
	$lack_type_total_peoper[$row['lack_type_num']]+= $row['people_number'];
	}
	
	
	$PHPExcel = new PHPExcel();	
	$PHPExcel->getActiveSheet()->setTitle($_POST['start_dt']);//設置sheet的name
	$PHPExcel->getActiveSheet()->setCellValue('A1', "統計區間");
	$PHPExcel->getActiveSheet()->setCellValue('B1', $begin_date."~".$end_date);
	$e_key = 'B';
	foreach($lack_type_array as $key=>$value){
		$PHPExcel->getActiveSheet()->setCellValue($e_key.'2', $value);
		$e_key++;
	}
	
	$x=3;
	foreach($teach_type_number_array as $key=>$value){
		$PHPExcel->getActiveSheet()->setCellValue('A'.$x, $teach_type_array[$key]);
		$e_key = 'B';
		foreach($value as $peopler_num){
			$PHPExcel->getActiveSheet()->setCellValue($e_key.$x, $peopler_num."名");
			$e_key++;
		}
		$x++;
	}
	
	$PHPExcel->getActiveSheet()->setCellValue('A'.$x, "小計");
	$e_key = 'B';
	foreach($lack_type_total_peoper as $peopler_num){
		$PHPExcel->getActiveSheet()->setCellValue($e_key.$x, $peopler_num."名");
		$e_key++;
	}		
	
	$PHPExcelWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel5');	
	header("Content-type: application/force-download");
	header("Content-Disposition: attachment; filename=\"static.ods\"");
	header("Cache-Control: max-age=0");
	$PHPExcelWriter->save("php://output");
	exit;
	}else{
	exit;
	}
?>