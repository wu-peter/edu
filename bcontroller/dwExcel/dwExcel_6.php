<?php
ini_set("memory_limit", "128M");
session_start();

//包含需求檔案 ------------------------------------------------------------------------
include("../class/common_lite.php");
if ($_SESSION['zeroteamzero'] != 'IS_LOGIN') {
	exit();
}
//宣告變數 ----------------------------------------------------------------------------
$ODb = new run_db("mysql", 3306);      //建立資料庫物件

if (is_array($_POST)) {
	foreach ($_POST as $key => $value) {
		$_POST[$key] = decode_dowith_sql($value);
	}
}

//人員類別ˊ資料
$member_categrory_array =
	array(
		"0" => "",
		"1" => "編內",
		"2" => "編外",
		// Mod by 心詠 20190814 客戶0725需求，將此選項改為『其他(如編內或編外、或其他等)』
		// "3" => "編內或編外"
		"3" => "其他(如編內或編外、或其他等)"
	);


/**
 * 查詢公立學校
 * 步驟說明：
 * 1.查建檔者的學校KEY&系名KEY
 * 2.查對應的學校跟系所
 * 3.查職缺名稱及數量
 * 4.查職缺類別
 * 5.查職缺屬於  編內/編外
 */
$sql_db = "select * from `post_data`";
$res    = $ODb->query($sql_db) or die("載入資料出錯，請聯繫管理員。");
while ($row = mysql_fetch_array($res)) {
	$back_member_id[$row['num']]    = $row['c_school_num'];
	$back_member_group[$row['num']] = $row['b_member_group_num'];
}

$sql_db = "select * from `back_group_data`";
$res    = $ODb->query($sql_db) or die("載入資料出錯，請聯繫管理員。");
while ($row = mysql_fetch_array($res)) {
	$back_group_group[$row['num']]          = $row['c_name'];
}

$sql_db = "select * from `school_data`";
$res    = $ODb->query($sql_db) or die("載入資料出錯，請聯繫管理員。");
while ($row = mysql_fetch_array($res)) {
	$school_name[$row['num']]  =   $row['school_name'];
}

$sql_db = "select * from `lack_data`";
$res    = $ODb->query($sql_db) or die("載入資料出錯，請聯繫管理員。");
while ($row = mysql_fetch_array($res)) {
	$lack_num[$row['post_data_num']]      =   $row['people_number'];
	$lack_pro_name[$row['post_data_num']] =   $row['pro_title'];
	$lack_type[$row['post_data_num']]     =   $row['lack_type_num'];
}

$sql_db = "select * from `lack_type`";
$res    = $ODb->query($sql_db) or die("載入資料出錯，請聯繫管理員。");
while ($row = mysql_fetch_array($res)) {
	$lack_name[$row['num']]   =   $row['c_name'];
}

$sql_db = "select * from `member_type`";
$res    = $ODb->query($sql_db) or  die("載入資料出錯，請聯繫管理員。");
while ($row = mysql_fetch_array($res)) {
	$member_type[$row['num']]   =   $row['c_type'];
}

//取出學術專長資料
$sql_dsc = "SELECT * FROM  `code_table` where MainCode='Academic_expertise' order by SubCode";
$res = $ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
while ($row = mysql_fetch_array($res)) {
	$Academic_expertise_array[$row['SubCode']] = $row['SubDesc'];
}



if ($_POST['start_dt'] != '') {
	require_once('../plugins/phpexcel/Classes/PHPExcel.php');
	require_once('../plugins/phpexcel/Classes/PHPExcel/IOFactory.php');

	$begin_date = $_POST['start_dt'];
	$end_date = $_POST['end_dt'];
	$Academic_expertise = $_POST['Academic_expertise'];
	$member_category = $_POST['member_category'];

	if ($end_date != '') {
		$where_dsc = " `post_data`.`c_postdate` >= '" . $begin_date . "' <=  '" . $end_date . "' ";
		//$where_dsc = " `p_d`.`c_postdate` between '".$begin_date."' and  '".$end_date."' ";
	} else {
		$where_dsc = " `p_d`.`c_postdate`>='" . $begin_date . "' ";
	}


	//撈資料表二
	$begin = $_POST['start_dt'];
	$end   = $_POST['end_dt'];

	if ($end != '') {

		$where_dsc = " `c_postdate` >= '" . $begin . "' and  `c_postdate`  <=  '" . $end . "' ";
		//$where_dsc = " `c_postdate` between '".$begin."' and  '".$end."' ";
	} else {
		$where_dsc = " `c_postdate`>='" . $begin . "' ";
	}

	$descript = '';
	//加上專長領域的搜尋 和報表title內容
	if (!empty($Academic_expertise)) {
		$where_dsc .= " AND `Academic_expertise` ='" . $Academic_expertise . "' ";
		$descript = "統計 日期區間為" . $begin_date . "~" . $end_date . "、學術領域為「" . $Academic_expertise_array[$Academic_expertise] . "」";
	} else {
		$descript = "統計 日期區間為" . $begin_date . "~" . $end_date;
	}
	//加上人員類別的搜尋 和報表title內容
	if (!empty($member_category)) {
		$where_dsc .= " AND `member_type` ='" . $member_category . "' ";
		$descript .= "、人員類別為「" . $member_categrory_array[$member_category] . "」";
	}

	$sql_dsc = "SELECT *,(CONCAT(format(CAST( `Complete_number` AS decimal( 10, 4 ) ) * 100 / CAST( `Real_people_number` AS decimal( 10, 4 ) ),2),'%'))'Rate'  
					FROM  `post_data` 
					LEFT JOIN  `lack_data` ON  `post_data`.`num` =  `lack_data`.`post_data_num` 
					LEFT JOIN  `school_data` ON  `post_data`.`c_school_num` =  `school_data`.`num`   where " . $where_dsc . " and `school_type` ='1' order by `add_date` ";

	$res = $ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");


	while ($row = mysql_fetch_array($res)) {
		//mod by 志豪 刪除人員分類&職缺說明 20160226
		$sql_array['c_school_num']        = $school_name[$row['c_school_num']];           //學校名稱
		$sql_array['Academic_expertise']  = $Academic_expertise_array[$row['Academic_expertise']];  //學術領域
		$sql_array['b_member_group_num']  = $back_group_group[$row['b_member_group_num']]; //系所
		$sql_array['member_type']         = $member_categrory_array[$row['member_type']];            //邊內編外
		$sql_array['c_title']             = $row['c_title'];                              //主旨
		$sql_array['add_date']             = $row['add_date'];                            //新增時間
		$sql_array['lack_type_num']       = $lack_name[$row['lack_type_num']];            //職缺類別名稱
		$sql_array['Real_people_number']       = $row['Real_people_number'];              //職缺數量
		$sql_array['Complete_number']       = $row['Complete_number'];                    //成功人數
		$sql_array['Rate']       = $row['Rate'];                                          //媒合率
		//$sql_array['pro_title']           = $row['pro_title'];                            //職缺說明

		$data_array[] = $sql_array;
	}


	$sql_db = "SELECT sum( `Real_people_number` ) 'Real_people_number_sum', sum( `Complete_number` ) 'Complete_number_sum',
		            (CONCAT(format(CAST( sum( `Complete_number` ) AS decimal( 10, 4 ) ) * 100 / CAST( sum( `Real_people_number` ) AS decimal( 10, 4 ) ),2),'%'))'Rate_Sum'  
					FROM  `post_data` 
					LEFT JOIN  `lack_data` ON  `post_data`.`num` =  `lack_data`.`post_data_num` 
					LEFT JOIN  `school_data` ON  `post_data`.`c_school_num` =  `school_data`.`num`   where " . $where_dsc . " and `school_type` ='1' order by `add_date` ";

	$res    = $ODb->query($sql_db) or  die("載入資料出錯，請聯繫管理員。");
	while ($row2 = mysql_fetch_array($res)) {
		$Real_people_number_sum   =   $row2['Real_people_number_sum'];
		$Complete_number_sum   =   $row2['Complete_number_sum'];
		$Rate_Sum   =   $row2['Rate_Sum'];
	}

	$PHPExcel = new PHPExcel();


	//mod by 志豪 刪除人員分類&職缺說明 20160226
	$PHPExcel->setActiveSheetIndex(0);
	$PHPExcel->getActiveSheet()->setTitle('職缺資料');
	// $PHPExcel -> getActiveSheet() -> setCellValue('A1', "統計區間");
	// $PHPExcel -> getActiveSheet() -> setCellValue('B1', $begin_date."~".$end_date);
	$PHPExcel->getActiveSheet()->mergeCells('A1:K2'); #合併欄位
	$PHPExcel->getActiveSheet()->getStyle('A1:G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$PHPExcel->getActiveSheet()->getStyle('A1:G1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$PHPExcel->getActiveSheet()->setCellValue('A1', $descript); # 給值

	$PHPExcel->getActiveSheet()->setCellValue('A3', "序號");
	$PHPExcel->getActiveSheet()->setCellValue('B3', "學校名稱");
	$PHPExcel->getActiveSheet()->setCellValue('C3', "學術領域");
	$PHPExcel->getActiveSheet()->setCellValue('D3', "學校系所");
	$PHPExcel->getActiveSheet()->setCellValue('E3', "人員類別");
	$PHPExcel->getActiveSheet()->setCellValue('F3', "職缺主旨");
	$PHPExcel->getActiveSheet()->setCellValue('G3', "新增時間");
	$PHPExcel->getActiveSheet()->setCellValue('H3', "職缺類別");
	$PHPExcel->getActiveSheet()->setCellValue('I3', "職缺名額");
	$PHPExcel->getActiveSheet()->setCellValue('J3', "媒合成功人數");
	$PHPExcel->getActiveSheet()->setCellValue('K3', "媒合率");

	//mod by 志豪 設定框線 20160226
	$e_key = 'B';
	$x = 4;
	if (is_array($data_array) && !empty($data_array)) {
		foreach ($data_array as $key => $my_data) {
			$PHPExcel->getActiveSheet()->getStyle('A' . $x)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$PHPExcel->getActiveSheet()->getStyle('A' . $x)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$PHPExcel->getActiveSheet()->getStyle('A' . $x)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$PHPExcel->getActiveSheet()->getStyle('A' . $x)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			foreach ($my_data as $key => $value) {
				if ($x == '4') {
					$PHPExcel->getActiveSheet()->getStyle('A' . '1')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle('A' . '1')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle('A' . '1')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle('A' . '1')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle('A' . '2')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle('A' . '2')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle('A' . '2')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle('A' . '2')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle('A' . '3')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle('A' . '3')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle('A' . '3')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle('A' . '3')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle($e_key . '1')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle($e_key . '1')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle($e_key . '1')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle($e_key . '1')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle($e_key . '2')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle($e_key . '2')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle($e_key . '2')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle($e_key . '2')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle($e_key . '3')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle($e_key . '3')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle($e_key . '3')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$PHPExcel->getActiveSheet()->getStyle($e_key . '3')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				}
				$PHPExcel->getActiveSheet()->getStyle($e_key . $x)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$PHPExcel->getActiveSheet()->getStyle($e_key . $x)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$PHPExcel->getActiveSheet()->getStyle($e_key . $x)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$PHPExcel->getActiveSheet()->getStyle($e_key . $x)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$e_key++;
			}
			$e_key = 'B';
			$x++;
		}



		$e_key = 'B';
		$x = 4;
		$S_Number = 1;
		foreach ($data_array as $key => $my_data) {
			$PHPExcel->getActiveSheet()->setCellValue('A' . $x, $S_Number);
			foreach ($my_data as $key => $value) {
				$PHPExcel->getActiveSheet()->setCellValue($e_key . $x, $value);
				$e_key++;
			}
			$e_key = 'B';
			$x++;
			$S_Number++;
		}

		//職缺人數總計
		$PHPExcel->getActiveSheet()->setCellValue('I' . $x, $Real_people_number_sum);
		//成功人數		
		$PHPExcel->getActiveSheet()->setCellValue('J' . $x, $Complete_number_sum);
		//媒合率
		$PHPExcel->getActiveSheet()->setCellValue('K' . $x, $Rate_Sum);

		$e_key = 'A';
		for ($e_key; $e_key < 'L'; $e_key++) {
			$PHPExcel->getActiveSheet()->getStyle($e_key . $x)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$PHPExcel->getActiveSheet()->getStyle($e_key . $x)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$PHPExcel->getActiveSheet()->getStyle($e_key . $x)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$PHPExcel->getActiveSheet()->getStyle($e_key . $x)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		}
	} else {
		# 如果搜尋結果為空，則只畫線
		$e_key = 'A';
		$x = '1';
		for ($e_key; $e_key < 'L'; $e_key++) {
			for ($x = '1'; $x < '4'; $x++) {
				$PHPExcel->getActiveSheet()->getStyle($e_key . $x)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$PHPExcel->getActiveSheet()->getStyle($e_key . $x)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$PHPExcel->getActiveSheet()->getStyle($e_key . $x)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$PHPExcel->getActiveSheet()->getStyle($e_key . $x)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			}
			$PHPExcel->getActiveSheet()->getStyle($e_key . $x)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$PHPExcel->getActiveSheet()->getStyle($e_key . $x)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$PHPExcel->getActiveSheet()->getStyle($e_key . $x)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$PHPExcel->getActiveSheet()->getStyle($e_key . $x)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		}
		$PHPExcel->getActiveSheet()->mergeCells('A4:K4'); #合併欄位
		$PHPExcel->getActiveSheet()->getStyle('A4:K4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$PHPExcel->getActiveSheet()->getStyle('A4:K4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$PHPExcel->getActiveSheet()->setCellValue('A4', '此搜尋無資料'); # 給值	
	}

	$PHPExcelWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel5');



	header("Content-type: application/force-download");
	if ($_POST["file_type"] == 1) {
		header("Content-Disposition: attachment; filename=\"static6.ods\"");
	} elseif ($_POST["file_type"] == 2) {
		header("Content-Disposition: attachment; filename=\"static6.xls\"");
	}
	header("Cache-Control: max-age=0");
	$PHPExcelWriter->save("php://output");
	exit;
} else {
	exit;
}
