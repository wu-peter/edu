<?php

	session_start();

	//包含需求檔案 ------------------------------------------------------------------------
	include("../class/common_lite.php");
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN')
	{
		exit();
	}
	//宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	
	if(is_array($_POST))
	{
			foreach($_POST as $key => $value)
			{
				$_POST[$key] = decode_dowith_sql($value);
			}	
	}
	
	/**
	 * 當登入者等級為學校人事室=>撈自己學校的資料
	 */
	 
	 //學校登入
	 if($_SESSION['userlogintype']=='2')
	{
		$sql = "select * from `post_data` where `c_school_num` = '".$_SESSION['cschoolnum']."'";
		
		$res = $ODb -> query($sql)or die("載入資料出錯，請聯繫管理員。");
		while ($row = mysql_fetch_array($res))
		{
			$back_member_id[$row['num']]    = $row['c_school_num'];        
			$back_member_group[$row['num']] = $row['b_member_group_num'];
		}
		
		$sql_db = "select * from `back_group_data`";
		$res    = $ODb->query($sql_db) or die("載入資料出錯，請聯繫管理員。");
		while ($row = mysql_fetch_array($res))
		{
			$back_group_group[$row['num']]          = $row['c_name']; 
		}
		
		$sql_db = "select * from `school_data`";
		$res    = $ODb -> query ($sql_db) or die("載入資料出錯，請聯繫管理員。");
		while ( $row = mysql_fetch_array($res) )
		{
			$school_name[$row['num']]  =   $row['school_name'];
		} 
			
		$sql_db = "select * from `lack_data`";
		$res    = $ODb->query($sql_db) or die("載入資料出錯，請聯繫管理員。");
		while ($row = mysql_fetch_array($res))
		{
			$lack_num[$row['post_data_num']]      =   $row['people_number'];
			$lack_pro_name[$row['post_data_num']] =   $row['pro_title'];    
			$lack_type[$row['post_data_num']]     =   $row['lack_type_num'];
		}
		   
		$sql_db = "select * from `lack_type`";
		$res    = $ODb->query($sql_db) or die("載入資料出錯，請聯繫管理員。");
		while ($row = mysql_fetch_array($res))
		{
			$lack_name[ $row['num'] ]   =   $row['c_name'];
		}
		   
		$sql_db = "select * from `member_type`";
		$res    = $ODb->query($sql_db) or  die("載入資料出錯，請聯繫管理員。");
		while ($row = mysql_fetch_array($res))
		{
			$member_type[$row['num']]   =   $row['c_type'];
		}
		
		$sql_db = "select * from `tw_counties`";
		$res    = $ODb->query($sql_db) or  die("載入資料出錯，請聯繫管理員。");
		while ($row = mysql_fetch_array($res))
		{
			$location[$row['num']]   =   $row['counties_name'];
		}
		
		$sql_db = "select * from `back_member`";
		$res    = $ODb->query($sql_db) or  die("載入資料出錯，請聯繫管理員。");
		while ($row = mysql_fetch_array($res))
		{
			$back_member[$row['num']] = $row['c_login_name'];
		}
		
		
			if($_POST['start_dt'] !='')
			{   
				require_once('../plugins/phpexcel/Classes/PHPExcel.php');
				require_once('../plugins/phpexcel/Classes/PHPExcel/IOFactory.php');

				//撈資料表二
				$begin = $_POST['start_dt'];	
				$end   = $_POST['end_dt']; 	
			
				if($end !='')
				{
					$where_dsc = " `c_postdate` between '".$begin."' and  '".$end."' ";
				}
				else
				{
					$where_dsc = " `c_postdate`>='".$begin."' ";
				}
				
				$sql_dsc = "SELECT *FROM  `post_data` 
							Left JOIN `lack_data`
							on `post_data`.`num` = `lack_data`.`post_data_num` where ".$where_dsc." and `c_school_num` = '".$_SESSION['cschoolnum']."'";	
				
				$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
				//die($sql_dsc);
				
				while($row = mysql_fetch_array($res))
				{	
					$sql_array['c_school_num']        = $school_name[$row['c_school_num']];           //學校名稱
					$sql_array['b_member_group_num']  = $back_group_group[$row['b_member_group_num']];//系所
					$sql_array['location']            = $row['location'];                      //地點說明
					$sql_array['c_title']             = $row['c_title'];                              //主旨
					$sql_array['up_date']             = $row['up_date'];                              //更新時間
					$sql_array['lack_type_num']       = $lack_name[$row['lack_type_num']];            //職缺類別名稱
					$sql_array['people_number']       = $row['people_number'];                        //職缺數量
					$sql_array['pro_title']           = $row['pro_title'];                            //職缺說明
					$sql_array['b_member_num']        = $back_member[$row['b_member_num']];           //新增職缺帳號
					$data_array[] = $sql_array;
				}
				//$sql_array['member_type']         = $member_type[$row['member_type']];            //邊內編外
			
			$PHPExcel = new PHPExcel();	
			
			
			
			$PHPExcel -> createSheet();
			$PHPExcel -> setActiveSheetIndex(0);
			$PHPExcel -> getActiveSheet() -> setTitle('職缺資料');
			$PHPExcel -> getActiveSheet() -> setCellValue('A1', "統計區間");
			$PHPExcel -> getActiveSheet() -> setCellValue('B1', $begin."~".$end);
			$PHPExcel -> getActiveSheet() -> setCellValue('B2', "學校名稱");
			$PHPExcel -> getActiveSheet() -> setCellValue('C2', "學校系所");
			$PHPExcel -> getActiveSheet() -> setCellValue('D2', "地點");
			$PHPExcel -> getActiveSheet() -> setCellValue('E2', "職缺主旨");
			$PHPExcel -> getActiveSheet() -> setCellValue('F2', "新增時間");
			$PHPExcel -> getActiveSheet() -> setCellValue('G2', "職缺類別");
			$PHPExcel -> getActiveSheet() -> setCellValue('H2', "職缺名額");
			$PHPExcel -> getActiveSheet() -> setCellValue('I2', "職缺說明");
			$PHPExcel -> getActiveSheet() -> setCellValue('J2', "職缺新增帳號");
			
			
			
			
			
			$e_key = 'B';
			$x = 3;
			foreach( $data_array as $key => $my_data )
			{
				foreach( $my_data as $key => $value )
				{
					$PHPExcel -> getActiveSheet() -> setCellValue($e_key.$x,$value);
					$e_key++;
				}
				$e_key = 'B';
				$x++;
			}
			
			
			$PHPExcelWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel5');
			
			header("Content-type: application/force-download");
			header("Content-Disposition: attachment; filename=\"static4.ods\"");
			header("Cache-Control: max-age=0");
			$PHPExcelWriter->save("php://output");
			exit;
			
		}	
		else
		{
			exit;
		}
	}
?>