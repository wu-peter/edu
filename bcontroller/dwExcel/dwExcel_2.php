<?php
	set_time_limit(120);//20170327 志誠 避免讀取太久讓資料出不來
    ini_set("memory_limit","128M");
	session_start();

	//包含需求檔案 ------------------------------------------------------------------------
	include("../class/common_lite.php");
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN')
	{
		exit();
	}
	//宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	
	if(is_array($_POST))
	{
		foreach($_POST as $key => $value)
		{
		    $_POST[$key] = decode_dowith_sql($value);
		}	
	}
	
	/*
	查詢步驟說明：
	1.拉出職缺名稱資料
	2.拉出各縣市資料
	3.根據時間區間調出職缺資料並將職缺名額放進對應的數量資料內	
	*/

	$sql_dsc = "select * from `lack_type` ";
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");		
	while($row = mysql_fetch_array($res))
	{
		$lack_type_array[$row['num']] =$row['c_name'];
	}	

	$sql_dsc = "select * from `tw_counties` ";
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");		
	while($row = mysql_fetch_array($res))
	{
		$counties_array[$row['num']] =$row['counties_name'];
		foreach($lack_type_array as $key =>$value)
		{
		    $counties_number_array[$row['num']][$key] =0; //各縣市各職缺的資料 $counties_number_array[縣市的num][職缺的num]
		}	
	}
	 	
    /**
     * 步驟說明：
     * 1.查建檔者的學校KEY&系名KEY
     * 2.查對應的學校跟系所
     * 3.查職缺名稱及數量
     * 4.查職缺類別
     * 5.查職缺屬於  編內/編外
     */
    $sql_db = "select * from `post_data`";
    $res    = $ODb->query($sql_db) or die("載入資料出錯，請聯繫管理員。"); 
    while ($row = mysql_fetch_array($res))
    {
        $back_member_id[$row['num']]    = $row['c_school_num'];        
        $back_member_group[$row['num']] = $row['b_member_group_num'];  
    }
   
    $sql_db = "select * from `back_group_data`";
    $res    = $ODb->query($sql_db) or die("載入資料出錯，請聯繫管理員。");
    while ($row = mysql_fetch_array($res))
    {
        $back_group_group[$row['num']]          = $row['c_name']; 
    }
    
    $sql_db = "select * from `school_data`";
    $res    = $ODb -> query ($sql_db) or die("載入資料出錯，請聯繫管理員。");
    while ( $row = mysql_fetch_array($res) )
    {
        $school_name[$row['num']]  =   $row['school_name'];
    } 
        
    $sql_db = "select * from `lack_data`";
    $res    = $ODb->query($sql_db) or die("載入資料出錯，請聯繫管理員。");
    while ($row = mysql_fetch_array($res))
    {
        $lack_num[$row['post_data_num']]      =   $row['people_number'];
        $lack_pro_name[$row['post_data_num']] =   $row['pro_title'];    
        $lack_type[$row['post_data_num']]     =   $row['lack_type_num'];
    }
       
    $sql_db = "select * from `lack_type`";
    $res    = $ODb->query($sql_db) or die("載入資料出錯，請聯繫管理員。");
    while ($row = mysql_fetch_array($res))
    {
        $lack_name[ $row['num'] ]   =   $row['c_name'];
    }
       
    $sql_db = "select * from `member_type`";
    $res    = $ODb->query($sql_db) or  die("載入資料出錯，請聯繫管理員。");
    while ($row = mysql_fetch_array($res))
    {
        $member_type[$row['num']]   =   $row['c_type'];
    }
	 
	if($_POST['start_dt'] !='')
	{   
    	require_once('../plugins/phpexcel/Classes/PHPExcel.php');
    	require_once('../plugins/phpexcel/Classes/PHPExcel/IOFactory.php');
    	
    	$begin_date = $_POST['start_dt'];	
    	$end_date = $_POST['end_dt']; 	
    
    	if($end_date !='')
    	{
    		$where_dsc = "  `p_d`.`c_postdate` >= '".$begin_date."' and `p_d`.`c_postdate` <=  '".$end_date."' ";
    		//$where_dsc = " `p_d`.`c_postdate` between '".$begin_date."' and  '".$end_date."' ";
    	}
    	else
    	{
    		$where_dsc = " `p_d`.`c_postdate`>='".$begin_date."' ";
    	}
    	
    	//開始撈資料表一
    	$sql_dsc ="
    	select `p_d`.`c_school_num`,
    	       `p_d`.`num`,`s_d`.`tw_counties_num`,
    	       `l_d`.`lack_type_num`,
    	       `l_d`.`people_number`     
    	from `post_data` as `p_d` 
    	left join `school_data` as `s_d` on `s_d`.`num` = `p_d`.`c_school_num` 
    	left join `lack_data` as `l_d` on `l_d`.`post_data_num` = `p_d`.`num`  
    	where ".$where_dsc." and `p_d`.`c_school_num` > '' and `l_d`.`people_number` > '' ";
    	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");	
    	while($row = mysql_fetch_array($res))
    	{				
    		$counties_number_array[$row['tw_counties_num']][$row['lack_type_num']] += $row['people_number'];
    	}
    	
    	//撈資料表二
    	$begin = $_POST['start_dt'];	
    	$end   = $_POST['end_dt']; 	
    
    	if($end !='')
    	{
    		$where_dsc = " `c_postdate` >= '".$begin."' and  `c_postdate`  <=  '".$end."' ";
    		//$where_dsc = " `c_postdate` between '".$begin."' and  '".$end."' ";
    	}
    	else
    	{
    		$where_dsc = " `c_postdate`>='".$begin."' ";
    	}
    	
    	$sql_dsc = "SELECT * ,(CONCAT(format(CAST( `Complete_number` AS decimal( 10, 4 ) ) * 100 / CAST( `Real_people_number` AS decimal( 10, 4 ) ),2),'%'))'Rate'  
					FROM  `post_data` 
					LEFT JOIN  `lack_data` ON  `post_data`.`num` =  `lack_data`.`post_data_num` 
					LEFT JOIN  `school_data` ON  `post_data`.`c_school_num` =  `school_data`.`num`   where ".$where_dsc."  order by `add_date` ";	
        
    	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
    	
    	
    	while($row = mysql_fetch_array($res))
    	{	
		    //mod by 志豪 刪除人員分類&職缺說明 20160226
    		$sql_array['c_school_num']        = $school_name[$row['c_school_num']];           //學校名稱
    		$sql_array['b_member_group_num']  = $back_group_group[$row['b_member_group_num']];//系所
    		//$sql_array['member_type']         = $member_type[$row['member_type']];            //邊內編外
			$sql_array['c_title']             = $row['c_title'];                              //主旨
			$sql_array['add_date']             = $row['add_date'];                              //新增時間
			$sql_array['lack_type_num']       = $lack_name[$row['lack_type_num']];            //職缺類別名稱
			$sql_array['Real_people_number']       = $row['Real_people_number'];                        //職缺數量
			$sql_array['Complete_number']       = $row['Complete_number'];                        //成功人數
			$sql_array['Rate']       = $row['Rate'];                        //媒合率
			//$sql_array['pro_title']           = $row['pro_title'];                            //職缺說明
			
			$data_array[] = $sql_array;
    	}
    	
		$sql_db = "SELECT sum( `Real_people_number` ) 'Real_people_number_sum', sum( `Complete_number` ) 'Complete_number_sum',
		            (CONCAT(format(CAST( sum( `Complete_number` ) AS decimal( 10, 4 ) ) * 100 / CAST( sum( `Real_people_number` ) AS decimal( 10, 4 ) ),2),'%'))'Rate_Sum'  
					FROM  `post_data` 
					LEFT JOIN  `lack_data` ON  `post_data`.`num` =  `lack_data`.`post_data_num` 
					LEFT JOIN  `school_data` ON  `post_data`.`c_school_num` =  `school_data`.`num`   where ".$where_dsc."  order by `add_date` ";
		
		$res    = $ODb->query($sql_db) or  die("載入資料出錯，請聯繫管理員。");
		while ($row2 = mysql_fetch_array($res))
		{
			$Real_people_number_sum   =   $row2['Real_people_number_sum'];
			$Complete_number_sum   =   $row2['Complete_number_sum'];
			$Rate_Sum   =   $row2['Rate_Sum'];
		}
    	
    	
    	$PHPExcel = new PHPExcel();	
    	
    	//$PHPExcel -> createSheet();
    	$PHPExcel->setActiveSheetIndex(0);//指定目前要編輯的工作表 ，預設0是指第一個工作表
    	$PHPExcel->getActiveSheet()->setTitle($_POST['start_dt']);//設置sheet的name
    	$PHPExcel->getActiveSheet()->setCellValue('A1', "統計區間");
    	$PHPExcel->getActiveSheet()->setCellValue('B1', $begin_date."~".$end_date);
    	
    	$e_key = 'B';
    	foreach($lack_type_array as $key=>$value)
    	{
    		$PHPExcel->getActiveSheet()->setCellValue($e_key.'2', $value);
    		$e_key++;
    	}
    	$x=3;
    	
    	foreach($counties_number_array as $key=>$value)
    	{
    		$PHPExcel->getActiveSheet()->setCellValue('A'.$x, $counties_array[$key]);
    		$e_key = 'B';
    		foreach($value as $peopler_num)
    		{
    			$PHPExcel->getActiveSheet()->setCellValue($e_key.$x, $peopler_num."名");
    			$e_key++;
    		}
    		$x++;
    	}
    	
    	$PHPExcel -> createSheet();
    	//mod by 志豪 條件同dwExcel_5 20160308
    	$PHPExcel -> setActiveSheetIndex(1);
    	$PHPExcel -> getActiveSheet() -> setTitle('職缺資料');
    	$PHPExcel -> getActiveSheet() -> setCellValue('A1', "統計區間");
    	$PHPExcel -> getActiveSheet() -> setCellValue('A2', "序號");
    	$PHPExcel -> getActiveSheet() -> setCellValue('B1', $begin_date."~".$end_date);
    	$PHPExcel -> getActiveSheet() -> setCellValue('B2', "學校名稱");
    	$PHPExcel -> getActiveSheet() -> setCellValue('C2', "學校系所");
    	$PHPExcel -> getActiveSheet() -> setCellValue('D2', "職缺主旨");
    	$PHPExcel -> getActiveSheet() -> setCellValue('E2', "新增時間");
    	$PHPExcel -> getActiveSheet() -> setCellValue('F2', "職缺類別");
    	$PHPExcel -> getActiveSheet() -> setCellValue('G2', "職缺名額");
    	$PHPExcel -> getActiveSheet() -> setCellValue('H2', "媒合成功人數");
    	$PHPExcel -> getActiveSheet() -> setCellValue('I2', "媒合率");
    	
    	//mod by 志豪 設定框線 20160226
    	$e_key = 'B';
    	$x = 3;
    	foreach( $data_array as $key => $my_data )
    	{			
		        $PHPExcel -> getActiveSheet() -> getStyle('A'.$x)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle('A'.$x)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle('A'.$x)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle('A'.$x)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    	    foreach( $my_data as $key => $value )
    	    {
				if($x == '3')
				{
		        $PHPExcel -> getActiveSheet() -> getStyle('A'.'1')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle('A'.'1')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle('A'.'1')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle('A'.'1')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle('A'.'2')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle('A'.'2')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle('A'.'2')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle('A'.'2')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle($e_key.'1')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle($e_key.'1')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle($e_key.'1')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle($e_key.'1')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle($e_key.'2')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle($e_key.'2')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle($e_key.'2')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle($e_key.'2')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				}
		        $PHPExcel -> getActiveSheet() -> getStyle($e_key.$x)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle($e_key.$x)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle($e_key.$x)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle($e_key.$x)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    	        $e_key++;
    	    }
    	    $e_key = 'B';
    	    $x++;
    	}
    	
    	$e_key = 'B';
    	$x = 3;
		$S_Number = 1;
    	foreach( $data_array as $key => $my_data )
    	{
    	        $PHPExcel -> getActiveSheet() -> setCellValue('A'.$x,$S_Number);
    	    foreach( $my_data as $key => $value )
    	    {
    	        $PHPExcel -> getActiveSheet() -> setCellValue($e_key.$x,$value);
    	        $e_key++;

    	    }
    	    $e_key = 'B';
    	    $x++;
    	    $S_Number++;
    	}
		
		//職缺人數總計
    	$PHPExcel -> getActiveSheet() -> setCellValue('G'.$x,$Real_people_number_sum);
    	//成功人數		
    	$PHPExcel -> getActiveSheet() -> setCellValue('H'.$x,$Complete_number_sum);
		//媒合率
    	$PHPExcel -> getActiveSheet() -> setCellValue('I'.$x,$Rate_Sum);
		
		$e_key = 'A';
    	for( $e_key ; $e_key < 'J';$e_key++)
    	{
		        $PHPExcel -> getActiveSheet() -> getStyle($e_key.$x)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle($e_key.$x)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle($e_key.$x)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		        $PHPExcel -> getActiveSheet() -> getStyle($e_key.$x)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		}
    	
    	
    	$PHPExcelWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel5');
    	
    	header("Content-type: application/force-download");
    	header("Content-Disposition: attachment; filename=\"static2.ods\"");
    	header("Cache-Control: max-age=0");
    	$PHPExcelWriter->save("php://output");
    	exit;
    	
	}	
	else
	{
	    exit;
	}
	
?>