<?php
  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	
	foreach($_GET as $key => $value){
		$_GET[$key] = decode_dowith_sql($value);
	}
	
	
	if($_GET['pg']=='' || !is_numeric(base64_decode($_GET['pg'])))
	{	
			ri_jump("?pg=".base64_encode("1")."&s=".$_GET['s']);
	}

	//Url解碼
	foreach($_GET as $key => $value)
	{
		$_GET[$key] = base64_decode($value);
	}

	$show_data_num = 50;//一頁顯示多少筆資料
	if($_GET['pg']== '1' ){
		$now_pg = 1;
		$pg_dsc = ' limit '.$show_data_num;
	}else{
		$now_pg = $_GET['pg'];
		$pg_dsc = ' limit '.(($_GET['pg']-1) * $show_data_num) .','.$show_data_num ;
	}
	
	//加入標題搜尋
	$where_dsc="";
	if($_GET['s'] !='')
	{
		$where_dsc = " where `c_title` like '%".$_GET['s']."%' ";
	}

	//取出member_board資料	
	$sql_dsc = "SELECT *FROM  `message_board` ".$where_dsc." order by `num` ".$pg_dsc;
	
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res))
	{
		$sql_array['num']          = $row['num'];
		$sql_array['member_num']   = $row['member_num'];
		$sql_array['c_title']      = $row['c_title'];
		$sql_array['c_dsc']		   = $row['c_dsc'];
		$sql_array['is_view']	   = $row['is_view'];
		$sql_array['view_dsc']     = $row['view_dsc'];
		$sql_array['view_date']	   = $row['view_date'];
		$sql_array['up_date']      = $row['up_date'];
		
		$data_array[] = $sql_array;
	}
	
	//取出前台會員資料`front_member`
	$sql_mem = "SELECT * FROM `front_member`";
    $res_mem=$ODb->query($sql_mem) or die("載入資料出錯，請聯繫管理員。");
    while($row = mysql_fetch_array($res_mem))
    {
        $FM[$row['num']] = $row['c_name']; 
    }
	
	//計算總頁數
	$total_pg = 0;
	$sql_dsc = "SELECT count(*) as `get_num` FROM `message_board` ".$where_dsc;
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res)){
		if($row['get_num'] > 0 ){
			$num1 = $row['get_num']/$show_data_num;
			$num1_array = explode('.',$num1);
			$total_pg = $num1_array[0];
			if(count($num1_array)>1){
			$total_pg++;
			}
		}
	}
	
	
?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>留言版管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
  $('#ulcssmenu ul').hide();
	$('#ulcssmenu li a').click(
		function() {
			var openMe = $(this).next();
			var mySiblings = $(this).parent().siblings().find('ul');
			if (openMe.is(':visible')) {
				openMe.slideUp('normal');  
			} else {
				mySiblings.slideUp('normal');  
				openMe.slideDown('normal');
			}
	  }
	);
	  $('#ulcssmenu li[id="message_data"] ul').slideDown('fast');

});

function ck_value(){
	$('#form').submit();
}

function del(key_num,key_dsc){
	if(confirm("請確定是否刪除填報標題?\r\n"+key_dsc)){
	 $.ajax({
	    url: 'js_function/delfunction.php',
		data: {keyNum:key_num,tables:"<?php echo base64_encode('message_board');?>"},
	    error: function(xhr) {
			alert('Ajax request 發生錯誤');
	    },
	    success: function(response) {
			
			location.reload();
	    }
	  });}}
	  
function search_key_word()
{
	if($('#search_key_word').val() !="" )
	{	
		var key_word ="";
		if($('#search_key_word').val()!='')
		{
			key_word = $('#search_key_word').val();
		}	
		//將搜尋值編碼
		$.ajax({
					url: 'js_function/value_encode.php',
					
					data: {values:key_word},
					
					error: function(xhr)
					{
						alert('Ajax request 發生錯誤');
					},
					
					success: function(response)
					{
						//console.log(response);
						location.replace('?pg=<?php echo base64_encode("1");?>&s='+response); 
					}
		      });
	}
	else
	{
		alert("請輸入搜尋值!!");
	}
}
</script>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
	  <div class="breadcrumb">
			 <a href="c_message_data.php">留言版</a> 
	  </div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" />關於留言版::留言版</h1>
      <!--<div class="buttons"><a href="c_message_data_a.php" class="button">回覆填報說明</a></div>-->
    </div>
    <!-- 搜尋區域-->
	<div class="heading">
		<div class="search_input">搜尋值 : <input type="text" name="search_key_word" id="search_key_word" class="in_search" value="<?php echo $_GET['s']; ?>" /></div>
      <div class="buttons">
		<a href="#" class="button" onclick="search_key_word()">搜尋</a>
		<a href="#" class="button" onclick="location.replace('c_message_data.php')">清除</a>
	  </div>
	</div>	
	<!-- 搜尋區域 end -->
     <div class="content">
			<div id="tab-general">
				<table class="list">
				<tr>									
					<td>新增時間</td>
					<td>會員名稱<td/>
					<td>留言版標題</td>
					<td>是否回覆<td/>	
					<td align="left" width="150">功能操作</td>
				</tr>
				<?php 
					if(is_array($data_array)){	
					foreach($data_array as $my_data){	
				?>
				<tr>
					<td><?php echo $my_data['up_date']; ?></td>
					<td><?php echo $FM[$my_data['member_num']];?><td/>
					<td><?php echo $my_data['c_title']; ?></td>
					<td>
					    <?php if($my_data['is_view']=='0')
					          {
					                echo "未回覆";
					          }
					          elseif($my_data['is_view']=='1')
					          {
					                echo "已回覆";
					          }
					          
					     ?>
					<td/>
					<td align="left">
					<a href="c_message_data_a.php?num=<?php echo base64_encode($my_data['num']);?>&pg=<?php echo base64_encode($_GET['pg']);?>&s=<?php echo base64_encode($_GET['s']);?>" class="button">編輯</a>
					<a onclick="del('<?php echo base64_encode($my_data['num']);?>','<?php echo $my_data['c_title'];?>');" class="button">刪除</a>
					</td>
				</tr>
				<?php 
					}
				}else{	?>
				<td colspan="6" align="center">無資料！！</td>
				<?php } ?>
				</table>
			</div>
			<div class="page" align="center">
			 <?php 
			 
			 /**
			  * 製作經base64編碼後的分頁
			  */
			 
				if($total_pg >0){
						if(($now_pg-1) > 0){
						echo '
						<a href="?pg='.base64_encode($now_pg-1).'&s='.base64_encode($_GET['s']).'" > << </a>&nbsp;&nbsp;
						';
						}else{
						echo '
						<a href="?pg='.base64_encode("1").'&s='.base64_encode($_GET['s']).'" > << </a>&nbsp;&nbsp;';
						}
					 
					  if(($now_pg-5)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-5).'&s='.base64_encode($_GET['s']).'">'.($now_pg-5).'</a></u>&nbsp;&nbsp;';
					  }
					   if(($now_pg-4)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-4).'&s='.base64_encode($_GET['s']).'">'.($now_pg-4).'</a></u>&nbsp;&nbsp;';
					  }
					   if(($now_pg-3)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-3).'&s='.base64_encode($_GET['s']).'">'.($now_pg-3).'</a></u>&nbsp;&nbsp;';
					  }
					   if(($now_pg-2)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-2).'&s='.base64_encode($_GET['s']).'">'.($now_pg-2).'</a></u>&nbsp;&nbsp;';
					  }
					   if(($now_pg-1)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-1).'&s='.base64_encode($_GET['s']).'">'.($now_pg-1).'</a></u>&nbsp;&nbsp;';
					  }
					  
						echo $now_pg."&nbsp;&nbsp;";//目前頁面
						
					   if(($now_pg+1)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+1).'&s='.base64_encode($_GET['s']).'">'.($now_pg+1).'</a></u>&nbsp;&nbsp;';
					  }			  
					   if(($now_pg+2)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+2).'&s='.base64_encode($_GET['s']).'">'.($now_pg+2).'</a></u>&nbsp;&nbsp;';
					  }			  
					   if(($now_pg+3)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+3).'&s='.base64_encode($_GET['s']).'">'.($now_pg+3).'</a></u>&nbsp;&nbsp;';
					  }			  
					   if(($now_pg+4)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+4).'&s='.base64_encode($_GET['s']).'">'.($now_pg+4).'</a></u>&nbsp;&nbsp;';
					  }			  
					   if(($now_pg+5)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+5).'&s='.base64_encode($_GET['s']).'">'.($now_pg+5).'</a></u>&nbsp;&nbsp;';
					  }			  
					  
						if(($total_pg-$now_pg) > 0){
						echo '&nbsp;&nbsp;
						<a href="?pg='.base64_encode($now_pg+1).'&s='.base64_encode($_GET['s']).'" > >> </a>';
						}else{
						echo '&nbsp;&nbsp;
						<a href="?pg='.base64_encode($now_pg).'&s='.base64_encode($_GET['s']).'" > >> </a>';
						}
				}	  
		  ?>
		</div>		
    </div>
  </div>
</div>
</div>
<script language="javascript">
<?php
if($mg !=''){
echo 'alert("存檔完畢！！");';
}
?>
</script> 

<?php include("./layout/footer.php");?>
</body></html>