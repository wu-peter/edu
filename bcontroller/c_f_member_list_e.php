<?php
  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	
	
	if($_POST['send_data']=='HasPostValue' ){		
		$nowdate =  date("Y-m-d H:i",time());		
		$up_dsc ="update `front_member`
		set 
		`c_pw`='".base64_encode($_POST['c_pw'])."',
		`c_name`='".$_POST['c_name']."',
		`c_email`='".$_POST['c_email']."',
		`c_login_type`='".$_POST['c_login_type']."',
		`up_date`='".$nowdate."' 
		 where `num`='".$_POST['num']."'";
		$res=$ODb->query($up_dsc) or die("更新資料出錯，請聯繫管理員。");
		ri_jump("c_f_member_list.php?pg=".$_POST['pg']);
	}
	
	if($_GET['num'] !=''){
		//取出資料	
		$sql_dsc = "SELECT * FROM `front_member` WHERE `num`='".$_GET['num']."'";
		$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
		while($row = mysql_fetch_array($res)){
			$user_array['num'] = $row['num'];
			$user_array['c_pw'] = base64_decode($row['c_pw']);
			$user_array['c_name'] = $row['c_name'];
			$user_array['c_email'] = $row['c_email'];
			$user_array['c_login_type'] = $row['c_login_type'];
			$user_array['up_date'] = $row['up_date'];
		}
	}else{
		ri_jump("c_f_member_list.php");
	}
	
?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>一般會員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
  $('#ulcssmenu ul').hide();
	$('#ulcssmenu li a').click(
		function() {
			var openMe = $(this).next();
			var mySiblings = $(this).parent().siblings().find('ul');
			if (openMe.is(':visible')) {
				openMe.slideUp('normal');  
			} else {
				mySiblings.slideUp('normal');  
				openMe.slideDown('normal');
			}
	  }
	);
	  $('#ulcssmenu li[id="front_member"] ul').slideDown('fast');
	
});



function ck_value(){
var isGo = true;
var err_dsc = '';
var ck_array =  ["c_email","c_pw","c_login_type","c_name"];
var err_array =  ["請輸入電子郵件!","請輸入密碼!","請選擇是否能登入!","請輸入姓名!"];
var type_array =  ["text","text","text","text"];

for(var x=0;x< ck_array.length;x++){
	switch(type_array[x]){
		case "text":
			if($('#'+ck_array[x]).val() ==''){
				err_dsc = err_dsc + err_array[x] +'\r\n';
				isGo = false;				
			}
		break;
		case "number":
			if(!$.isNumeric($('#'+ck_array[x]).val()) ){
				err_dsc = err_dsc + err_array[x] +'\r\n';
				isGo = false;				
			}		
		break;
	}
}		
if($('#c_pw').val().length<8){
		err_dsc = err_dsc + "密碼長度必須大於8碼!" +'\r\n';
		isGo = false;
	}
	if(isGo){
		$('#form').submit();
	}
	
	if(err_dsc !=''){
		alert(err_dsc);
	}
}
</script>
</head>
<body>
<?php include 'layout/head.php' ?>

<div id="container">

<?php
include('layout/menu_left.php');//載入左邊選單
?>  
  
  <div id="content">
  <div class="breadcrumb">
		<a href="c_f_member_list.php">一般會員管理</a> :: <a href="c_f_member_list_e.php">編輯會員</a>
	</div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" /> 編輯會員</h1>
      <div class="buttons"><a onclick="ck_value()" class="button">存檔</a><a href="c_f_member_list.php?pg=<?php echo $_GET['pg'];?>" class="button">取消</a></div>
    </div>
    <div class="content">
	    <form action="c_f_member_list_e.php" method="post" enctype="multipart/form-data" id="form">
			<div id="tab-general">
				<table class="form">
				<tr>
					<td colspan="2">*字號欄位必須填寫</td>
				</tr>
				<tr>
					<td>*電子郵件</td>
					<td><input type="text" name="c_email" id="c_email" size="100" value="<?php echo $user_array['c_email']; ?>"></td>
				</tr>
				<tr>
					<td>*密碼</td>
					<td><input type="password" name="c_pw" id="c_pw" size="100" value="<?php echo $user_array['c_pw']; ?>"></td>
				</tr>
				<tr>
					<td>*姓名</td>
					<td><input type="text" name="c_name" id="c_name" size="100" value="<?php echo $user_array['c_name']; ?>"></td>
				</tr>
				<tr>
					<td>是否能登入</td>
					<td>
						<label><input type="radio" name="c_login_type"  value="0"   <?php if($user_array['c_login_type']=='0'){echo "checked";} ?>  />是</label>
						<label><input type="radio" name="c_login_type" value="1"   <?php if($user_array['c_login_type']=='1'){echo "checked";} ?> />否</label>

					</td>
				</tr>
				</table>
			</div>
			<input type="hidden" name="send_data" value="HasPostValue">
			<input type="hidden" name="num" value="<?php echo $user_array['num']; ?>">
			<input type="hidden" name="pg" id="pg" value="<?php echo $_GET['pg']; ?>">			
			
		</form>	
    </div>
  </div></div>
</div>
<?php include("./layout/footer.php");?>
</body></html>