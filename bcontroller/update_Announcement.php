<?php
  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>後台會員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>

<!--月曆-->
<script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/jquery/custom_language_zh.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
  $('#ulcssmenu ul').hide();
	$('#ulcssmenu li a').click(
		function() {
			var openMe = $(this).next();
			var mySiblings = $(this).parent().siblings().find('ul');
			if (openMe.is(':visible')) {
				openMe.slideUp('normal');  
			} else {
				mySiblings.slideUp('normal');  
				openMe.slideDown('normal');
			}
	  }
	);
	  $('#ulcssmenu li[id="front_member"] ul').slideDown('fast');

});

function ck_value(){
	
	
	$('#form').submit();
	}
</script>
</head>
<body>
<?php include 'layout/head.php' ?>

<div id="container">

<?php
include('layout/menu_left.php');//載入左邊選單
?>  
  
  <div id="content">
  <div class="breadcrumb">
		<a href="">更新公告</a> :: <a href="">更新公告</a>
	</div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" /> 更新公告</h1>
    </div>
    <div class="content">
	    <form action="" method="post" enctype="multipart/form-data" id="form">
			<div id="tab-general">
				<table class="form">
				<tr style="background:#FFFFFF">
					<td colspan="2" >系統於2016/04/06新增三項功能如下：<br><br>
一、「職缺管理」功能內新增「實際職缺數」及「媒合成功人數」欄位，公告職缺如超過報名截止日期，則需填報此欄位。<br>
二、「職缺管理」功能內新增職缺資料時，可透過「畫面預覽」按鈕預覽職缺資料。<br>
三、「職缺管理」功能內新增「學術專長」欄位。<br>
<br>
</td>
				</tr>				
				</table>
			</div>
			<input type="hidden" name="send_data" value="HasPostValue">
			
			
		</form>	
    </div>
  </div></div>
</div>
<?php include("./layout/footer.php");?>
</body></html>