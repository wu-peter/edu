<?php
  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	//die($_POST['class_name']);
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	
	foreach($_GET as $key => $value){
		$_GET[$key] = decode_dowith_sql($value);
	}
	
	
	if($_GET['pg']=='' || !is_numeric(base64_decode($_GET['pg'])))
	{	
			ri_jump("?pg=".base64_encode("1")."&s=".$_GET['s']);
	}

	//Url解碼
	foreach($_GET as $key => $value)
	{
		$_GET[$key] = base64_decode($value);
	}
	
	
	if($_GET['num'] !='')
	{
		$sql_dsc = "select * from `back_member` 
				  	Left join `back_group_data` on `back_member`.`c_group_num` = `back_group_data`.`num` 
					Left join  `school_data` on `back_member`.`c_school_num` = `school_data`.`num` where `c_login_type` = '1' and `back_member`.`num` = '".$_GET['num']."'";
		//die($sql_dsc);
		$res = $ODb -> query($sql_dsc) or die("請與管理員聯絡!!");
		while( $row = mysql_fetch_array($res) )
		{
			$sql['num']          = $row['num'];
			$sql['c_login_name'] = $row['c_login_name'];
			$sql['school_name'] = $row['school_name'];			
			$sql['c_name'] = $row['c_name'];			
		}
		
	}
	
	
	

?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>後台會員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.js"></script>
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
  $('#ulcssmenu ul').hide();
	$('#ulcssmenu li a').click(
		function() {
			var openMe = $(this).next();
			var mySiblings = $(this).parent().siblings().find('ul');
			if (openMe.is(':visible')) {
				openMe.slideUp('normal');  
			} else {
				mySiblings.slideUp('normal');  
				openMe.slideDown('normal');
			}
	  }
	);
	  $('#ulcssmenu li[id="back_member"] ul').slideDown('fast');
	
});

function changepw(key_num)
{
	if(confirm("請確認是否還原密碼"))
	{

	 $.ajax({
	    url: 'js_function/updatefunction.php',
		data: {keyNum:key_num,tables:"<?php echo base64_encode('back_member_list');?>"},
	    error: function(xhr) {
			alert('Ajax request 發生錯誤');
	    },
	    success: function(response) {			
			window.location.replace('lack_account.php');
	    }
	  });
	  
    }
}
</script>
</head>
<body>
<?php include 'layout/head.php' ?>

<div id="container">

<?php
include('layout/menu_left.php');//載入左邊選單
?>  
  
  <div id="content">
  <div class="breadcrumb">
		<a href="lack_account.php">鎖定帳號管理</a> ::
	</div>
    <div class="box">
    <div class="heading">
      <div class="buttons"><a onclick="history.back()" class="button">上一頁</a></div>
    </div>
    <div class="content">
	    <form action="lack_account.php" method="post" enctype="multipart/form-data" id="form">
			<div id="tab-general">
				<table class="form">
				<tr>
					<td>學校名稱</td>
					<td>
						<?php echo$sql['school_name'];?>							
					</td>
				</tr>
				<tr>
					<td>學校系所</td>
					<td>
						<?php echo $sql['c_name'];?>							
					</td>
				</tr>
				<tr>
					<td>帳號</td>
					<td><?php echo $sql['c_login_name'];?></td>
				</tr>
				<tr>
					<td>解鎖帳號並還原密碼</td>
					<td><a onclick = "changepw('<?php echo base64_encode($_GET['num']);?>')" class="button" id = "button"><label><font color="red" size ="5">解鎖帳號並還原密碼請按此</font></label></a></td>
				</tr>
							
				</table>
			</div>
			<input type="hidden" name="send_data" value="HasPostValue">
			<input type="hidden" name="num" value="<?php echo $sql['num']; ?>">
			<input type="hidden" name="pg" id="pg" value="<?php echo $_GET['pg']; ?>">
			<input type="hidden" name="c_type" value="<?php echo $sql['c_type']; ?>">
			<input type="hidden" name="f_num" value="<?php echo $sql['f_num']; ?>">	
			<input type="hidden" name="c_class_id" value="<?php echo $sql['c_class_id']; ?>">			
		</form>	
    </div>
  </div></div>
</div>
<?php include("./layout/footer.php");?>
</body></html>