<?php
  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	
	//SQL injection
	foreach($_GET as $key => $value){
		$_GET[$key] = decode_dowith_sql($value);
	}
	
	//判斷pg參數
	if($_GET['pg']=='' || !is_numeric(base64_decode($_GET['pg'])))
	{	
			ri_jump("?pg=".base64_encode("1")."&s=".$_GET['s']);
	}

	//Url解碼
	foreach($_GET as $key => $value)
	{
		$_GET[$key] = base64_decode($value);
	}
	
	$show_data_num = 50;//一頁顯示多少筆資料
	if($_GET['pg']== '1' ){
		$now_pg = 1;
		$pg_dsc = ' limit '.$show_data_num;
	}else{
		$now_pg = $_GET['pg'];
		$pg_dsc = ' limit '.(($_GET['pg']-1) * $show_data_num) .','.$show_data_num ;
	}
	
	//加入標題搜尋
	$where_dsc="";
	if($_GET['s'] !='')
	{
		$where_dsc = " where `c_title` like '%".$_GET['s']."%' ";
	}
	
	//取出資料	
	$sql_dsc = "SELECT *FROM  `pic_control_data`".$where_dsc." order by `num` ".$pg_dsc;
	
	
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res))
	{
		$sql_array['num'] = $row['num'];
		$sql_array['c_title'] = $row['c_title'];
		$sql_array['c_save_dir'] = $row['c_save_dir'];
		$sql_array['c_save_name'] = $row['c_save_name'];
		$sql_array['c_url'] = $row['c_url'];
		$sql_array['up_date'] = $row['up_date'];
		
		if($row['c_login_type']==0){
			$sql_array['has_log'] = "是";
		}else{
			$sql_array['has_log'] = "否";
		}
		$data_array[] = $sql_array;
	}
		
	//計算總頁數
	$total_pg = 0;
	$sql_dsc = "SELECT count(*) as `get_num` FROM `pic_control_data`".$where_dsc;
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res)){
		if($row['get_num'] > 0 ){
			$num1 = $row['get_num']/$show_data_num;
			$num1_array = explode('.',$num1);
			$total_pg = $num1_array[0];
			if(count($num1_array)>1){
			$total_pg++;
			}
		}
	}
	
?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>一般會員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
  $('#ulcssmenu ul').hide();
	$('#ulcssmenu li a').click(
		function() {
			var openMe = $(this).next();
			var mySiblings = $(this).parent().siblings().find('ul');
			if (openMe.is(':visible')) {
				openMe.slideUp('normal');  
			} else {
				mySiblings.slideUp('normal');  
				openMe.slideDown('normal');
			}
	  }
	);
	  $('#ulcssmenu li[id="front_member"] ul').slideDown('fast');

});

function ck_value(){
	$('#form').submit();
}

function del(key_num,key_dsc)
{
	if(confirm("請確認是否刪除下列法規項目的資料!!\r\n"+key_dsc))
	{
	 $.ajax({
	    url: 'js_function/delfunction.php',
	    
		data: {keyNum:key_num,tables:"<?php echo base64_encode('pic_control_set');?>"},
		
	    error: function(xhr) 
	    {
			alert('Ajax request 發生錯誤');
	    },
	    
	    success: function(response) 
	    {			
			location.reload();
	    }
	   });
	}
}

function search_key_word()
{
	if($('#search_key_word').val() !="" )
	{	
		var key_word ="";
		if($('#search_key_word').val()!='')
		{
			key_word = $('#search_key_word').val();
		}	
		//將搜尋值編碼
		$.ajax({
					url: 'js_function/value_encode.php',
					
					data: {values:key_word},
					
					error: function(xhr)
					{
						alert('Ajax request 發生錯誤');
					},
					
					success: function(response)
					{
						//console.log(response);
						location.replace('?pg=<?php echo base64_encode("1");?>&s='+response); 
					}
		      });
	}
	else
	{
		alert("請輸入搜尋值!!");
	}
}
</script>
</head>
<body>
<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
	  <div class="breadcrumb">
			 <a href="">相關連結</a> 
	  </div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" /> 相關連結::<a href="c_pic_control_set.php">連結管理</a></h1>
      <div class="buttons"><a href="c_pic_control_set_a.php" class="button">新增相關連結</a></div>
    </div>
     <!-- 搜尋區域-->
	<div class="heading">
		<div class="search_input">搜尋值 : <input type="text" name="search_key_word" id="search_key_word" class="in_search" value="<?php echo $_GET['s']; ?>" /></div>
      <div class="buttons">
		<a href="#" class="button" onclick="search_key_word()">搜尋</a>
		<a href="#" class="button" onclick="location.replace('c_pic_control_set.php')">清除</a>
	  </div>
	</div>	
	<!-- 搜尋區域 end -->	
     <div class="content">
			<div id="tab-general">
				<table class="list">
				<tr>
					<!-- <td>順序</td> -->
					<td>相關連結標題</td>
					<td>上傳圖檔</td>
					<td align="left" width="150">功能操作</td>
				</tr>
				<?php 
					if(is_array($data_array)){	
					foreach($data_array as $my_data){	
				?>
				<tr>
					<!-- <td><?php echo $my_data['num']; ?></td>  -->
					<td><?php echo $my_data['c_title']; ?></td>
					<td><a href="<?php echo $my_data['c_url']?>" target="_blank"><img src="<?php echo $my_data['c_save_dir'].$my_data['c_save_name'];?>" width="50" width="50"></a></td>
					<td align="left">
					<a href="c_pic_control_set_e.php?num=<?php echo base64_encode($my_data['num']);?>&pg=<?php echo base64_encode($_GET['pg']);?>&s=<?php echo base64_encode($_GET['s']);?>" class="button">編輯</a>
					<a onclick="del('<?php echo base64_encode($my_data['num']);?>','<?php echo $my_data['c_title']; ?>');" class="button">刪除</a>
					</td>
				</tr>
				<?php 
					}
				}else{	?>
				<td colspan="4" align="center">無資料！！</td>
				<?php } ?>
				</table>
			</div>
			<div class="page" align="center">
			 <?php 
			 
			 /**
			  * 製作經base64編碼後的分頁
			  */
			 
				if($total_pg >0){
						if(($now_pg-1) > 0){
						echo '
						<a href="?pg='.base64_encode($now_pg-1).'&s='.base64_encode($_GET['s']).'" > << </a>&nbsp;&nbsp;
						';
						}else{
						echo '
						<a href="?pg='.base64_encode("1").'&s='.base64_encode($_GET['s']).'" > << </a>&nbsp;&nbsp;';
						}
					 
					  if(($now_pg-5)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-5).'&s='.base64_encode($_GET['s']).'">'.($now_pg-5).'</a></u>&nbsp;&nbsp;';
					  }
					   if(($now_pg-4)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-4).'&s='.base64_encode($_GET['s']).'">'.($now_pg-4).'</a></u>&nbsp;&nbsp;';
					  }
					   if(($now_pg-3)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-3).'&s='.base64_encode($_GET['s']).'">'.($now_pg-3).'</a></u>&nbsp;&nbsp;';
					  }
					   if(($now_pg-2)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-2).'&s='.base64_encode($_GET['s']).'">'.($now_pg-2).'</a></u>&nbsp;&nbsp;';
					  }
					   if(($now_pg-1)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-1).'&s='.base64_encode($_GET['s']).'">'.($now_pg-1).'</a></u>&nbsp;&nbsp;';
					  }
					  
						echo $now_pg."&nbsp;&nbsp;";//目前頁面
						
					   if(($now_pg+1)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+1).'&s='.base64_encode($_GET['s']).'">'.($now_pg+1).'</a></u>&nbsp;&nbsp;';
					  }			  
					   if(($now_pg+2)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+2).'&s='.base64_encode($_GET['s']).'">'.($now_pg+2).'</a></u>&nbsp;&nbsp;';
					  }			  
					   if(($now_pg+3)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+3).'&s='.base64_encode($_GET['s']).'">'.($now_pg+3).'</a></u>&nbsp;&nbsp;';
					  }			  
					   if(($now_pg+4)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+4).'&s='.base64_encode($_GET['s']).'">'.($now_pg+4).'</a></u>&nbsp;&nbsp;';
					  }			  
					   if(($now_pg+5)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+5).'&s='.base64_encode($_GET['s']).'">'.($now_pg+5).'</a></u>&nbsp;&nbsp;';
					  }			  
					  
						if(($total_pg-$now_pg) > 0){
						echo '&nbsp;&nbsp;
						<a href="?pg='.base64_encode($now_pg+1).'&s='.base64_encode($_GET['s']).'" > >> </a>';
						}else{
						echo '&nbsp;&nbsp;
						<a href="?pg='.base64_encode($now_pg).'&s='.base64_encode($_GET['s']).'" > >> </a>';
						}
				}	  
		  ?>
		</div>		
    </div>
  </div>
</div>
</div>
<script language="javascript">
<?php
if($mg !=''){
echo 'alert("存檔完畢！！");';
}
?>
</script> 

<?php include("./layout/footer.php");?>
</body></html>