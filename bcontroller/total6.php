<?php
//包含需求檔案 ------------------------------------------------------------------------
include("./class/common_lite.php");
session_start();
if ($_SESSION['zeroteamzero'] != 'IS_LOGIN') {
	ri_jump("login.php");
}

//宣告變數 ----------------------------------------------------------------------------
$ODb = new run_db("mysql", 3306);      //建立資料庫物件

//取出學術專長資料
$sql_dsc = "SELECT * FROM  `code_table` where MainCode='Academic_expertise' order by SubCode";
$res = $ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
while ($row = mysql_fetch_array($res)) {
	$Academic_expertise_array['MainCode'] = $row['MainCode'];
	$Academic_expertise_array['SubCode'] = $row['SubCode'];
	$Academic_expertise_array['SubDesc'] = $row['SubDesc'];
	$Academic_expertise_type_array[] = $Academic_expertise_array;
}

$member_categrory =
	array(
		"0" => "",
		"1" => "編內",
		"2" => "編外",
		// Mod by 心詠 20190814 客戶0725需求，將此選項改為『其他(如編內或編外、或其他等)』
		// "3" => "編內或編外"
		"3" => "其他(如編內或編外、或其他等)"
	);

?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">

<head>
	<meta charset="UTF-8" />
	<title>統計資料</title>

	<!--月曆-->


	<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
	<script src="js/jquery/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="js/jquery/jquery-ui.js"></script>
	<script type="text/javascript" src="js/jquery/custom_language_zh.js"></script>
	<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.16.custom.css" />

	<script type="text/javascript">
		//-----------------------------------------
		// Confirm Actions (delete, uninstall)
		//-----------------------------------------
		$(document).ready(
			function() {

				$('#ulcssmenu ul').hide();

				$('#ulcssmenu li a').click(
					function() {
						var openMe = $(this).next();
						var mySiblings = $(this).parent().siblings().find('ul');
						if (openMe.is(':visible')) {
							openMe.slideUp('normal');
						} else {
							mySiblings.slideUp('normal');
							openMe.slideDown('normal');
						}
					}
				);

				$('#ulcssmenu li[id="total"] ul').slideDown('fast');

			}
		);


		jQuery(function($) {

			$('#start_dt').datepicker({
				dateFormat: 'yy-mm-dd',
				changeYear: true,
				changeMonth: true
			});
			$('#end_dt').datepicker({
				dateFormat: 'yy-mm-dd',
				changeYear: true,
				changeMonth: true
			});


		});
	</script>
	<?php //include '../template/header.inc'; 
	?>
</head>

<body>

	<?php include 'layout/head.php' ?>
	<div id="container">
		<?php
		include('layout/menu_left.php'); //載入左邊選單
		?>
		<div id="content">
			<div class="breadcrumb">

			</div>
			<div class="box">
				<div class="heading">
					<h1><img src="image/category.png" alt="" />統計資料::私立學校職缺統計</h1>

				</div>
				<div class="content">
					<div id="tab-general">
						<form action="./dwExcel/dwExcel_6.php" method="post" id="form_1" target="_blank">
							<table class="list">
								<tr>
									<td>學術領域</td>
									<td width="120px">人員類別</td>
									<td>起始時間</td>
									<td>結束時間</td>
									<td width="100px">下載格式</td>
									<td align="left" width="150px">功能操作</td>

								<tr>
									<td>
										<div id="Academic_expertise">
											<select name="Academic_expertise">
												<?php
												foreach ($Academic_expertise_type_array as $value) {
													if ('0' == $value['SubCode']) {
														echo '<option value="' . $value['SubCode'] . '" selected>' . $value['SubDesc'] . '</option>';
													} else {
														echo '<option value="' . $value['SubCode'] . '" >' . $value['SubDesc'] . '</option>';
													}
												}
												?>
											</select>
										</div>
									</td>
									<td width="120px">
										<div id="member_category">
											<select name="member_category" style="width:100px">
												<?php
												foreach ($member_categrory as $key => $value) {
													echo '<option value="' . $key . '" selected>' . $value . '</option>';
												}
												?>
											</select>
										</div>
									</td>
									<td><input type="text" name="start_dt" id="start_dt" value="<?php echo date("Y-m-d", time()); ?>" class="datepicker"></td>
									<td><input type="text" name="end_dt" id="end_dt" value="<?php echo date("Y-m-d", time()); ?>" class="datepicker"></td>
									<td width="50px">
										<select name="file_type" style="width:75px">
											<option value="1" selected>OSD</option>
											<option value="2">EXCEL</option>
										</select>
									</td>
									<td align="left">
										<a href="#" class="button" onclick="$('#form_1').submit()">匯出</a>

									</td>
								</tr>

							</table>
						</form>
					</div>

				</div>
			</div>
		</div>
	</div>


	<?php include("./layout/footer.php"); ?>
</body>

</html>