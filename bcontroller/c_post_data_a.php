<?php
//包含需求檔案 ------------------------------------------------------------------------
include("./class/common_lite.php");
session_start();
if ($_SESSION['zeroteamzero'] != 'IS_LOGIN') {
	ri_jump("login.php");
}
//die(print_r($_SESSION));
//宣告變數 ----------------------------------------------------------------------------
$ODb = new run_db("mysql", 3306);      //建立資料庫物件
$upFileFload = "./upFile/" . date("Ymd", time());
$upFile = $upFileFload . "/";

//上傳資料	
//`back_group_data_num` ='".$_SESSION['cgroupnum']."',
if (isset($_POST['send_data']) && $_POST['send_data'] == 'HasPostValue') {
	if (is_array($_POST)) {
		foreach ($_POST as $key => $value) {
			$_POST[$key] = decode_dowith_sql($value);
		}
	}
	//取出編輯者的資料

	$sql_department = "select * from `back_member` where `num`='" . $_SESSION['num'] . "'";
	$res = $ODb->query($sql_department) or die("更新資料出錯，請聯繫管理員。");
	while ($row = mysql_fetch_array($res)) {
		$b_member_group_num = $row['c_group_num'];

		//若用人單位有值，代表人事室幫忙挑選科系，所以要針對科系名稱去找出對應的num
		if ($_POST['school_depart_num'] != '') {
			$sql_dsc = "select `num` from `back_group_data` where `c_school_num`='" . $_SESSION['cschoolnum'] . "' and `c_name`='" . $_POST['school_depart_num'] . "' ";
			$res_x = $ODb->query($sql_dsc) or die("更新資料出錯，請聯繫管理員。");
			while ($row_x = mysql_fetch_array($res_x)) {
				$b_member_group_num = $row_x['num']; //選科系才有值					
			}
		}
	}

	$nowdate =  date("Y-m-d H:i", time());
	$_POST['c_dsc'] = str_replace("twRRtw", ">", $_POST['c_dsc']);
	$_POST['c_dsc'] = str_replace("twLLtw", "<", $_POST['c_dsc']);
	//新增欄位 學術專長 mod by 志豪 20160215
	$up_dsc = "insert into `post_data` set `c_title`             ='" . $_POST['c_title'] . "',
											  `c_dsc`               ='" . $_POST['c_dsc'] . "',
											  `c_postdate`          ='" . $_POST['c_postdate'] . "',
											  `c_enddate`           ='" . $_POST['c_enddate'] . "',
											  `contact_name`        ='" . $_POST['contact_name'] . "',
											  `contact_phone`       ='" . $_POST['contact_phone'] . "',
											  `contact_email`       ='" . $_POST['contact_email'] . "',
											  `other_url`           ='" . $_POST['other_url'] . "',
											  `Academic_expertise`  ='" . $_POST['Academic_expertise'] . "',
											  `member_type`         ='" . $_POST['member_type'] . "',
											  `c_school_num`        ='" . $_SESSION['cschoolnum'] . "',
											  `school_depart_num`   ='" . $_POST['school_depart_num'] . "',
											  `b_member_group_num`  ='" . $b_member_group_num . "',
											  `b_member_num`        ='" . $_SESSION['num'] . "',
											  `up_date`             ='" . $nowdate . "',		  
											  `add_date`            ='" . $nowdate . "',		  
											  `tw_counties_num`     ='" . $_POST['tw_counties_num'] . "',
											  `location`            ='" . decode_dowith_sql($_POST['location']) . "'";


	//die($up_dsc);                               
	$res = $ODb->query($up_dsc) or die("更新資料出錯，請聯繫管理員。");




	//處理上傳檔案
	$getID = mysql_insert_id();
	foreach ($_FILES as $key => $weight_name) {
		$file_type = explode('_', $key);

		if ($file_type[0] == 'e') {
			$sql_dsc = "_en";
		} else {
			$sql_dsc = "";
		}
		$file_type_dsc = explode(".", basename($weight_name['name']));

		$mtime = explode(" ", microtime());
		$startTime = $mtime[1] . substr($mtime[0], 2);
		$new_name =  $startTime . "." . $file_type_dsc[1];
		$uploadfile = $upFile . $new_name;
		if (move_uploaded_file($weight_name['tmp_name'], $uploadfile)) {
			$sql = "insert into `file_data` set `table_num`='" . $getID . "',`c_save_dir`='" . $upFile . "',`table_name`='post_data" . $sql_dsc . "',`file_name`='" . $weight_name['name'] . "',`save_name`='" . $new_name . "',`up_date`='" . date("Y-m-d H:i", time()) . "'";
			$res = $ODb->query($sql) or die("更新資料出錯，請聯繫管理員。");
		}
	}

	//處理職缺名額

	for ($x = 0; $x < $_POST['lack_num']; $x++) {
		if ($_POST['lack_type_sw_' . $x] != '' && $_POST['lack_paople_num_' . $x] != '') {
			if ($_POST['lack_type_sw_' . $x] == "other") {
				$pro_title = $_POST['other_' . $x];
			} else {
				$pro_title = '';
			}
			$sql = "insert into `lack_data` set `Complete_number`='" . $_POST['Complete_number_' . $x] . "',`Real_people_number`='" . $_POST['Real_people_number_' . $x] . "',`lack_type_num`='" . $_POST['lack_type_sw_' . $x] . "',`post_data_num`='" . $getID . "',`people_number`='" . $_POST['lack_paople_num_' . $x] . "',`pro_title`='" . $pro_title . "',`establishment_type`='" . $_POST['establishment_' . $x] . "',`up_date`='" . date("Y-m-d H:i", time()) . "' ";
			$res = $ODb->query($sql) or die("更新資料出錯，請聯繫管理員。");
		}
	}


	ri_jump("c_post_data.php");
}

//新增取出編輯者資料  mod by 志豪 20160215
$sql_dsc = "SELECT * FROM `post_data` WHERE `b_member_num`='" . @$_SESSION['num'] . "' " . @$and_dsc;
$res = $ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
while ($row = mysql_fetch_array($res)) {

	$user_array['num']             = $row['num'];
	$user_array['c_title']         = $row['c_title'];
	$user_array['c_dsc']           = $row['c_dsc'];
	$user_array['c_postdate']      = $row['c_postdate'];
	//新增欄位 學術專長 mod by 志豪 20160215
	$user_array['Academic_expertise']  = $row['Academic_expertise'];
	$user_array['school_depart_num']   = $row['school_depart_num'];
	$user_array['c_enddate']           = $row['c_enddate'];
	$user_array['up_date']             = $row['up_date'];
	$user_array['tw_counties_num']     = $row['tw_counties_num'];
	$user_array['location']            = $row['location'];
	$user_array['contact_name']        = $row['contact_name'];
	$user_array['contact_phone']       = $row['contact_phone'];
	$user_array['contact_email']       = $row['contact_email'];
	$user_array['other_url']           = $row['other_url'];
	//新增 mod by 志豪 20160215
	$b_member_num                  = $row['b_member_num'];
	$c_school_num                  = $row['c_school_num'];
	// 新增 人員類別 by 心詠 20190311
	$user_array['member_type']     = $row['member_type'];
}
//新增取出學校  mod by 志豪 20160215
$sql_dsc = "SELECT * FROM  `school_data` order by `num` ";
$res = $ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
while ($row = mysql_fetch_array($res)) {
	$school_data_array[$row['num']]     = $row['school_name']; //學校名稱
	$school_location_array[$row['num']] = $row['tw_counties_num']; //地點
	$school_adress_array[$row['num']]   = $row['school_adress'];   //地址
	$school_url_array[$row['num']]      = $row['school_url'];
}
//新增取出系所  mod by 志豪 20160215
$sql_dsc = "SELECT * FROM  `back_group_data` order by `num` ";
$res = $ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
while ($row = mysql_fetch_array($res)) {
	$college_depart_array_P[$row['num']] = $row['c_name'];
}
//新增back_member編輯者資料 mod by 志豪 20160215
$sql_dsc = "select * from `back_member`";
$res = $ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
while ($row = mysql_fetch_array($res)) {
	$back_member[$row['num']] = $row['c_group_num'];
}

//die(var_dump($_SESSION));
//用編輯者學校，找出學校系所
$sql_query = "select * from `back_group_data` where`c_school_num` = '" . $_SESSION['cschoolnum'] . "' and `c_type` = '3'";
$res = $ODb->query($sql_query) or die("載入資料出錯，請聯繫管理員。");
while ($row = mysql_fetch_array($res)) {
	$school_depart['num']        = $row['num'];
	$school_depart['c_class_id'] = $row['c_class_id'];
	$school_depart['c_name']     = $row['c_name'];
	$school_depart_array[] = $school_depart;
}


//取出縣市
$tw_counties = "select * from `tw_counties` ";
$res = $ODb->query($tw_counties) or die("載入資料出錯，請聯繫管理員。");
while ($row = mysql_fetch_array($res)) {
	$sql_array['num']           = $row['num'];
	$sql_array['counties_name'] = $row['counties_name'];
	$tw_counties_array[]        = $sql_array;
}

//取出職務缺額類別
$sql_array = array();
$sql_dsc = "SELECT *FROM  `lack_type` order by `c_order`+0 ";
$res = $ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
while ($row = mysql_fetch_array($res)) {
	$sql_array['num'] = $row['num'];
	$sql_array['c_name'] = $row['c_name'];
	$lack_type_array[] = $sql_array;
}

//取出使用者資料
$sql_dsc = "SELECT * FROM  `back_member` where num='" . @$_SESSION['num'] . "'";
$res = $ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
while ($row = mysql_fetch_array($res)) {
	$my_user_data['c_name'] = $row['c_name'];
	$my_user_data['c_main_phone'] = $row['c_main_phone'];
	$my_user_data['c_email'] = $row['c_email'];
	$c_school_num = $row['c_school_num'];
}

//取出使用者所在學校的科系名稱
$availableTags_Data = '';
$sql_dsc = "SELECT * FROM  `back_group_data` where `c_school_num`='" . $c_school_num . "' and `c_class_id`>'' ";
$res = $ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
while ($row = mysql_fetch_array($res)) {
	$availableTags_Data .= '"' . $row['c_name'] . '",';
}
if ($availableTags_Data != '') {
	$availableTags_Data = substr($availableTags_Data, 0, -1);
}
//取出學術專長資料
$sql_dsc = "SELECT * FROM  `code_table` where MainCode='Academic_expertise' order by SubCode";
$res = $ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
while ($row = mysql_fetch_array($res)) {
	$Academic_expertise_array['MainCode'] = $row['MainCode'];
	$Academic_expertise_array['SubCode'] = $row['SubCode'];
	$Academic_expertise_array['SubDesc'] = $row['SubDesc'];
	$Academic_expertise_type_array[] = $Academic_expertise_array;
}

?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">

<head>
	<meta charset="UTF-8" />
	<title>職缺管理</title>
	<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
	<script src="js/jquery/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="js/jquery/jquery-ui.js"></script>
	<script type="text/javascript" src="js/jquery/custom_language_zh.js"></script>
	<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.16.custom.css" />

	<script type="text/javascript">
		//-----------------------------------------
		// Confirm Actions (delete, uninstall)
		//-----------------------------------------
		$(document).ready(
			function() {

				$('#ulcssmenu ul').hide();
				$('#ulcssmenu li a').click(
					function() {
						var openMe = $(this).next();
						var mySiblings = $(this).parent().siblings().find('ul');
						if (openMe.is(':visible')) {
							openMe.slideUp('normal');
						} else {
							mySiblings.slideUp('normal');
							openMe.slideDown('normal');
						}
					}
				);

				$('#ulcssmenu li[id="job"] ul').slideDown('fast');
				$("#school_depart_num").autocomplete({
					source: './js_function/autocomplete.php',
					minLength: 1
				});
			}
		);
		var availableTags = [<?php echo $availableTags_Data; ?>];
		var nowFileNum = 1;
		errorfiletype = "";
		//mod by 志豪 20160310新增判斷 實際職缺數<媒合成功人數 時跳出訊息
		function ck_value() {

			var isGo = true;
			var err_dsc = '';
			var ck_array = ["c_title", "c_postdate", "c_enddate", "c_dsc", "up_date", "location", "contact_name", "contact_email", "school_depart_num", "number"];
			var err_array = ["請輸入公告主旨!", "請輸入職缺公告日期", "請輸入報名截止日期", "請輸入內文！", "請輸入更新日期！！", "請輸入地點", "請輸入聯絡人姓名!!", "請輸入聯絡人E-mail!!", "請檢查用人單位是否輸入正確!!", "「媒合成功人數」應小於或等於「實際職缺數」!"];
			var type_array = ["text", "text", "text", "ckedit", "text", "text", "text", "text", "userinput", "number", "number"];


			for (var x = 0; x < ck_array.length; x++) {
				switch (type_array[x]) {
					case "userinput":
						<?php
						/*	用人單位停止檢查輸入科系是否有跟資料庫內的科系資料相符合*/
			if( $_SESSION['userlogintype']=='2' ){?>
			var get_value = $('#school_depart_num').val();
			if(availableTags.indexOf(get_value)>0){
				
			}else{		
					err_dsc = err_dsc + err_array[x] +'\r\n';
					isGo = false;
			}
			<?php } 
			
						?>
						break;
					case "text":
						if ($('#' + ck_array[x]).val() == '') {
							err_dsc = err_dsc + err_array[x] + '\r\n';
							isGo = false;
						}
						break;
					case "ckedit":
						var dsc = encodeURI(editor.document.getBody().getText());
						if (dsc == '%0A') { //cdedit 無輸入時自動會補入%0A
							err_dsc = err_dsc + err_array[x] + '\r\n';
							isGo = false;
						}
						break;
					case "number":

						for (i = 0; i < lack_type_num; i = i + 1) {
							try {
								if (parseInt(document.getElementById("Real_people_number_" + i).value) < parseInt(document.getElementById("Complete_number_" + i).value)) {
									err_dsc = err_dsc + err_array[x] + '\r\n';
									isGo = false;
								}
							} catch (e) {}
						}

						break;
				}
			}
			for (i = 0; i < lack_type_num; i = i + 1) {
				try {
					if (document.getElementById("Real_people_number_" + i).value == "") {
						err_dsc = err_dsc + '實際職缺人數不可空白' + '\r\n';
						isGo = false;
					} else if (document.getElementById("Real_people_number_" + i).value != "" &&
						parseInt(document.getElementById("lack_paople_num_" + i).value) != parseInt(document.getElementById("Real_people_number_" + i).value)) {
						err_dsc = err_dsc + '職缺名額 和 實際職缺人數不相同，請確認是否正確' + '\r\n';
						isGo = false;
					}
				} catch (e) {}
			}
			if (errorfiletype != '') {
				err_dsc += errorfiletype + '\r\n';
				isGo = false;
			}

			if (isGo) {
				$('#form').submit();
			}

			if (err_dsc != '') {
				alert(err_dsc);
			}
		}


		function addOtherFile() {
			var addnum = $('#addNum').val();
			if (!$.isNumeric(addnum)) {
				$('#addNum').focus();
				alert('只能輸入數字');
			} else {
				for (var x = 0; x < addnum; x++) {
					nowFileNum++;
					var newobj = '<br><input type="file" name="c_file' + nowFileNum + '" id="c_file' + nowFileNum + '" onchange="chk_file()">';
					$('#file_area').append(newobj);
				}
			}
		}

		jQuery(function($) {
			$('#c_enddate').datepicker({
				dateFormat: 'yy-mm-dd',
				changeYear: true,
				changeMonth: true,
				minDate: '-0'
			});
			$('#c_testdate').datepicker({
				dateFormat: 'yy-mm-dd',
				changeYear: true,
				changeMonth: true,
				minDate: '-0'
			});
			$('#c_pastdate').datepicker({
				dateFormat: 'yy-mm-dd',
				changeYear: true,
				changeMonth: true,
				minDate: '-0'
			});
			$('#c_postdate').datepicker({
				dateFormat: 'yy-mm-dd',
				changeYear: true,
				changeMonth: true,
				maxDate: '-0',
				minDate: '-0'
			});
		});
		var lack_type_num = 1;

		function add_lack_type() {
			//mod by 志豪 20160301 判斷日期決定是否可輸入	
			var datetime_YYYY = new Date().getYear() + 1900;
			var datetime_MM = new Date().getMonth() + 1;
			if (datetime_MM.toString().length == 1) {
				datetime_MM = '0' + datetime_MM;
			}
			var datetime_dd = new Date().getDate();
			if (datetime_dd.toString().length == 1) {
				datetime_dd = '0' + datetime_dd;
			}
			var datetime_now = datetime_YYYY + '-' + datetime_MM + '-' + datetime_dd;
			var enddate = window.document.getElementById('c_enddate').value;
			datetime_now = datetime_now.replace("-", "");
			datetime_now = datetime_now.replace("-", "");
			enddate = enddate.replace("-", "");
			enddate = enddate.replace("-", "");

			//mod by 志豪 20160301 已截止則實際職缺欄位及成功人數將不可修改	
			//if (enddate >= datetime_now)判斷示大於小於方向錯誤
			if (enddate < datetime_now) {
				var html_dsc = '<div id="lack_span_' + lack_type_num + '"><select name="establishment_' + lack_type_num + '" id="establishment_' + lack_type_num + '" ><option value=""></option><option value="1">兼任</option><option value="2">專任</option></select> <select name="lack_type_sw_' + lack_type_num + '" id="lack_type_sw_' + lack_type_num + '" onchange="chk_sw(' + lack_type_num + ')" ><?php for ($x = 0; $x < count($lack_type_array); $x++) {
				echo '<option value="' . $lack_type_array[$x]['num'] . '">' . $lack_type_array[$x]['c_name'] . '</option>';
				} ?><option value="other">其他</option></select> <input type="text" style="width:30px;"  onKeyPress="return(event.keyCode>47 && event.keyCode<58) " id="lack_paople_num_' + lack_type_num + '" name="lack_paople_num_' + lack_type_num + '">名;實際職缺數：<input type="text" title="「實際職缺數」係指貴校實際出缺數。" style="width:30px;background-color:#F5F3F4;" onKeyPress="return(event.keyCode>47 && event.keyCode<58) " ReadOnly="true" id="Real_people_number_' + lack_type_num + '" name="Real_people_number_' + lack_type_num + '" >名;媒合成功人數：<input type="text" title="「媒合成功人數」係指透過本系統媒合成功至貴校任教之人數。" style="width:30px;background-color:#F5F3F4;" ReadOnly="true" id="Complete_number_' + lack_type_num + '" name="Complete_number_' + lack_type_num + '" >名; <span id="other_area_' + lack_type_num + '" style="display:none;">職稱:<input type="text" style="width:100px;" name="other_' + lack_type_num + '" value=""></span><input type="button" value="移除" onclick="del_lack_type(\'lack_span_' + lack_type_num + '\')"></div>';
				$('#lack_area').before(html_dsc);
				for (i = 0; i <= lack_type_num; i = i + 1) {
					try {
						document.getElementById("lack_paople_num_" + i).style.backgroundColor = '#F5F3F4'; // 變更欄位顏色
						document.getElementById("lack_paople_num_" + i).readOnly = true; // 變更欄位為不可輸入
						document.getElementById("Real_people_number_" + i).style.backgroundColor = '#F5F3F4'; // 變更欄位顏色
						document.getElementById("Real_people_number_" + i).readOnly = true; // 變更欄位為不可輸入
						document.getElementById("Complete_number_" + i).style.backgroundColor = '#FFFFFF'; // 變更欄位顏色
						document.getElementById("Complete_number_" + i).readOnly = false; // 變更欄位為不可輸入
					} catch (e) {}
				}
			} else {
				var html_dsc = '<div id="lack_span_' + lack_type_num + '"><select name="establishment_' + lack_type_num + '" id="establishment_' + lack_type_num + '" ><option value=""></option><option value="1">兼任</option><option value="2">專任</option></select> <select name="lack_type_sw_' + lack_type_num + '" id="lack_type_sw_' + lack_type_num + '" onchange="chk_sw(' + lack_type_num + ')" ><?php for ($x = 0; $x < count($lack_type_array); $x++) {
				echo '<option value="' . $lack_type_array[$x]['num'] . '">' . $lack_type_array[$x]['c_name'] . '</option>';} ?>
				<option value="other">其他</option></select> <input type="text" style="width:30px;" onKeyPress="return(event.keyCode>47 && event.keyCode<58) " id="lack_paople_num_' + lack_type_num + '" name="lack_paople_num_' + lack_type_num + '" >名;實際職缺數：<input type="text" title="「實際職缺數」係指貴校實際出缺數。" style="width:30px;background-color:#FFFFFF;" onKeyPress="return(event.keyCode>47 && event.keyCode<58) " id="Real_people_number_' + lack_type_num + '" name="Real_people_number_' + lack_type_num + '" >名;媒合成功人數：<input type="text" title="「媒合成功人數」係指透過本系統媒合成功至貴校任教之人數。" style="width:30px;background-color:#FFFFFF;" onKeyPress="return(event.keyCode>47 && event.keyCode<58) " id="Complete_number_' + lack_type_num + '" name="Complete_number_' + lack_type_num + '" >名; <span id="other_area_' + lack_type_num + '" style="display:none;">職稱:<input type="text" style="width:100px;" name="other_' + lack_type_num + '" value=""></span><input type="button" value="移除" onclick="del_lack_type(\'lack_span_' + lack_type_num + '\')"></div>';
				$('#lack_area').before(html_dsc);
				for (i = 0; i <= lack_type_num; i = i + 1) {
					try {
						document.getElementById("lack_paople_num_" + i).style.backgroundColor = '#FFFFFF'; // 變更欄位顏色
						document.getElementById("lack_paople_num_" + i).readOnly = false; // 變更欄位為不可輸入
						document.getElementById("Real_people_number_" + i).style.backgroundColor = '#FFFFFF'; // 變更欄位顏色
						document.getElementById("Real_people_number_" + i).readOnly = false; // 變更欄位為不可輸入
						document.getElementById("Complete_number_" + i).style.backgroundColor = '#F5F3F4'; // 變更欄位顏色
						document.getElementById("Complete_number_" + i).readOnly = true; // 變更欄位為不可輸入
					} catch (e) {}
				}
			}
			
			lack_type_num++;
			$('#lack_num').val(lack_type_num);
		}

		function del_lack_type(obj_id) {
			$('#' + obj_id).remove();

		}

		function chk_sw(obj_index) {
			if ($('#lack_type_sw_' + obj_index).val() == "other") {
				$('#other_area_' + obj_index).show();

			} else {
				$('#other_area_' + obj_index).hide();
			}
		}
		//mod my peter 20200326
		function chk_file() {
			errorfiletype = "";
			for(var i=1;i<nowFileNum+1;i++){
			    var filepath = document.getElementById("c_file" + i).value.split('\\');
			    //console.log(filepath);
			    var filename = filepath[filepath.length - 1]; // 取得檔名
			    filename = filename.slice(-3)
			    //console.log(filename);
			    //var ext = /\.(odt|ods|odp|odg|odb)$/i; // 設定要篩選出的副檔名
			    //var result = ext.test(filename); // 檔案檢查結果
			    //console.log(result);
			     if ( filename=="odt"||filename =="pdf"||filename=="ODT"||filename=="PDF"||filename =="") {errorfiletype = "";} 
			     else{alert("檔案類型錯誤,請選擇ODT或PDF格式檔案");
				  errorfiletype = "檔案類型錯誤,請選擇ODT或PDF格式檔案";
				  break;}
			    }
			    
		}
		

		var friest_ck = true;

		function sw_data() {
			if (friest_ck) {
				$('#contact_name').val("<?php echo $my_user_data['c_name']; ?>");
				$('#contact_phone').val("<?php echo $my_user_data['c_main_phone']; ?>");
				$('#contact_email').val("<?php echo $my_user_data['c_email']; ?>");
				friest_ck = false;
			} else {
				//$('#contact_name').val("");
				//$('#contact_phone').val("");
				//$('#contact_email').val("");
				friest_ck = true;
			}

		}

		//畫面預覽 mod by 志豪 20160215
		function Preview() {
			var file_P = $('#file').text();
			var cke_c_dsc = $('#cke_c_dsc iframe').contents().find('body p').html();

			var establishment = $('#establishment_0 :selected').text() + $('#lack_type_sw_0 :selected').text() + "x" + window.document.getElementById('lack_paople_num_0').value + "名";
			for (i = 1; i < lack_type_num; i = i + 1) {
				var sestablishment = '#establishment_' + i + ' :selected'
				var slack_type_sw = '#lack_type_sw_' + i + ' :selected'
				establishment += " " + $(sestablishment).text() + $(slack_type_sw).text() + "x" + window.document.getElementById('lack_paople_num_' + i).value + "名";
			}

			var tw_counties = $('#tw_counties_num :selected').text() + window.document.getElementById('location').value
			window.document.getElementById('c_title_P').value = window.document.getElementById('c_title').value;
			window.document.getElementById('up_date_P').value = window.document.getElementById('up_date').value;
			try {
				window.document.getElementById('school_depart_num_P').value = window.document.getElementById('school_depart_num').value;
			} catch (e) {
				window.document.getElementById('school_depart_num_P').value = "";
			}
			window.document.getElementById('c_postdate_P').value = window.document.getElementById('c_postdate').value;
			window.document.getElementById('c_enddate_P').value = window.document.getElementById('c_enddate').value;
			window.document.getElementById('establishment_P').value = establishment;
			window.document.getElementById('tw_counties_num_P').value = tw_counties;
			window.document.getElementById('Academic_expertise_P').value = $('#Academic_expertise :selected').text();

			if (cke_c_dsc == "undefined") {
				window.document.getElementById('c_dsc_P').innerHTML = cke_c_dsc;
			} else {
				window.document.getElementById('c_dsc_P').innerHTML = "";
			}

			window.document.getElementById('contact_name_P').value = window.document.getElementById('contact_name').value;
			window.document.getElementById('contact_phone_P').value = window.document.getElementById('contact_phone').value;
			window.document.getElementById('contact_email_P').value = window.document.getElementById('contact_email').value;
			window.document.getElementById('file_P').value = file_P;

			if (file_P == "") {
				window.document.getElementById('file_P').style.display = 'none';
			}

			window.document.getElementById('Preview_e').style.display = '';
			window.document.getElementById('data_e').style.display = 'none';
			window.document.getElementById('Preview').style.display = 'none';
			window.document.getElementById('Preview_back').style.display = '';
			//window.document.getElementById('ck_value').style.display = 'none';
			//window.document.getElementById('back').style.display = 'none';
		}
		// 畫面預覽 mod by 志豪 20160215
		function Preview_back() {
			window.document.getElementById('Preview_e').style.display = 'none';
			window.document.getElementById('data_e').style.display = '';
			window.document.getElementById('Preview_back').style.display = 'none';
			window.document.getElementById('Preview').style.display = '';
			//window.document.getElementById('ck_value').style.display = '';
			//window.document.getElementById('back').style.display = '';
		}
	</script>
</head>

<body>

	<?php include 'layout/head.php' ?>
	<div id="container">
		<?php
		include('layout/menu_left.php'); //載入左邊選單
		?>
		<div id="content">
			<div class="breadcrumb">
				職缺消息 :: 職缺管理 :: 新增職缺資料
			</div>
			<div class="box">
				<div class="heading">
					<h1><img src="image/category.png" alt="" />職缺消息 :: 職缺管理 :: 新增職缺資料</h1>
					<div class="buttons">
						<a id="Preview" onclick="Preview()" class="button">畫面預覽</a>
						<a style="display:none" id="Preview_back" onclick="Preview_back()" class="button">結束預覽</a>
						<a onclick="ck_value()" class="button">存檔</a>
						<a class="button" onclick="history.back();">取消</a>
					</div>
				</div>
				<div class="content" id="Preview_e" style="display:none">
					<!-- 畫面預覽 mod by 志豪 20160215 -->
					<section>
						<div class="form2" style="width:800px;margin:0px auto">
							<div class="main-title" style="background:#f5f5f5;padding:10px 5px;border-bottom:3px double #e6e6e6;border-top=3px double #e6e6e6">
								公告主旨：<input style="width:500px;border: 0px" background="white" type="text" name="c_title_P" id="c_title_P" value="" readonly>
								<span>刊載日期：<input style="width:100px;border: 0px" background="white" type="text" name="up_date_P" id="up_date_P" value="" readonly></span>
							</div>
							<div style="background:#ffffff;border-bottom:3px double #e6e6e6;border-top=3px double #e6e6e6">
								<table height="10px">
									<tr>
									</tr>
								</table>
							</div>
							<div class="infor">
								<ul>
									<li class="center" style="background:#f5f5f5;border-top=3px double #e6e6e6;height:35px;line-height:35px;font-family=FontAwesome"><i class="fa fa-bookmark"></i> 發佈資訊</li>
									<li style="background:#d4e2eb;height:40px;line-height:40px"><label>公告單位：</label><?php echo $school_data_array[$c_school_num] . $college_depart_array_P[$back_member[$b_member_num]]; ?></li>
									<li style="background:#f5f5f5;height:40px;line-height:40px"><label>用人單位：</label><input style="width:500px;border: 0px" background="white" type="text" name="school_depart_num_P" id="school_depart_num_P" value="" readonly></li>
									<li style="background:#d4e2eb;height:40px;line-height:40px"><label>職缺公告日期：</label><input style="width:500px;border: 0px;background:#d4e2eb" background="white" type="text" name="c_postdate_P" id="c_postdate_P" value="" readonly></li>
									<li style="background:#f5f5f5;height:40px;line-height:40px"><label>報名截止日期：</label><input style="width:500px;border: 0px" background="white" type="text" name="c_enddate_P" id="c_enddate_P" value="" readonly></li>
									<li style="background:#d4e2eb;height:40px;line-height:40px"><label>開缺職缺：</label><input style="width:500px;border: 0px;background:#d4e2eb" background="white" type="text" name="establishment_P" id="establishment_P" value="" readonly></li>
									<li style="background:#f5f5f5;height:40px;line-height:40px"><label>學校地址：</label><?php echo $school_adress_array[$c_school_num]; ?></li>
									<li style="background:#d4e2eb;height:40px;line-height:40px"><label>工作地點：</label><input style="width:500px;border: 0px;background:#d4e2eb" background="white" type="text" name="tw_counties_num_P" id="tw_counties_num_P" value="" readonly></li>
									<li style="background:#f5f5f5;height:40px;line-height:40px"><label>學術專長：</label><input style="width:500px;border: 0px" background="white" type="text" name="Academic_expertise_P" id="Academic_expertise_P" value="" readonly></li>
									<li style="background:#d4e2eb">
										<table>
											<tr>
												<td><label>公告內容：</label></td>
												<td><span style="width:500px;border: 0px;background:#d4e2eb" background="white" type="text" name="c_dsc_P" id="c_dsc_P" value=""><br><br></td>
											</tr>
										</table>
									</li>
									<li style="background:#f5f5f5"><label>相關附件</label>
										<table>
											<tr>
												<td>
													<input style="padding:5px;color:white;padding:2px 10px;display:block;font-size:12px;margin:3px 0;background:#4089c2;background-webkit-border-radius:5px;background-moz-border-radius:5px;border-radius:5px;border-radius-moz-background-clip:padding;border-radius-webkit-background-clip:padding-box;width:750px;border: 0px" background="white" type="text" name="file_P" id="file_P" value="" readonly>
												</td>
											</tr>
											<tr>
												<td></td>
										</table>
									</li>
									<li style="background:#d4e2eb;height:40px;line-height:40px"><label>學校網址：</label><a href="<?php echo $school_url_array[$c_school_num]; ?>" target="_blank"><?php echo $school_url_array[$c_school_num]; ?></a></li>

									<li style="background:#f5f5f5;height:40px;line-height:40px"><label>聯絡人：</label><input style="width:500px;border: 0px" background="white" type="text" name="contact_name_P" id="contact_name_P" value="" readonly></li>
									<li style="background:#d4e2eb;height:40px;line-height:40px"><label>連絡電話：</label><input style="width:500px;border: 0px;background:#d4e2eb" background="white" type="text" name="contact_phone_P" id="contact_phone_P" value="" readonly></li>
									<li style="background:#f5f5f5;height:40px;line-height:40px"><label>E-MAIL：</label><input style="width:500px;border: 0px" background="white" type="text" name="contact_email_P" id="contact_email_P" value="" readonly></li>
									<li style="background:#d4e2eb;height:40px;line-height:40px"><label>相關連結：</label>無</li>
									<li style="background:#f5f5f5;height:40px;line-height:40px"><label>意向書連結：</label>請登入會員後填報意向書</li>
									<li style="background:#d4e2eb;height:40px;line-height:40px"><label>備註：</label>
										<font color="red" size="+1">如對本職缺有投件意願，可參考本意向書</font>
									</li>
								</ul>
							</div><!-- infor end -->
						</div><!-- info end -->
					</section><!-- section end -->

					</section><!-- section end -->
				</div>
				<div class="content" id="data_e">
					<form action="c_post_data_a.php" method="post" enctype="multipart/form-data" id="form">
						<div id="tab-general">
							<table class="form">
								<tr>
									<font color="red" size="+2">*號為必填欄位</font>
									<td>
										<font color="red">*</font>公告主旨
									</td>
									<td><input type="text" name="c_title" id="c_title" value="" background="white"></td>
								</tr>
								<tr>
									<td>
										<font color="red">*</font>職缺公告日期
									</td>
									<td><?php echo  date("Y-m-d", time()); ?><input background="white" type="hidden" name="c_postdate" id="c_postdate" value="<?php echo  date("Y-m-d", time()); ?>"></td>
									<!--<td><input type="text" name="c_postdate" id="c_postdate" value="<?php echo date("Y-m-d", time()); ?>" background="white"></td>-->
								</tr>
								<tr>
									<td>
										<font color="red">*</font>報名截止日期
									</td>
									<td><input type="text" name="c_enddate" id="c_enddate" value="<?php echo date("Y-m-d", time()); ?>" background="white"></td>
								</tr>
								<tr>
									<td>
										<font color="red">*</font>更新日期
									</td>
									<td><?php echo date("Y-m-d", time()); ?><input type="hidden" name="up_date" id="up_date" value="<?php echo date("Y-m-d", time()); ?>" background="white"></td>
								</tr>
								<tr>
									<td>
										<font color="red">*</font>工作地點
									<td>
										<div id="tw_counties_num">
											<select name="tw_counties_num">

												<?php
												for ($x = 0; $x < count($tw_counties_array); $x++) {
													echo '<option value="' . $tw_counties_array[$x]['num'] . '">' . $tw_counties_array[$x]['counties_name'] . '</option>';
												}
												?>
											</select>

											<input type="text" style="width:582px;" id="location" name="location">
										</div>
									</td>
								</tr>
								<tr>
									<td>學術專長</td>
									<td>
										<div id="Academic_expertise">
											<select name="Academic_expertise">
												<?php
												foreach ($Academic_expertise_type_array as $value) {
													if ('0' == $value['SubCode']) {
														echo '<option value="' . $value['SubCode'] . '" selected>' . $value['SubDesc'] . '</option>';
													} else {
														echo '<option value="' . $value['SubCode'] . '" >' . $value['SubDesc'] . '</option>';
													}
												}
												?>
											</select>
											<!--<input type="text" name = "Academic_expertise" id="Academic_expertise">-->
									</td>
								</tr>
								<tr>
									<td>
										<font color="red">*</font>人員類別
									</td>
									<td>
										<div id="member_type">
											<select name="member_type">
												<!-- <option value=""></option> -->
												<option value="1">編內</option>
												<option value="2">編外</option>
												<!-- Mod by 心詠 20190814 客戶0725需求，將此選項改為『其他(如編內或編外、或其他等)』 -->
												<!-- <option value="3">編內或編外</option> -->
												<option value="3">其他(如編內或編外、或其他等)</option>
											</select>
											<span>
												<font color="red">※ 此為新增選項，請務必正確填寫。</font>
											</span>
										</div>

									</td>
								</tr>
								<?php if ($_SESSION['userlogintype'] == '0' || $_SESSION['userlogintype'] == '1' || $_SESSION['userlogintype'] == '2') { ?>
								<tr>
									<td>用人單位</td>
									<td>
										<input type="text" name="school_depart_num" id="school_depart_num">
									</td>
								</tr>
								<?php } ?>
								<tr>
									<td>
										<font color="red">*</font>聯絡人姓名
									</td>
									<td>
										<input type="text" name="contact_name" id="contact_name" background="white">
									</td>
								</tr>
								<tr>
									<td>
										<font color="red">*</font>聯絡人電話
									</td>
									<td>
										<input type="text" name="contact_phone" id="contact_phone" background="white">
									</td>
								</tr>
								<tr>
									<td>
										<font color="red">*</font>聯絡人E-mail
									</td>
									<td>
										<input type="text" name="contact_email" id="contact_email" background="white">
									</td>
								</tr>
								<tr>
									<td>同本人資料</td>
									<td>
										<input type="checkbox" onclick="sw_data()">
										<font color="red" size="+1"> 本案「聯絡人」資料同「刊登本職缺者」</font>
									</td>
								</tr>

								<tr>
									<td>相關連結</td>
									<td>
										<input type="text" name="other_url" id="other_url">
									</td>
								</tr>
								<tr>
									<td>
										<font color="red">*</font>開缺職缺
									</td>
									<td>
										<div id="lack_span_0">
											<select name="establishment_0" id="establishment_0">
												<option value=""></option>
												<option value="1">兼任</option>
												<option value="2">專任</option>
											</select>
											<select name="lack_type_sw_0" id="lack_type_sw_0" onchange="chk_sw(0)">
												<?php
												for ($x = 0; $x < count($lack_type_array); $x++) {
													echo '<option value="' . $lack_type_array[$x]['num'] . '">' . $lack_type_array[$x]['c_name'] . '</option>';
												}
												?>
												<option value="other">其他</option>
											</select>
											<!--20190412 將職稱位置變更至此 mod by 心詠-->
											<span id="other_area_0" style="display:none;">職稱:<input type="text" style="width:100px;" name="other_0" value="">
											</span>
											<input type="text" style="width:30px; background:#FFFFFF"  onKeyPress="return(event.keyCode>47 && event.keyCode<58) " id="lack_paople_num_0" name="lack_paople_num_0">名;實際職缺數：<input type="text" title="「實際職缺數」係指貴校實際出缺數。" style="width:30px; background:#FFFFFF" required="required" ; onKeyPress="return(event.keyCode>47 && event.keyCode<58) " id="Real_people_number_0" name="Real_people_number_0">名;媒合成功人數：<input type="text" title="「媒合成功人數」係指透過本系統媒合成功至貴校任教之人數。" style="width:30px; background:#F5F3F4" readOnly="true" onKeyPress="return(event.keyCode>47 && event.keyCode<58) " id="Complete_number_0" name="Complete_number_0">名;


										</div>
										<p id="lack_area"><input type="button" value="新增缺額" onclick="add_lack_type()"></p>
									</td>
								</tr>

								<tr>
									<td>
										<font color="red">*</font>內文
									</td>
									<td>
										<textarea name="c_dsc" id="c_dsc"></textarea>
									</td>
								</tr>
								<tr>
									<td>附加檔案：<br>請上傳ODT或PDF檔，檔案大小限制2MB</br></td>
									<td>
										<div id="file_area">
											<input type="file" name="c_file1" id="c_file1" onchange="chk_file()">
										</div>
									</td>
								</tr>
							</table>
						</div>
						<input type="hidden" name="send_data" value="HasPostValue">
						<input type="hidden" name="lack_num" id="lack_num" value="1">
					</form>
					<table id="t_c">
						<tr>
							<td>新增附加檔案欄位<input type="text" id="addNum" name="addNum" value="1" size="2" maxlength="2">個<input type="button" value="新增" onclick="addOtherFile()"></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		var editor = CKEDITOR.replace('c_dsc', {});
	</script>
	<script language="javascript">
		<?php
		if ($mg != '') {
			echo 'alert("存檔完畢！！");';
		}
		?>
	</script>

	<?php include("./layout/footer.php"); ?>
</body>

</html>
