<?php
	session_start();
  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件

	// 更新資料
	if($_POST['send_data']=='HasPostValue' ){
		$up_dsc ="update `user` set `u_login_name`='".$_POST['u_login_name']."',`u_password`='".base64_encode($_POST['u_password'])."' where `num`='1'";
		$res=$ODb->query($up_dsc) or die("更新資料出錯，請聯繫管理員。");
		
		$mg="ok";
	}
	
	
	//取出資料
	$sql_dsc = "select * from `user` where `num`='1'";//管理員資料
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res)){
		$user_data['u_login_name'] = $row['u_login_name'];
		$user_data['u_password'] = base64_decode($row['u_password']);
	}
	
	
	//確認是否有資料未填
	if($_SESSION['check']!="1"){
		if($_SESSION['userlogintype']=="0"){
			$sql_dsc = "SELECT  DISTINCT `c_title`  
							FROM  `post_data` 
							INNER JOIN  `lack_data` ON  `post_data`.`num` =  `lack_data`.`post_data_num` 
							LEFT JOIN  `school_data` ON  `post_data`.`c_school_num` =  `school_data`.`num` 
							WHERE `post_data`.b_member_num='".$_SESSION['num']."' AND `c_enddate` < '".date("Y-m-d",time())."' 
							AND (`Real_people_number` is null OR `Complete_number` is null OR `Real_people_number` = '' 
							OR `Complete_number` = '' ) ";	
			// echo "<script>alert('".$sql_dsc."');</script>"; 

			$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
			//$row = mysql_fetch_array($res);
			$first=0;
			$desc='';
			if (!empty($res)){
				while ($row=mysql_fetch_array($res)){
					$c_title = $row['c_title'];
					if($first==0){
						$desc ="『 ".$c_title." 』";
						$first=1;
					}else{
						$desc = $desc.、."『 ".$c_title." 』";
					}					
				}
				echo "<script >alert('您好!\\n為了解大專校院利用本網站公告職缺之媒合成效，請填列貴校".$desc."公告「實際職缺數」及「媒合成功人數」，謝謝!\\n\\n備註：\\n1.「實際職缺數」係指貴校實際出缺數。\\n2.「媒合成功人數」係指透過本系統媒合成功至貴校任教之人數。' );</script>"; 
				$_SESSION['check']="1";	
			}
		}
	}
	$_SESSION['check']="1";
	
	
?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>管理首頁</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
  $('#ulcssmenu ul').hide();
	$('#ulcssmenu li a').click(
		function() {
			var openMe = $(this).next();
			var mySiblings = $(this).parent().siblings().find('ul');
			if (openMe.is(':visible')) {
				openMe.slideUp('normal');  
			} else {
				mySiblings.slideUp('normal');  
				openMe.slideDown('normal');
			}
	  }
	);
	  $('#ulcssmenu li[id="index_data"] ul').slideDown('fast');

});

function ck_value(){
	$('#form').submit();
}
</script>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
<?php if($_SESSION['c_pwd_change'] == "1"){ri_jump("./c_change.php");exit;} ?>
  <div id="content">
  <div class="breadcrumb">
		系統設定 :: <a href="index.php">管理員帳密設定</a>
	</div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" /> 管理員帳密設定</h1>
      <div class="buttons"><a onclick="ck_value()" class="button">存檔</a></div>
    </div>
    <div class="content">
	    <form action="index.php" method="post" enctype="multipart/form-data" id="form">
			<div id="tab-general">
				<table class="form">
				<tr>
					<td>管理員帳號</td>
					<td><input type="text" name="u_login_name" id="u_login_name" value="<?php echo $user_data['u_login_name']; ?>"></td>
				</tr>
				<tr>
					<td>管理員密碼</td>
					<td><input type="password" name="u_password" id="u_password" value="<?php echo $user_data['u_password']; ?>"></td>
				</tr>				
				</table>
			</div>
			<input type="hidden" name="send_data" value="HasPostValue">
		</form>	
    </div>
  </div>
</div>
</div>
<script language="javascript">
<?php
if($mg !=''){
echo 'alert("存檔完畢！！");';
}
?>
</script> 

<?php include("./layout/footer.php");?>
</body></html>