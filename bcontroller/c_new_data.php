<?php
  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	
	if($_GET['pg']=='' || !is_numeric($_GET['pg'])){	
			ri_jump("?pg=1");
	}

	$show_data_num = 50;//一頁顯示多少筆資料
	if($_GET['pg']== '1' ){
		$now_pg = 1;
		$pg_dsc = ' limit '.$show_data_num;
	}else{
		$now_pg = $_GET['pg'];
		$pg_dsc = ' limit '.(($_GET['pg']-1) * $show_data_num) .','.$show_data_num ;
	}
	
	//取出資料	
	$sql_dsc = "SELECT *FROM  `new_data` order by `num` ".$pg_dsc;
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res))
	{
		$sql_array['num']          = $row['num'];
		$sql_array['c_type']       = $row['c_type'];
		$sql_array['c_title']      = $row['c_title'];
		$sql_array['c_dsc']        = $row['c_dsc'];
		$sql_array['c_url']        = $row['c_url'];
		$sql_array['up_date']      = $row['up_date']	;
		
		if($row['c_login_type']==0){
			$sql_array['has_log'] = "是";
		}else{
			$sql_array['has_log'] = "否";
		}
		$data_array[] = $sql_array;
	}
	
	/*
	$sql_qsc = "SELECT *FROM  `new_type` order by `num` ".$pg_dsc;
	$res1=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	*/
	
	//計算總頁數
	$total_pg = 0;
	$sql_dsc = "SELECT count(*) as `get_num` FROM `new_data` ";
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res)){
		if($row['get_num'] > 0 ){
			$num1 = $row['get_num']/$show_data_num;
			$num1_array = explode('.',$num1);
			$total_pg = $num1_array[0];
			if(count($num1_array)>1){
			$total_pg++;
			}
		}
	}
	
	//取出資料	
	$new_type = "SELECT *FROM  `new_type` order by `num` ".$pg_dsc;
	$res=$ODb->query($new_type) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res))
	{
		$new_type_array[$row['num']] = $row['c_name'];
	}
?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>一般會員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
  $('#ulcssmenu ul').hide();
	$('#ulcssmenu li a').click(
		function() {
			var openMe = $(this).next();
			var mySiblings = $(this).parent().siblings().find('ul');
			if (openMe.is(':visible')) {
				openMe.slideUp('normal');  
			} else {
				mySiblings.slideUp('normal');  
				openMe.slideDown('normal');
			}
	  }
	);
	  $('#ulcssmenu li[id="news"] ul').slideDown('fast');

});

function ck_value(){
	$('#form').submit();
}

function del(key_num){
	if(confirm("請確認是否刪除!!\r\n")){
	 $.ajax({
	    url: 'delfunction.php',
		data: {keyNum:key_num,tables:"new_data"},
	    error: function(xhr) {
			alert('Ajax request 發生錯誤');
	    },
	    success: function(response) {
			
			location.reload();
	    }
	  });}}
</script>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
	  <div class="breadcrumb">
			 <a href="c_new_data.php">分類管理</a> 
	  </div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" />最新消息::消息管理</h1>
      <div class="buttons"><a href="c_new_data_a.php" class="button">新增消息</a></div>
    </div>
     <div class="content">
			<div id="tab-general">
				<table class="list">
				<tr>
					
					<td>分類名稱</td>
					<td>文章標題</td>					
					<td>新增時間</td>
					<td align="left" width="150">功能操作</td>
				</tr>
				<?php 
					if(is_array($data_array)){	
					foreach($data_array as $my_data){	
				?>
				<tr>
					
					<td><?php echo $new_type_array[$my_data['c_type']]; ?></td>
					<td><?php echo $my_data['c_title']; ?></td>
					<td><?php echo $my_data['up_date']; ?></td>
					<td align="left">
					<a href="c_new_data_e.php?num=<?php echo $my_data['num'];?>" class="button">編輯</a>
					<a onclick="del(<?php echo $my_data['num'];?>);" class="button">刪除</a>
					</td>
				</tr>
				<?php 
					}
				}else{	?>
				<td colspan="4" align="center">無資料！！</td>
				<?php } ?>
				</table>
			</div>
			<div class="page" align="center">
			 <?php
			  
			 	/**
			 	 * 分頁製作
			 	 */
			 
				if($total_pg >0){
						if(($now_pg-1) > 0){
						echo '
						<a href="c_new_data.php?pg='.($now_pg-1).'" > << </a>&nbsp;&nbsp;
						';
						}else{
						echo '
						<a href="c_new_data.php?pg=1" > << </a>&nbsp;&nbsp;';
						}
					 
					  if(($now_pg-5)>0){
						echo '<a href="c_new_data.php?pg='.($now_pg-5).'">'.($now_pg-5).'</a>';
					  }
					   if(($now_pg-4)>0){
						echo '<a href="c_new_data.php?pg='.($now_pg-4).'">'.($now_pg-4).'</a>';
					  }
					   if(($now_pg-3)>0){
						echo '<a href="c_new_data.php?pg='.($now_pg-3).'">'.($now_pg-3).'</a>';
					  }
					   if(($now_pg-2)>0){
						echo '<a href="c_new_data.php?pg='.($now_pg-2).'">'.($now_pg-2).'</a>';
					  }
					   if(($now_pg-1)>0){
						echo '<a href="c_new_data.php?pg='.($now_pg-1).'">'.($now_pg-1).'</a>';
					  }
					  
						echo $now_pg;//目前頁面
						
					   if(($now_pg+1)<=$total_pg){
						echo '<a href="c_new_data.php?pg='.($now_pg+1).'">'.($now_pg+1).'</a>';
					  }			  
					   if(($now_pg+2)<=$total_pg){
						echo '<a href="c_new_data.php?pg='.($now_pg+2).'">'.($now_pg+2).'</a>';
					  }			  
					   if(($now_pg+3)<=$total_pg){
						echo '<a href="c_new_data.php?pg='.($now_pg+3).'">'.($now_pg+3).'</a>';
					  }			  
					   if(($now_pg+4)<=$total_pg){
						echo '<a href="c_new_data.php?pg='.($now_pg+4).'">'.($now_pg+4).'</a>';
					  }			  
					   if(($now_pg+5)<=$total_pg){
						echo '<a href="c_new_data.php?pg='.($now_pg+5).'">'.($now_pg+5).'</a>';
					  }			  
					  
						if(($total_pg-$now_pg) > 0){
						echo '&nbsp;&nbsp;
						<a href="c_new_data.php?pg='.($now_pg+1).'" > >> </a>';
						}else{
						echo '&nbsp;&nbsp;
						<a href="c_new_data.php?pg='.$now_pg.'" > >> </a>';
						}
				}	  
		  ?>
		</div>		
    </div>
  </div>
</div>
</div>
<script language="javascript">
<?php
if($mg !=''){
echo 'alert("存檔完畢！！");';
}
?>
</script> 

<?php include("./layout/footer.php");?>
</body></html>