<?php
  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	 
    	//解SQL Injection
    if (is_array($_GET)) 
    {
        foreach($_GET as $key => $value)
        {
    		$_GET[$key] = decode_dowith_sql($value);
    	}
        //die($_GET[$key]);
    	//解base64
    	
    	foreach($_GET as $key => $value)
    	{
    		$_GET[$key] = base64_decode($value);
    	}
		
    }
    	
	
	if($_POST['send_data']=='HasPostValue' )
	{
		$nowdate =  date("Y-m-d H:i",time());
		$up_dsc ="update `message_board` set `is_view`    ='1',
											 `view_dsc`   ='".decode_dowith_sql($_POST['view_dsc'])."',
											 `view_date`  ='".$nowdate."' 
										 	  where `num`  ='".$_POST['num']."'";
											  
		$res=$ODb->query($up_dsc) or die("更新資料出錯，請聯繫管理員。");
		ri_jump("c_message_data.php?pg=".base64_encode($_POST['pg'])."&s=".base64_encode($_POST['s']));
		
	}
	else
	{
	    //die("請與管理員聯絡");
    }
	if($_GET['num'] !=''){
		//取出內文資料	
		$sql_dsc = "SELECT * FROM `message_board` WHERE `num`='".$_GET['num']."'";
		$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
		while($row = mysql_fetch_array($res))
		{
			$user_array['num']          = $row['num'];
		    $user_array['member_num']   = $row['member_num'];
		    $user_array['c_title']      = $row['c_title'];
		    $user_array['c_dsc']		= $row['c_dsc'];
		    $user_array['is_view']	    = $row['is_view'];
		    $user_array['view_dsc']     = $row['view_dsc'];
		    $user_array['view_date']	= $row['view_date'];
		    $user_array['up_date']      = $row['up_date'];			
		}
		
		
	}else{
		ri_jump("c_message_data.php");
	}	
	
?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>一般會員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>

<!--月曆-->
<script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/jquery/custom_language_zh.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.16.custom.css" />


<script type="text/javascript">
	

</script>
<script type="text/javascript">
	//-----------------------------------------
	// Confirm Actions (delete, uninstall)
	//-----------------------------------------
	$(document).ready
	(
		function()
		{
			
		  $('#ulcssmenu ul').hide();
		  
		  $('#ulcssmenu li a').click
		  (
				function() 
				{
					var openMe = $(this).next();
					var mySiblings = $(this).parent().siblings().find('ul');
					if (openMe.is(':visible')) 
					{
						openMe.slideUp('normal');  
					} 
					else 
					{
						mySiblings.slideUp('normal');  
						openMe.slideDown('normal');
					}
			    }
			);
			
		  $('#ulcssmenu li[id="que_and_ans"] ul').slideDown('fast');
			
		}
	);
	
	var nowFileNum=2;

function ck_value(){
var isGo = true;
var err_dsc = '';
var ck_array   =  [ "view_dsc"    ];
var err_array  =  [ "請輸入內文！" ];
var type_array =  [ "ckedit" ];

for(var x=0;x< ck_array.length;x++){
	switch(type_array[x]){
		case "text":
				if($('#'+ck_array[x]).val() ==''){
				err_dsc = err_dsc + err_array[x] +'\r\n';
				isGo = false;
				}
		break;
		case "ckedit":
				var dsc =encodeURI(editor.document.getBody().getText());
				if(dsc == '%0A'){//cdedit 無輸入時自動會補入%0A
					err_dsc = err_dsc + err_array[x] +'\r\n';
					isGo = false;
				}
		break;
		case "number":
			if(!$.isNumeric($('#'+ck_array[x]).val()) ){
				err_dsc = err_dsc + err_array[x] +'\r\n';
				isGo = false;				
			}		
		break;
	}
}	
	if(isGo){
		$('#form').submit();
	}
	
	if(err_dsc !=''){
		alert(err_dsc);
	}
}


function addOtherFile(){
var addnum = $('#addNum').val();
if(!$.isNumeric(addnum)) {
	$('#addNum').focus();
    alert('只能輸入數字');
}else{
for(var x=0;x<addnum;x++){
nowFileNum++;
var newobj = '<br><input type="file" name="c_file'+nowFileNum+'" id="c_file'+nowFileNum+'">';
$('#file_area').append(newobj);
}
}
}

jQuery(function($){
  $('#up_date').datepicker({dateFormat: 'yy-mm-dd',changeYear : true,changeMonth : true});
});

function del(num,num2){
var dsc_value = $('#del_dsc').val();
$('#c_del_area_'+num).remove();
	if( dsc_value == ''){
		$('#del_dsc').val(num2);
	}else{
		$('#del_dsc').val(dsc_value+','+num2);
	}
}
</script>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
	  <div class="breadcrumb">
			 <a href="c_message_data.php">填報說明</a> 
	  </div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" />填報說明::填報管理</h1>
     <div class="buttons"><a onclick="ck_value()" class="button">存檔</a><a class="button" onclick="history.back();">取消</a></div>
    </div>
     <div class="content">
	    <form action="" method="post" enctype="multipart/form-data" id="form">
			<div id="tab-general">
				<table class="form">
				<tr>
					<td>會員名稱：</td>
					<td>
						<?php
						    echo $user_array['member_num'];
						?>
					</td>
				</tr>				
				<tr>
					<td>標題</td>
					<td>
						<?php 
						    echo $user_array['c_title']; 
						?>						
					</td>
				</tr>
				<tr>
					<td>內文</td>
					<td>
					    <?php 
					        echo $user_array['c_dsc']; 
					    ?>
					</td>
				</tr>	
				<tr>
					<td>回覆內容</td>
					<td>
						<textarea name="view_dsc" id="view_dsc" ><?php echo $user_array['view_dsc']; ?></textarea>						
					</td>
				</tr>	
				</table>
			</div>
			<input type="hidden" name="send_data" value="HasPostValue">
			<input type="hidden" name="num"  value="<?php echo $user_array['num'];  ?>">
			<input type="hidden" name="pg"  value="<?php echo $_GET['pg'];  ?>">
		</form>	
    </div>
  </div>
</div>
</div>
<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script> 
<script type="text/javascript">
var editor = CKEDITOR.replace('view_dsc', {});
</script> 
<script language="javascript">
<?php
if($mg !=''){
echo 'alert("存檔完畢！！");';
}
?>
</script> 

<?php include("./layout/footer.php");?>
</body></html>