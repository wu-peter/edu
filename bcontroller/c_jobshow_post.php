<?php

  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	
	if($_GET['pg']=='' || !is_numeric($_GET['pg'])){	
			ri_jump("?pg=1");
	}

	$show_data_num = 50;//一頁顯示多少筆資料
	if($_GET['pg']== '1' ){
		$now_pg = 1;
		$pg_dsc = ' limit '.$show_data_num;
	}else{
		$now_pg = $_GET['pg'];
		$pg_dsc = ' limit '.(($_GET['pg']-1) * $show_data_num) .','.$show_data_num ;
	}
	
	$is_go = true;
	//根據登入者的權限限制可觀看的資料
	switch($_SESSION['userlogintype']){
		case "0"://管理員或是教育部
			$where_dsc ="";
		break;
		case "1"://地方教育局
			/**
			 * 想要拿出  地方教育局   底下   學校    系所    資料
			 * 1.用   $_SESSION['cgroupnum'] 對應   c_school_num  判斷登入的群組類別  (資料表：back_group_data)
			 * 2.選擇   資料表：back_group_data 的f_num欄位     找出所屬的   教育局    ID
			 * 3. 之後塞進  post_data 對應  c_school_num欄位  找出對應的   資料欄位
			 */
			$sql_department = "select `c_school_num` from `back_group_data` where `f_num`='".$_SESSION['cgroupnum']."'" ;						
			$res = $ODb->query($sql_department) or die("更新資料出錯，請聯繫管理員。") ;
			while ($row=mysql_fetch_array($res)) 
			{
				$in_dsc .= "'".$row['c_school_num']."',";	
			}
			if($in_dsc !="")
			{
				$where_dsc = " where `c_school_num` in (".substr($in_dsc,0,-1).") ";
			}
			else
			{
				$is_go=false;
			}
		break;
		case "2"://學校
			$where_dsc = " where `c_school_num`='".$_SESSION['cschoolnum']."' ";
		break;
		case "3"://科系
			$where_dsc = " where `c_school_num`='".$_SESSION['cschoolnum']."' and `b_member_group_num`='".$_SESSION['cgroupnum']."' ";
		break;
	}
if($is_go)
{
		//取出資料	
		$sql_dsc = "SELECT *FROM  `post_data` ".$where_dsc." order by `num` ".$pg_dsc;
		//die($sql_dsc);
		$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
		while($row = mysql_fetch_array($res))
		{
			$sql_array['num']          = $row['num'];
			$sql_array['c_title']      = $row['c_title'];
			$sql_array['c_dsc']		   = $row['c_dsc'];
			$sql_array['c_postdate']   = $row['c_postdate'];
			$sql_array['c_enddate']	   = $row['c_enddate'];
			$sql_array['c_testdate']   = $row['c_testdate'];
			$sql_array['c_pastdate']   = $row['c_pastdate'];
			$sql_array['post_type']	   = $row['post_type'];
			$sql_array['up_date']      = $row['up_date']	;
			
			if($row['c_login_type']==0){
				$sql_array['has_log'] = "是";
			}else{
				$sql_array['has_log'] = "否";
			}
			$data_array[] = $sql_array;
		}
		
		//計算總頁數
		$total_pg = 0;
		
		$sql_dsc = "SELECT count(*) as `get_num` FROM `post_data` ";
		$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
		while($row = mysql_fetch_array($res)){
			if($row['get_num'] > 0 ){
				$num1 = $row['get_num']/$show_data_num;
				$num1_array = explode('.',$num1);
				$total_pg = $num1_array[0];
				if(count($num1_array)>1){
				$total_pg++;
				}
			}
		}
}
	
	
	//上傳檔案
?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>一般會員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
  $('#ulcssmenu ul').hide();
	$('#ulcssmenu li a').click(
		function() {
			var openMe = $(this).next();
			var mySiblings = $(this).parent().siblings().find('ul');
			if (openMe.is(':visible')) {
				openMe.slideUp('normal');  
			} else {
				mySiblings.slideUp('normal');  
				openMe.slideDown('normal');
			}
	  }
	);
	  $('#ulcssmenu li[id="job"] ul').slideDown('fast');

});

function ck_value(){
	$('#form').submit();
}

</script>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
	  <div class="breadcrumb">
			 <a href="c_jobshow_post.php">應徵職缺管理</a> 
	  </div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" />應徵職缺::應徵職缺管理</h1>
    </div>
      <div class="content">
			<div id="tab-general">
				<table class="list">
				<tr>
					<!--<td>公告單位</td>-->
					<td>公告主旨</td>					
					<!--<td>教育階類別</td>-->
					<!--<td>工作地點</td>-->
					<td>職缺公告日期</td>
					<td>報名截止日期</td>
					<!--<td>考試開始日期</td>-->
					<!--<td>錄取公告日期</td>-->
					<td align="left" width="150">功能操作</td>
				</tr>
				<?php 
					if(is_array($data_array)){	
					foreach($data_array as $my_data){	
				?>
				<tr>
					<td><?php echo $my_data['c_title']; ?></td>
					<td><?php echo $my_data['c_postdate']; ?></td>
					<td><?php echo $my_data['c_enddate']; ?></td>
					<!--<td><?php echo $my_data['c_testdate']; ?></td>-->
					<!--<td><?php echo $my_data['c_pastdate']; ?></td>-->
					<td align="left">
					<a href="c_jobshow_post_e.php?num=<?php echo $my_data['num'];?>&pg=<?php echo $_GET['pg'];?>" class="button">瀏覽</a>
					</td>
				</tr>
				<?php 
					}
				}else{	?>
				<td colspan="4" align="center">無資料！！</td>
				<?php } ?>
				</table>
			</div>
			<div class="page" align="center">
			 <?php
			  
			 	/**
			 	 * 分頁製作
			 	 */
			 
				if($total_pg >0){
						if(($now_pg-1) > 0){
						echo '
						<a href="c_jobshow_post.php?pg='.($now_pg-1).'" > << </a>&nbsp;&nbsp;
						';
						}else{
						echo '
						<a href="c_jobshow_post.php?pg=1" > << </a>&nbsp;&nbsp;';
						}
					 
					  if(($now_pg-5)>0){
						echo '<a href="c_jobshow_post.php?pg='.($now_pg-5).'">'.($now_pg-5).'</a>';
					  }
					   if(($now_pg-4)>0){
						echo '<a href="c_jobshow_post.php?pg='.($now_pg-4).'">'.($now_pg-4).'</a>';
					  }
					   if(($now_pg-3)>0){
						echo '<a href="c_jobshow_post.php?pg='.($now_pg-3).'">'.($now_pg-3).'</a>';
					  }
					   if(($now_pg-2)>0){
						echo '<a href="c_jobshow_post.php?pg='.($now_pg-2).'">'.($now_pg-2).'</a>';
					  }
					   if(($now_pg-1)>0){
						echo '<a href="c_jobshow_post.php?pg='.($now_pg-1).'">'.($now_pg-1).'</a>';
					  }
					  
						echo $now_pg;//目前頁面
						
					   if(($now_pg+1)<=$total_pg){
						echo '<a href="c_jobshow_post.php?pg='.($now_pg+1).'">'.($now_pg+1).'</a>';
					  }			  
					   if(($now_pg+2)<=$total_pg){
						echo '<a href="c_jobshow_post.php?pg='.($now_pg+2).'">'.($now_pg+2).'</a>';
					  }			  
					   if(($now_pg+3)<=$total_pg){
						echo '<a href="c_jobshow_post.php?pg='.($now_pg+3).'">'.($now_pg+3).'</a>';
					  }			  
					   if(($now_pg+4)<=$total_pg){
						echo '<a href="c_jobshow_post.php?pg='.($now_pg+4).'">'.($now_pg+4).'</a>';
					  }			  
					   if(($now_pg+5)<=$total_pg){
						echo '<a href="c_jobshow_post.php?pg='.($now_pg+5).'">'.($now_pg+5).'</a>';
					  }			  
					  
						if(($total_pg-$now_pg) > 0){
						echo '&nbsp;&nbsp;
						<a href="c_jobshow_post.php?pg='.($now_pg+1).'" > >> </a>';
						}else{
						echo '&nbsp;&nbsp;
						<a href="c_jobshow_post.php?pg='.$now_pg.'" > >> </a>';
						}
				}	  
		  ?>
		</div>						
    </div>
  </div>
</div>
</div>
<script language="javascript">
<?php
if($mg !=''){
echo 'alert("存檔完畢！！");';
}
?>
</script> 

<?php include("./layout/footer.php");?>
</body></html>