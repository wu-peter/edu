<?php

  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	
	if($_GET['pg']=='' || !is_numeric($_GET['pg'])){	
			ri_jump("?pg=1");
	}

	$show_data_num = 50;//一頁顯示多少筆資料
	if($_GET['pg']== '1' ){
		$now_pg = 1;
		$pg_dsc = ' limit '.$show_data_num;
	}else{
		$now_pg = $_GET['pg'];
		$pg_dsc = ' limit '.(($_GET['pg']-1) * $show_data_num) .','.$show_data_num ;
	}
	
	$is_go = true;
	//根據登入者的權限限制可觀看的資料
	switch($_SESSION['userlogintype']){
		case "0"://管理員或是教育部
			$where_dsc ="";
		break;
		case "1"://地方教育局
			/**
			 * 想要拿出  地方教育局   底下   學校    系所    資料
			 * 1.用   $_SESSION['cgroupnum'] 對應   c_school_num  判斷登入的群組類別  (資料表：back_group_data)
			 * 2.選擇   資料表：back_group_data 的f_num欄位     找出所屬的   教育局    ID
			 * 3. 之後塞進  post_data 對應  c_school_num欄位  找出對應的   資料欄位
			 */
			$sql_department = "select `c_school_num` from `back_group_data` where `f_num`='".$_SESSION['cgroupnum']."' or `num`='".$_SESSION['cgroupnum']."'" ;						
			$res = $ODb->query($sql_department) or die("更新資料出錯，請聯繫管理員。") ;
			while ($row=mysql_fetch_array($res)) 
			{
				$in_dsc .= "'".$row['c_school_num']."',";	
			}
			if($in_dsc !="")
			{
				$where_dsc = " where `c_school_num` in (".substr($in_dsc,0,-1).") ";
			}
			else
			{
				$is_go=false;
			}
		break;
		case "2"://學校人室處
			$where_dsc = " where `c_school_num`='".$_SESSION['cschoolnum']."' ";
		break;
		case "3"://科系
			$where_dsc = " where `c_school_num`='".$_SESSION['cschoolnum']."' and `b_member_group_num`='".$_SESSION['cgroupnum']."' ";
		break;
	}
if($is_go)
{
		//取出資料	
		$sql_dsc = "SELECT *FROM  `post_data` ".$where_dsc." order by `num` DESC".$pg_dsc;
		//die($sql_dsc);
		$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
		while($row = mysql_fetch_array($res))
		{
			$sql_array['num']                = $row['num'];
			$sql_array['b_member_num']       = $row['b_member_num'];
			$sql_array['b_member_group_num'] = $row['b_member_group_num'];
			$sql_array['c_title']            = $row['c_title'];
			$sql_array['c_dsc']		         = $row['c_dsc'];
			$sql_array['c_postdate']         = $row['c_postdate'];
			$sql_array['c_enddate']	         = $row['c_enddate'];
			$sql_array['up_date']            = $row['up_date'];
			$sql_array['add_date']            = $row['add_date'];
			$sql_array['tw_counties_num']    = $row['tw_counties_num'];
			$sql_array['location']           = $row['location'];
			
			
			
			if(isset($row['c_login_type']) && $row['c_login_type']==0){
				$sql_array['has_log'] = "是";
			}else{
				$sql_array['has_log'] = "否";
			}
			$data_array[] = $sql_array;
		}
		
		//取出新增處室
		$sql_query = "select * from `back_member` ";
		
		$res = $ODb -> query($sql_query) or die("載入資料出錯，請聯繫管理員。");
		while($row = mysql_fetch_array($res) )
		{
			$back_member[$row['num']] = $row['c_login_name'] ;
		}
		
		//取出縣市
		
		$sql_query = "select * from `tw_counties` ";
		$res = $ODb -> query($sql_query) or die("載入資料出錯，請聯繫管理員。");
		while($row = mysql_fetch_array($res))
		{
			$tw_counties [ $row['num']] = $row['counties_name'];
		}
		
		//計算總頁數
		$total_pg = 0;
		
		$sql_dsc = "SELECT count(*) as `get_num` FROM `post_data` ";
		$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
		while($row = mysql_fetch_array($res)){
			if($row['get_num'] > 0 ){
				$num1 = $row['get_num']/$show_data_num;
				$num1_array = explode('.',$num1);
				$total_pg = $num1_array[0];
				if(count($num1_array)>1){
				$total_pg++;
				}
			}
		}
}
	
	//確認是否有資料未填
	if($_SESSION['check']!="1")
	{
		if($_SESSION['userlogintype']=="3")
		{
			$sql_dsc = "SELECT  DISTINCT `c_title`  
							FROM  `post_data` 
							INNER JOIN  `lack_data` ON  `post_data`.`num` =  `lack_data`.`post_data_num` 
							LEFT JOIN  `school_data` ON  `post_data`.`c_school_num` =  `school_data`.`num`   where `post_data`.b_member_num='".$_SESSION['num']."' and `c_enddate` < '".date("Y-m-d",time())."' and (`Real_people_number` is null or `Complete_number` is null or `Real_people_number` = '' or `Complete_number` = '' ) ";	
			//echo "<script>alert('".$sql_dsc."');</script>"; 
			
			$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
			//$row = mysql_fetch_array($res);
			$first=0;
			$desc='';
			if (!empty($res)){
				while ($row=mysql_fetch_array($res))
		        {
					$c_title = $row['c_title'];
					if($first==0)
					{
						$desc ="『 ".$c_title." 』";
						$first=1;
					}
					else
					{
						$desc = $desc.、."『 ".$c_title." 』";
					}					
		        }
				echo "<script >alert('您好!\\n為了解大專校院利用本網站公告職缺之媒合成效，請填列貴校".$desc."公告「實際職缺數」及「媒合成功人數」，謝謝!\\n\\n備註：\\n1.「實際職缺數」係指貴校實際出缺數。\\n2.「媒合成功人數」係指透過本系統媒合成功至貴校任教之人數。' );</script>"; 
				$_SESSION['check']="1";	
			}
		}
	}
	$_SESSION['check']="1";
	
	//上傳檔案
?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>職缺管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
  $('#ulcssmenu ul').hide();
	$('#ulcssmenu li a').click(
		function() {
			var openMe = $(this).next();
			var mySiblings = $(this).parent().siblings().find('ul');
			if (openMe.is(':visible')) {
				openMe.slideUp('normal');  
			} else {
				mySiblings.slideUp('normal');  
				openMe.slideDown('normal');
			}
	  }
	);
	  $('#ulcssmenu li[id="job"] ul').slideDown('fast');

});

function ck_value(){
	$('#form').submit();
}

function del(key_num,key_dsc){
	if(confirm("請確認是否刪除下列公告主旨的資料!!\r\n"+key_dsc))
	{
	 $.ajax({
	    url: 'delfunction.php',
		data: {keyNum:key_num,tables:"post_data"},
	    error: function(xhr) {
			alert('Ajax request 發生錯誤');
	    },
	    success: function(response) {
			
			location.reload();
	    }
	   });
	}
}
</script>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
	  <div class="breadcrumb">
			職缺消息 :: 職缺管理 
	  </div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" />職缺消息 :: 職缺管理</h1>
	  
      <div class="buttons"><a href="c_post_data_a.php" class="button">新增職缺</a></div>
	  
    </div>
     <div class="content">
			<div id="tab-general">
				<table class="list">
				<tr>
					<td>公告主旨</td>					
					<td>職缺公告日期</td>
					<td>報名截止日期</td>
					<td>職缺新增帳號</td>
					<td>職缺新增日期</td>
					<td>工作地點</td>
					<td align="left" width="150">功能操作</td>
				</tr>
				<?php 
					if(is_array($data_array)){	
					foreach($data_array as $my_data){
				?>
				<tr>
					<td><?php echo $my_data['c_title']; ?></td>
					<td><?php echo $my_data['c_postdate']; ?></td>
					<td><?php echo $my_data['c_enddate']; ?></td>
					<td><?php if(isset($my_data['b_member_num']) && !empty($my_data['b_member_num'])) {echo $back_member[$my_data['b_member_num']];}?></td>
					<td><?php echo $my_data['add_date'];?></td>
					<td><?php echo $tw_counties[$my_data['tw_counties_num']].$my_data['location'];?></td>
					
					<td align="left">
					<a href="c_post_data_e.php?num=<?php echo $my_data['num'];?>&pg=<?php echo $_GET['pg'];?>" class="button">編輯</a>
					<a onclick="del(<?php echo $my_data['num'];?>,'<?php echo $my_data['c_title']; ?>');" class="button">刪除</a>
					</td>
				</tr>
				<?php 
					}
				}else{	?>
				<td colspan="8" align="center">無資料！！</td>
				<?php } ?>
				</table>
			</div>
						<div class="page" align="center">
			 <?php
			  
			 	/**
			 	 * 分頁製作
			 	 */
			 
				if($total_pg >0){
						if(($now_pg-1) > 0){
						echo '
						<a href="c_post_data.php?pg='.($now_pg-1).'" > << </a>&nbsp;&nbsp;
						';
						}else{
						echo '
						<a href="c_post_data.php?pg=1" > << </a>&nbsp;&nbsp;';
						}
					 
					  if(($now_pg-5)>0){
						echo '<a href="c_post_data.php?pg='.($now_pg-5).'">'.($now_pg-5).'</a>';
					  }
					   if(($now_pg-4)>0){
						echo '<a href="c_post_data.php?pg='.($now_pg-4).'">'.($now_pg-4).'</a>';
					  }
					   if(($now_pg-3)>0){
						echo '<a href="c_post_data.php?pg='.($now_pg-3).'">'.($now_pg-3).'</a>';
					  }
					   if(($now_pg-2)>0){
						echo '<a href="c_post_data.php?pg='.($now_pg-2).'">'.($now_pg-2).'</a>';
					  }
					   if(($now_pg-1)>0){
						echo '<a href="c_post_data.php?pg='.($now_pg-1).'">'.($now_pg-1).'</a>';
					  }
					  
						echo $now_pg;//目前頁面
						
					   if(($now_pg+1)<=$total_pg){
						echo '<a href="c_post_data.php?pg='.($now_pg+1).'">'.($now_pg+1).'</a>';
					  }			  
					   if(($now_pg+2)<=$total_pg){
						echo '<a href="c_post_data.php?pg='.($now_pg+2).'">'.($now_pg+2).'</a>';
					  }			  
					   if(($now_pg+3)<=$total_pg){
						echo '<a href="c_post_data.php?pg='.($now_pg+3).'">'.($now_pg+3).'</a>';
					  }			  
					   if(($now_pg+4)<=$total_pg){
						echo '<a href="c_post_data.php?pg='.($now_pg+4).'">'.($now_pg+4).'</a>';
					  }			  
					   if(($now_pg+5)<=$total_pg){
						echo '<a href="c_post_data.php?pg='.($now_pg+5).'">'.($now_pg+5).'</a>';
					  }			  
					  
						if(($total_pg-$now_pg) > 0){
						echo '&nbsp;&nbsp;
						<a href="c_post_data.php?pg='.($now_pg+1).'" > >> </a>';
						}else{
						echo '&nbsp;&nbsp;
						<a href="c_post_data.php?pg='.$now_pg.'" > >> </a>';
						}
				}	  
		  ?>
		</div>				
    </div>
  </div>
</div>
</div>
<script language="javascript">
<?php
if($mg !=''){
echo 'alert("存檔完畢！！");';
}
?>
</script> 

<?php include("./layout/footer.php");?>
</body></html>