<?php
  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	$upFileFload = "./upFile/".date("Ymd",time());
	$upFile = $upFileFload."/";
	
	if($_POST['send_data']=='HasPostValue' )
	{
		if (!is_dir($upFileFload)) 
		{      //檢察upload資料夾是否存在
		  if (!mkdir($upFile))
		  { //不存在的話就創建upload資料夾
			//die ("上傳目錄不存在，並且創建失敗");
			}
		}
		$_POST['c_dsc'] = str_replace("twRRtw",">",$_POST['c_dsc']);
		$_POST['c_dsc'] = str_replace("twLLtw","<",$_POST['c_dsc']);
		//$nowdate =  date("Y-m-d H:i",time());		
		$up_dsc ="insert into `new_data` set `c_title`    ='".decode_dowith_sql($_POST['c_title'])    ."',
											 `c_dsc`      ='".decode_dowith_sql($_POST['c_dsc'])      ."',
											 `c_type`     ='".$_POST['c_type']     ."',
											 `up_date`    ='".decode_dowith_sql($_POST['up_date'])    ."'";
		$res=$ODb->query($up_dsc) or die("更新資料出錯，請聯繫管理員。");
		$getID = mysql_insert_id();
		
		//處理上傳檔案
		foreach($_FILES as $key => $weight_name){
		$file_type = explode('_',$key);
		if($file_type[0]=='e'){
			$sql_dsc = "_en";
		}else{
			$sql_dsc = "";
		}
		$file_type_dsc = explode(".",basename($weight_name['name']));
		$mtime = explode(" ", microtime()); 
		$startTime = $mtime[1].substr($mtime[0],2);			
		$new_name =  $startTime.".".$file_type_dsc[1];
		$uploadfile = $upFile.$new_name;
		
			if (move_uploaded_file($weight_name['tmp_name'], $uploadfile)) {
				$file_size=filesize($uploadfile);
				$sql = "insert into `file_data` set `table_num`='".$getID."',`c_save_dir`='".$upFile."',`table_name`='new_data".$sql_dsc."',`file_name`='".$weight_name['name']."',`save_name`='".$new_name."',`up_date`='".date("Y-m-d H:i",time())."',`file_size`='".$file_size."'";
				$res=$ODb->query($sql) or die("更新資料出錯，請聯繫管理員。");
			}
		}
		ri_jump("c_new_data.php");
	}
	
		
	$up_dsc ="select * from `new_type` order by `num` ";
	$res=$ODb->query($up_dsc) or die("更新資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res))
	{
		$sql_array['num'] = $row['num'];				
		$sql_array['c_name'] = $row['c_name'];
		$type_array[] = $sql_array;
	}
		
?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>一般會員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<!--月曆-->
<script src="js/jquery/jquery-1.10.2.js"></script>
<script src="js/jquery/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/jquery/custom_language_zh.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.16.custom.css" />



<script type="text/javascript">
	//-----------------------------------------
	// Confirm Actions (delete, uninstall)
	//-----------------------------------------
	$(document).ready(function(){
		$('#ulcssmenu ul').hide();
		$('#ulcssmenu li a').click(function(){
			var openMe = $(this).next();
			var mySiblings = $(this).parent().siblings().find('ul');
			if (openMe.is(':visible')){
				openMe.slideUp('normal');  
			} 
			else {
				mySiblings.slideUp('normal');  
				openMe.slideDown('normal');
			}
		});
		$('#ulcssmenu li[id="news"] ul').slideDown('fast');
	});
	
	var nowFileNum=1;
	var errorfiletype = "";

	function ck_value(){
	var isGo = true;
	var err_dsc = '';
	var ck_array   =  ["c_type","c_title"  ,  "c_dsc"     ,  "up_date"     ];
	var err_array  =  ["請輸入分類","請輸入標題!" , "請輸入內文！"   , "請輸入更新日期！！"];
	var type_array =  ["number","text"     , "ckedit"      ,    "text"      ];

	for(var x=0;x< ck_array.length;x++){
	switch(type_array[x]){
		case "number":
			if(!$.isNumeric($('#'+ck_array[x]).val()) ){
				err_dsc = err_dsc + err_array[x] +'\r\n';
				isGo = false;				
			}		
		break;
		case "text":
				if($('#'+ck_array[x]).val() ==''){
				err_dsc = err_dsc + err_array[x] +'\r\n';
				isGo = false;
				}
		break;
		case "ckedit":
				var dsc =encodeURI(editor.document.getBody().getText());
				if(dsc == '%0A'){//cdedit 無輸入時自動會補入%0A
					err_dsc = err_dsc + err_array[x] +'\r\n';
					isGo = false;
				}
		break;
	}
	}	
	if (errorfiletype != '') {
		err_dsc += errorfiletype + '\r\n';
		isGo = false;
	}

	if(isGo){
		$('#form').submit();
	}
	
	if(err_dsc !=''){
		alert(err_dsc);
	}
}


	function addOtherFile(){
	var addnum = $('#addNum').val();
	if(!$.isNumeric(addnum)) {
	$('#addNum').focus();
	alert('只能輸入數字');
	}else{
	for(var x=0;x<addnum;x++){
	nowFileNum++;
	var newobj = '<br><input type="file" name="c_file'+nowFileNum+'" id="c_file'+nowFileNum+ '" onchange="chk_file()">';
	$('#file_area').append(newobj);}}}

	function chk_file() {
		errorfiletype = "";
		for(var i=1;i<nowFileNum+1;i++){
		var filepath = document.getElementById("c_file" + i).value.split('\\');
		var filename = filepath[filepath.length - 1]; // 取得檔名
		filename = filename.slice(-4)
		if ( filename==".doc"||filename =="docx"||filename==".xls"||filename=="xlsx"||filename==".ppt"||filename==".pptx") 
		{alert("檔案類型錯誤,如需上傳文件請使用開放文件格式檔案");
		errorfiletype = "檔案類型錯誤,如需上傳文件請使用開放文件格式檔案";
		break;}}
	}

jQuery(function($){
  $('#up_date').datepicker({dateFormat: 'yy-mm-dd',changeYear : true,changeMonth : true});
});

</script>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
	  <div class="breadcrumb">
			 <a href="c_new_data.php">分類管理</a> 
	  </div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" />最新消息::消息管理</h1>
     <div class="buttons"><a onclick="ck_value()" class="button">存檔</a><a href="c_new_data.php" class="button">取消</a></div>
    </div>
     <div class="content">
	    <form action="c_new_data_a.php" method="post" enctype="multipart/form-data" id="form">
			<div id="tab-general">
				<table class="form">
				<tr>
					<td>分類</td>
					<td>
						<select name="c_type" id="c_type" >
							<option value=""></option>
							<?php foreach($type_array as $value){
								echo '<option value="'.$value['num'].'">'.$value['c_name'].'</option>';
							}?>
						</select>
					</td>
				</tr>				
				<tr>
					<td>標題</td>
					<td>
						<input type="text" name="c_title" id="c_title" size="100">						
					</td>
				</tr>
				<tr>
					<td>更新日期</td>
					<td><?php echo date("Y-m-d", time()); ?><input type="hidden" name="up_date" id="up_date" value="<?php echo date("Y-m-d", time()); ?>" background="white"></td>
				</tr>	
				<tr>
					<td>內文</td>
					<td>
						<textarea name="c_dsc" id="c_dsc"></textarea>						
					</td>
				</tr>
				<tr>
					<td>附加檔案：<br>如需上傳文件請使用PDF檔或開放文件格式<br>(如：ODT.ODP.ODS等)</td>
					<td>
						<div id="file_area">
							<input type="file" name="c_file1" id="c_file1" onchange="chk_file()">
						</div>
					</td>
				</tr>
				</table>
			</div>
			<input type="hidden" name="send_data" value="HasPostValue">
		</form>
		<table id="t_c">
			<tr>
				<td >新增附加檔案欄位<input type="text" id="addNum" name="addNum" value="1" size="2" maxlength="2" >個<input type="button" value="新增" onclick="addOtherFile()"></td>
			</tr>				
		</table>	
    </div>
  </div>
</div>
</div>
<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script> 
<script type="text/javascript">
var editor = CKEDITOR.replace('c_dsc', {});
</script> 
<script language="javascript">
<?php
if($mg !=''){
echo 'alert("存檔完畢！！");';
}
?>
</script> 

<?php include("./layout/footer.php");?>
</body></html>