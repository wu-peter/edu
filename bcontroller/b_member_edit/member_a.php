<?php
	session_start();

  //包含需求檔案 ------------------------------------------------------------------------
	include("../class/common_lite.php");
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("../logout.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件


	if(is_array($_POST)){
		foreach($_POST as $key => $value){
		$_POST[$key] = decode_dowith_sql($value);
		}
	}
	
	//處理成員聲請
	if($_POST['send_data']=='has_post_value' && $_POST['c_group_num']!='' ){
	
	//取出群組資料	
	$sql_dsc = "select `c_type` from `back_group_data` where  `num`='".$_POST['c_group_num']."'";
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res)){
		$c_type = $row['c_type'];
	}	

	
	$nowdate =  date("Y-m-d H:i",time());		
	$up_dsc ="
	insert into `back_member` 
	set 
	`c_login_name`='".$_POST['c_login_name']."',
	`c_pw`='".base64_encode($_POST['c_pw'])."',
	`c_email`='".$_POST['c_email']."',	
	`c_type`='".$c_type."',	
	`c_group_num`='".$_POST['c_group_num']."',
	`c_school_num`='".$_POST['c_school_num']."',	
	`c_login_type`='".$_POST['c_login_type']."',
	`up_date`='".$nowdate."'";
	$res=$ODb->query($up_dsc) or die("更新資料出錯，請聯繫管理員。");
		ri_jump("member.php?cgn=".$_POST['c_group_num']);
	}
	
	if($_GET['cgn']=='' || !is_numeric($_GET['cgn'])){	
		ri_jump("logout.php");
	}
	
	$sql_dsc = "select `c_school_num` from `back_group_data` where  `num`='".$_GET['cgn']."'";
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res)){
		$c_school_num = $row['c_school_num'];
	}	
	
	//層級敘述 例如: xx教育局 :: xx學校
	$group_name_dsc = get_group_dsc($_GET['cgn']);
	function get_group_dsc($get_num){
	//取出群組資料	
		$ODb = new run_db("mysql",3306);      //建立資料庫物件
		$sql_dsc = "select * from `back_group_data` where  `num`='".$get_num."'";
		$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
		while($row = mysql_fetch_array($res)){
			if($row['f_num']!=''){
				$temp_dsc = get_group_dsc($row['f_num'])." :: ";
			}
			$g_dsc = $temp_dsc.$row['c_name'];
		}
		return $g_dsc;
	}
?>
<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>成員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="../js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript">

function ck_value(){
var isGo = true;
var err_dsc = '';
var ck_array =  ["c_login_name","c_pw","c_email","c_login_type"];
var err_array =  ["請輸入登入帳號!","請輸入密碼!","請輸入電子郵件!","請選擇是否能登入!"];
var type_array =  ["text","text","text","text","text"];

for(var x=0;x< ck_array.length;x++){
	switch(type_array[x]){
		case "text":
			if($('#'+ck_array[x]).val() ==''){
				err_dsc = err_dsc + err_array[x] +'\r\n';
				isGo = false;				
			}
		break;
		case "number":
			if(!$.isNumeric($('#'+ck_array[x]).val()) ){
				err_dsc = err_dsc + err_array[x] +'\r\n';
				isGo = false;				
			}		
		break;
	}
}		

	if($('#c_pw').val().length<8){
		err_dsc = err_dsc + "密碼長度必須大於8碼!" +'\r\n';
		isGo = false;
	}
	if(isGo){
		$('#form').submit();
	}
	
	if(err_dsc !=''){
		alert(err_dsc);
	}
}
</script>
</head>
<body>
<style>

</style>

<!--右方登入者身份提示與登出按鈕-->

    <div class="box">
    <div class="heading">
      <h1><img src="images/category.png" alt="" /> <?php echo $group_name_dsc;?> -> 新增成員資料</h1>
      <div class="buttons"><a onclick="ck_value()" class="button">存檔</a><a href="member.php?pg=<?php echo $_GET['pg'];?>&cgn=<?php echo $_GET['cgn'];?>" class="button">取消</a></div>
    </div>
    <div class="content">
	    <form action="member_a.php" method="post" enctype="multipart/form-data" id="form">
			<div id="tab-general">
				<table class="form">
				<tr>
					<td colspan="2">*字號欄位必須填寫</td>
				</tr>
				<tr>
					<td>*帳號</td>
					<td><input type="text" name="c_login_name" id="c_login_name" size="100"></td>
				</tr>
				<tr>
					<td>*密碼</td>
					<td><input type="password" name="c_pw" id="c_pw" size="100"></td>
				</tr>				
				<tr>
					<td>*電子郵件</td>
					<td><input type="text" name="c_email" id="c_email" size="100"></td>
				</tr>
				<tr>
					<td>是否能登入</td>
					<td>
						<label><input type="radio" name="c_login_type"  value="0" id="paper_0" checked />是</label>
						<label><input type="radio" name="c_login_type" value="1" id="paper_1"  />否</label>
					</td>
				</tr>				
				</table>					
			</div>
			<input type="hidden" name="send_data" value="has_post_value">
			<input type="hidden" name="c_group_num" value="<?php echo $_GET['cgn'];?>">	
			<input type="hidden" name="c_school_num" value="<?php echo $c_school_num;?>">				
		</form>	
    </div>
  </div></div>
  </div>

</body>
</html>