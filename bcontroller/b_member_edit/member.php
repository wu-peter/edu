<?php
	session_start();

  //包含需求檔案 ------------------------------------------------------------------------
	include("../class/common_lite.php");
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("../logout.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件

	if($_GET['cgn']=='' || !is_numeric($_GET['cgn'])){	
		ri_jump("logout.php");
	}
	
	if($_GET['pg']=='' || !is_numeric($_GET['pg'])){	
			ri_jump("?pg=1&cgn=".$_GET['cgn']);
	}
	
	$show_data_num = 50;//一頁顯示多少筆資料
	if($_GET['pg']== '1' ){
		$now_pg = 1;
		$pg_dsc = ' limit '.$show_data_num;
	}else{
		$now_pg = $_GET['pg'];
		$pg_dsc = ' limit '.(($_GET['pg']-1) * $show_data_num) .','.$show_data_num ;
	}
	
	//取出資料	
	$sql_dsc = "select * from `back_member` where `c_group_num`='".$_GET['cgn']."' order by `num` ".$pg_dsc;
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res)){
		$sql_array['num'] = $row['num'];
		$sql_array['c_login_name'] = $row['c_login_name'];
		$sql_array['c_email'] = $row['c_email'];
		$sql_array['c_login_type'] = $row['c_login_type'];
		if($row['c_login_type']==0){
			$sql_array['has_log'] = "是";
		}else{
			$sql_array['has_log'] = "否";
		}		
		$data_array[] = $sql_array;
	}
		
	//計算總頁數
	$total_pg = 0;
	$sql_dsc = "SELECT count(*) as `get_num` FROM `back_member`  where `c_group_num`='".$_GET['cgn']."' ";
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res)){
		if($row['get_num'] > 0 ){
			$num1 = $row['get_num']/$show_data_num;
			$num1_array = explode('.',$num1);
			$total_pg = $num1_array[0];
			if(count($num1_array)>1){
			$total_pg++;
			}
		}
	}
	
	//層級敘述 例如: xx教育局 :: xx學校
	$group_name_dsc = get_group_dsc($_GET['cgn']);
	function get_group_dsc($get_num){
	//取出群組資料	
		$ODb = new run_db("mysql",3306);      //建立資料庫物件
		$sql_dsc = "select * from `back_group_data` where  `num`='".$get_num."'";
		$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
		while($row = mysql_fetch_array($res)){
			if($row['f_num']!=''){
				$temp_dsc = get_group_dsc($row['f_num'])." :: ";
			}
			$g_dsc = $temp_dsc.$row['c_name'];
		}
		return $g_dsc;
	}
?>
<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>會員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
</head>
<body>
<style>

</style>

<!--右方登入者身份提示與登出按鈕-->

<div class="box">
    <div class="heading">
      <h1><img src="images/category.png" alt="" /> 成員管理 :: <?php echo $group_name_dsc;?></h1>
      <div class="buttons"><a href="member_a.php?cgn=<?php echo $_GET['cgn'];?>" class="button">新增成員</a></div>
    </div>
    <div class="content">
          <div id="tab-general">
              <table class="list">
              <tr>
                  <td>帳號</td>				
                  <td>電子信箱</td>				
				  <td width="60">是否能登入</td>
                  <td align="left" width="150">功能操作</td>
              </tr>
              <?php 
				if(is_array($data_array)){	
					foreach($data_array as $my_data){	?>
				<tr>
					<td><?php echo $my_data["c_login_name"];?></td>
					<td><?php echo $my_data["c_email"];?></td>
					<td><?php echo $my_data["has_log"];?></td>
					<td align="left">
					<a href="member_e.php?num=<?php echo $my_data['num'];?>&pg=<?php echo $_GET['pg'];?>&cgn=<?php echo $_GET['cgn'];?>" class="button">編輯</a>
					<!--<a onclick="del(<?php echo $my_data['num'];?>);" class="button">刪除</a>-->
					</td>
				</tr>
				<?php 
					}
				}else{	?>
				<td colspan="4" align="center">無資料！！</td>
				<?php } ?>
              </table>
          </div>
		<div class="page" align="center">
			 <?php 
				if($total_pg >0){
						if(($now_pg-1) > 0){
						echo '
						<a href="?pg='.($now_pg-1).'&cgn='.$_GET['cgn'].'" > << </a>&nbsp;&nbsp;
						';
						}else{
						echo '
						<a href="?pg=1&cgn='.$_GET['cgn'].'" > << </a>&nbsp;&nbsp;';
						}
					 
					  if(($now_pg-5)>0){
						echo '<a href="?pg='.($now_pg-5).'&cgn='.$_GET['cgn'].'">'.($now_pg-5).'</a>';
					  }
					   if(($now_pg-4)>0){
						echo '<a href="?pg='.($now_pg-4).'&cgn='.$_GET['cgn'].'">'.($now_pg-4).'</a>';
					  }
					   if(($now_pg-3)>0){
						echo '<a href="?pg='.($now_pg-3).'&cgn='.$_GET['cgn'].'">'.($now_pg-3).'</a>';
					  }
					   if(($now_pg-2)>0){
						echo '<a href="?pg='.($now_pg-2).'&cgn='.$_GET['cgn'].'">'.($now_pg-2).'</a>';
					  }
					   if(($now_pg-1)>0){
						echo '<a href="?pg='.($now_pg-1).'&cgn='.$_GET['cgn'].'">'.($now_pg-1).'</a>';
					  }
					  
						echo $now_pg;//目前頁面
						
					   if(($now_pg+1)<=$total_pg){
						echo '<a href="?pg='.($now_pg+1).'&cgn='.$_GET['cgn'].'">'.($now_pg+1).'</a>';
					  }			  
					   if(($now_pg+2)<=$total_pg){
						echo '<a href="?pg='.($now_pg+2).'&cgn='.$_GET['cgn'].'">'.($now_pg+2).'</a>';
					  }			  
					   if(($now_pg+3)<=$total_pg){
						echo '<a href="?pg='.($now_pg+3).'&cgn='.$_GET['cgn'].'">'.($now_pg+3).'</a>';
					  }			  
					   if(($now_pg+4)<=$total_pg){
						echo '<a href="?pg='.($now_pg+4).'&cgn='.$_GET['cgn'].'">'.($now_pg+4).'</a>';
					  }			  
					   if(($now_pg+5)<=$total_pg){
						echo '<a href="?pg='.($now_pg+5).'&cgn='.$_GET['cgn'].'">'.($now_pg+5).'</a>';
					  }			  
					  
						if(($total_pg-$now_pg) > 0){
						echo '&nbsp;&nbsp;
						<a href="?pg='.($now_pg+1).'&cgn='.$_GET['cgn'].'" > >> </a>';
						}else{
						echo '&nbsp;&nbsp;
						<a href="?pg='.$now_pg.'&cgn='.$_GET['cgn'].'" > >> </a>';
						}
				}	  
		  ?>
		</div>		
		</div>
    </div>
</div>

</body>
</html>