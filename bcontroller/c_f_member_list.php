<?php
  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	
	if($_GET['pg']=='' || !is_numeric($_GET['pg'])){	
			ri_jump("?pg=1"."&s=".$_GET['s']);
	}


	//Url解碼 20190311	
	$_GET['s'] = base64_decode($_GET['s']);

	if($_GET['s']!='') {
		$sql_dsc = "select * from `front_member` where c_name LIKE '%".$_GET['s']."%' OR c_email like '%".$_GET['s']."%'";
		$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	}


	$show_data_num = 50;//一頁顯示多少筆資料
	if($_GET['pg']== '1' ){
		$now_pg = 1;
		$pg_dsc = ' limit '.$show_data_num;
	}else{
		$now_pg = $_GET['pg'];
		$pg_dsc = ' limit '.(($_GET['pg']-1) * $show_data_num) .','.$show_data_num ;
	}
	
	//取出資料	
	$sql_dsc = "select * from `front_member` WHERE c_name LIKE '%".$_GET['s']."%' OR c_email like '%".$_GET['s']."%' order by `num` ".$pg_dsc;

	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res)){
		$sql_array['num'] = $row['num'];
		$sql_array['c_email'] = $row['c_email'];
		$sql_array['c_pw'] = $row['c_pw'];
		$sql_array['c_name'] = $row['c_name'];
		
		if($row['c_login_type']==0){
			$sql_array['has_log'] = "是";
		}else{
			$sql_array['has_log'] = "否";
		}
		$data_array[] = $sql_array;
	}
		
	//計算總頁數
	$total_pg = 0;
	$sql_dsc = "SELECT count(*) as `get_num` FROM `front_member` WHERE c_name LIKE '%".$_GET['s']."%' OR c_email like '%".$_GET['s']."%'";
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res)){
		if($row['get_num'] > 0 ){
			$num1 = $row['get_num']/$show_data_num;
			$num1_array = explode('.',$num1);
			$total_pg = $num1_array[0];
			if(count($num1_array)>1){
			$total_pg++;
			}
		}
	}
	
?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
		<meta charset="UTF-8" />
		<title>一般會員管理</title>
		<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
		<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>
		<script type="text/javascript">
		//-----------------------------------------
		// Confirm Actions (delete, uninstall)
		//-----------------------------------------
		$(document).ready(function(){
			$('#ulcssmenu ul').hide();
			$('#ulcssmenu li a').click(
				function() {
					var openMe = $(this).next();
					var mySiblings = $(this).parent().siblings().find('ul');
					if (openMe.is(':visible')) {
						openMe.slideUp('normal');  
					} else {
						mySiblings.slideUp('normal');  
						openMe.slideDown('normal');
					}
				}
			);
				$('#ulcssmenu li[id="front_member"] ul').slideDown('fast');

		});

		function ck_value(){
			$('#form').submit();
		}

		// -----------
		// Search function
		// -----------
		function search_key_word() {
			if($('#search_key_word').val() !="" ) {
				var key_word ="";
				if($('#search_key_word').val()!='') {
					key_word = $('#search_key_word').val();
				}

				//將搜尋值編碼
				$.ajax({
					url: 'js_function/value_encode.php',
					
					data: {values:key_word},
					
					error: function(xhr)
					{
						alert('Ajax request 發生錯誤');
					},
					
					success: function(response)
					{
						//console.log(response);
						location.replace('?pg=1&s='+response); 
					}
				});


			} else {
				alert("請輸入搜尋值!!");
			}
		}

		</script>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
  <div class="breadcrumb">
		<a href="index.php">一般會員管理</a>
	</div>
    <div class="box">
			<div class="heading">
					<h1><img src="image/category.png" alt="" /> 一般會員管理</h1>
					<div class="buttons"><a href="c_f_member_list_a.php" class="button">新增會員資料</a></div>
			</div>
			<!--20190311 新增搜尋功能 S-->
			<div class="heading">
				搜尋條件 : <input type="text" name="search_key_word" id="search_key_word" class="in_search" style="width:30%;" placeholder="請輸入Email或姓名關鍵字" value="<?php echo $_GET['s']; ?>" />
				<div class="buttons">
					<a href="#" class="button" onclick="search_key_word()">搜尋</a>
					<a href="#" class="button" onclick="location.replace('c_f_member_list.php')">清除</a>
				</div>
			</div>	
			<!--20190311 新增搜尋功能 E-->
    <div class="content">
			<div id="tab-general">
				<table class="list">
				<tr>
					<td >E-mail</td>
					<td width="150">姓名</td>
					<td width="60">是否能登入</td>
					<td align="left" width="150">功能操作</td>
				</tr>
				<?php 
				if(is_array($data_array)){	
					foreach($data_array as $my_data){	?>
				<tr>
					<td><?php echo $my_data["c_email"];?></td>
					<td><?php echo $my_data["c_name"];?></td>
					<td><?php echo $my_data["has_log"];?></td>
					<td align="left">
					<a href="c_f_member_list_e.php?num=<?php echo $my_data['num'];?>&pg=<?php echo $_GET['pg'];?>" class="button">編輯</a>
					<!--<a onclick="del(<?php //echo $my_data['num'];?>);" class="button">刪除</a>-->
					</td>
				</tr>
				<?php 
					}
				}else{	?>
				<td colspan="4" align="center">無資料！！</td>
				<?php } ?>
				</table>
			</div>
		<div class="page" align="center">
			 <?php 
				if($total_pg >0){
						if(($now_pg-1) > 0){
						echo '
						<a href="?pg='.($now_pg-1).'&s='.base64_encode($_GET['s']).'" > << </a>&nbsp;&nbsp;
						';
						}else{
						echo '
						<a href="?pg=1&s='.base64_encode($_GET['s']).'" > << </a>&nbsp;&nbsp;';
						}
					 
					  if(($now_pg-5)>0){
						echo '<a href="?pg='.($now_pg-5).'">'.($now_pg-5).'</a>';
					  }
					   if(($now_pg-4)>0){
						echo '<a href="?pg='.($now_pg-4).'">'.($now_pg-4).'</a>';
					  }
					   if(($now_pg-3)>0){
						echo '<a href="?pg='.($now_pg-3).'">'.($now_pg-3).'</a>';
					  }
					   if(($now_pg-2)>0){
						echo '<a href="?pg='.($now_pg-2).'">'.($now_pg-2).'</a>';
					  }
					   if(($now_pg-1)>0){
						echo '<a href="?pg='.($now_pg-1).'">'.($now_pg-1).'</a>';
					  }
					  
						echo $now_pg;//目前頁面
						
					   if(($now_pg+1)<=$total_pg){
						echo '<a href="?pg='.($now_pg+1).'">'.($now_pg+1).'</a>';
					  }			  
					   if(($now_pg+2)<=$total_pg){
						echo '<a href="?pg='.($now_pg+2).'">'.($now_pg+2).'</a>';
					  }			  
					   if(($now_pg+3)<=$total_pg){
						echo '<a href="?pg='.($now_pg+3).'">'.($now_pg+3).'</a>';
					  }			  
					   if(($now_pg+4)<=$total_pg){
						echo '<a href="?pg='.($now_pg+4).'">'.($now_pg+4).'</a>';
					  }			  
					   if(($now_pg+5)<=$total_pg){
						echo '<a href="?pg='.($now_pg+5).'">'.($now_pg+5).'</a>';
					  }			  
					  
						if(($total_pg-$now_pg) > 0){
						echo '&nbsp;&nbsp;
						<a href="?pg='.($now_pg+1).'" > >> </a>';
						}else{
						echo '&nbsp;&nbsp;
						<a href="?pg='.$now_pg.'" > >> </a>';
						}
				}	  
		  ?>
		</div>				
    </div>
  </div>
</div>
</div>
<script language="javascript">
<?php
if($mg !=''){
echo 'alert("存檔完畢！！");';
}
?>
</script> 

<?php include("./layout/footer.php");?>
</body></html>