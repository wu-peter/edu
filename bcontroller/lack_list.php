<?php
	session_start();

  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN')
	{
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	
	if($_GET['pg']=='' || !is_numeric(base64_decode($_GET['pg'])))
	{	
		ri_jump("?pg=".base64_encode("1")."&s=".$_GET['s']."&s=".$_GET['s2']."&s3=".$_GET['s3']);
	}

	$_GET['pg'] = base64_decode($_GET['pg']);
	
	if(isset($_GET['s']) and $_GET['s'] > ''){
		$_GET['s'] = urldecode($_GET['s']);
	}
	if(isset($_GET['s2']) and $_GET['s2'] > ''){
		$_GET['s2'] = urldecode($_GET['s2']);
	}
	if(isset($_GET['s3']) and $_GET['s3'] > ''){
		$_GET['s3'] = urldecode($_GET['s3']);
	}
	
	$show_data_num = 30;//一頁顯示多少筆資料
	if($_GET['pg']== '1' )
	{
		$now_pg = 1;
		$pg_dsc = ' limit '.$show_data_num;
	}
	else
	{
		$now_pg = $_GET['pg'];
		$pg_dsc = ' limit '.(($_GET['pg']-1) * $show_data_num) .','.$show_data_num ;
	}

	$where_dsc_array = array();//搜尋條件
	$where_dsc = '';//搜尋條件

	//查詢學校
	if(isset($_GET['s2']) and  $_GET['s2'] > '')
	{
		$tempName = $_GET['s2'];
		$tempDsc = '';
		$sql_dsc = "SELECT `num` FROM  `school_data` where `school_name` like '%".$tempName."%' order by `num`";
		$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
		while($row = mysql_fetch_array($res))
		{
			$tempDsc .= "'".$row['num']."',";
		}
		if($tempDsc > ''){
			$where_dsc_array[] =' `c_school_num` IN ('.substr($tempDsc,0,-1).') ';
		}
	}

	//查詢系所
	if(isset($_GET['s3']) and  $_GET['s3'] > '')
	{
		$tempName = $_GET['s3'];
		$tempDsc = '';
		$sql_dsc = "SELECT `num` FROM  `back_group_data` where `c_name` like '%".$tempName."%' order by `num`";
		$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
		while($row = mysql_fetch_array($res))
		{
			$tempDsc .= "'".$row['num']."',";
		}
		if($tempDsc > ''){
			$where_dsc_array[] =' `c_group_num` IN ('.substr($tempDsc,0,-1).') ';
		}
	}

	//查詢帳號
	if($_GET['s'] !='')
	{
		$where_dsc_array[] = " `c_login_name` like '%".$_GET['s']."%' ";
	}
	
	if(count($where_dsc_array) > 0){
		for($x=0;$x<count($where_dsc_array);$x++){
			if($x == 0){
				$where_dsc = " where ".$where_dsc_array[$x]." ";
			}else{
				$where_dsc .= " and ". $where_dsc_array[$x]." ";
			}
		}
	}

	//取出資料	
	$sql_dsc = "SELECT * FROM  `back_member` ".$where_dsc." order by `num`".$pg_dsc;
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res))
	{
		$sql_array['num']          = $row['num'];
		$sql_array['c_login_name'] = $row['c_login_name'];
		$sql_array['c_pw']         = $row['c_pw'];
		$sql_array['c_school_num'] = $row['c_school_num'];
		$sql_array['c_group_num']  = $row['c_group_num'];
		
		$data_array[] = $sql_array;
	}

	//計算筆數並製作分頁
	$total_pg = 0;
	$sql_dsc = "SELECT count(*) as `get_num` FROM `back_member` ".$where_dsc ;
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res))
	{
		if($row['get_num'] > 0 )
		{
			$num1 = $row['get_num']/$show_data_num;
			$num1_array = explode('.',$num1);
			$total_pg = $num1_array[0];
			if(count($num1_array)>1)
			{
				$total_pg++;
			}
		}
	}
	

	$sql_dsc = "select * from `school_data`";
	$res = $ODb -> query($sql_dsc) or die("請與管理員聯絡!!");
	while( $row = mysql_fetch_array($res) )
	{
		$school_data[$row['num']] = $row['school_name'];
	}
	
	$sql_dsc = "select * from `back_group_data`";
	$res = $ODb -> query($sql_dsc) or die("請與管理員聯絡!!");
	while( $row = mysql_fetch_array($res) )
	{
		$back_group_data[$row['num']] = $row['c_name'];
	}
	
?>
<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>一般會員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function()
{
  $('#ulcssmenu ul').hide();
	$('#ulcssmenu li a').click
	(
		function() 
		{
			var openMe = $(this).next();
			var mySiblings = $(this).parent().siblings().find('ul');
			
			if (openMe.is(':visible')) 
			{
				openMe.slideUp('normal');  
			} 
			else 
			{
				mySiblings.slideUp('normal');  
				openMe.slideDown('normal');
			}
	  }
	);
	  $('#ulcssmenu li[id="rule_set"] ul').slideDown('fast');

});

function del(key_num,key_dsc)
{
	if(confirm("請確認是否刪除下列法規項目的資料!!\r\n"+key_dsc))
	{
	 $.ajax({
			    url: 'js_function/delfunction.php',
			    
				data: {keyNum:key_num,tables:"<?php echo base64_encode('school_data');?>"},
				
			    error: function(xhr) 
			    {
					alert('Ajax request 發生錯誤');
			    },
			    
			    success: function(response) 
			    {
					location.reload();
			    }
	       });
	}
}

function search_key_word()
{
	var goSearch = false;
	var tempDsc = '?pg=<?php echo base64_encode("1");?>';
	if($('#search_key_word').val() !="" )
	{	
		var key_word = $('#search_key_word').val();
		tempDsc  = tempDsc + "&s=" + encodeURIComponent(key_word);
		goSearch = true;
	}else{
		tempDsc  = tempDsc + "&s=";
	}
	
	if($('#search_key_word2').val() !="" )
	{	
		var key_word = $('#search_key_word2').val();
		tempDsc  = tempDsc + "&s2=" + encodeURIComponent(key_word);
		goSearch = true;		
	}else{
		tempDsc  = tempDsc + "&s2=";
	}
	
	if($('#search_key_word3').val() !="" )
	{	
		var key_word = $('#search_key_word3').val();
		tempDsc  = tempDsc + "&s3=" + encodeURIComponent(key_word);
		goSearch = true;		
	}else{
		tempDsc  = tempDsc + "&s3=";
	}
	
	if(goSearch){
		location.replace(tempDsc); 
	}else{
		alert("請輸入搜尋值!!");
	}
}
</script>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
	  <div class="breadcrumb">
	  <a href="">學校基本資料設定</a> 
	  </div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" /> 學校基本資料設定::<a href="">資料管理</a></h1>
      <!--<div class="buttons"><a href="school_data_a.php" class="button">新增學校基本資料</a></div>-->
    </div>
     <!-- 搜尋區域-->
	<div class="heading">
		登入帳號 : <input type="text" name="search_key_word" id="search_key_word" class="in_search" style="width:10%;" value="<?php echo $_GET['s']; ?>" /> ||
		學校名稱 : <input type="text" name="search_key_word2" id="search_key_word2" class="in_search" style="width:15%;" value="<?php echo $_GET['s2']; ?>" /> ||
		學校系所 : <input type="text" name="search_key_word3" id="search_key_word3" class="in_search" style="width:15%;" value="<?php echo $_GET['s3']; ?>" />
     <div class="buttons">
		<a href="#" class="button" onclick="search_key_word()">搜尋</a>
		<a href="#" class="button" onclick="location.replace('lack_list.php')">清除</a>
	  </div>
	</div>	
	<!-- 搜尋區域 end -->	
     <div class="content">
			<div id="tab-general">
				<table class="list">
				<tr>
					<td>登入帳號</td>
					<td>登入密碼</td>
					<td>學校名稱</td>
					<td>學校系所</td>
					
					<td align="left" width="150">功能操作</td>
				</tr>
				<?php 
					if(is_array($data_array)){	
					foreach($data_array as $my_data){	
				?>
				<tr>
					<td><?php echo $my_data['c_login_name'];?></td>
					<td><?php echo base64_decode($my_data['c_pw']);?></td>
					<td><?php echo $school_data[$my_data['c_school_num']];?></td>
					<td><?php echo $back_group_data[$my_data['c_group_num']];?></td>
					
					<td align="left">
					
					</td>
				</tr>
				<?php 
					}
				}else{	?>
				<td colspan="4" align="center">無資料！！</td>
				<?php } ?>
				</table>
			</div>
			<div class="page" align="center">
			 <?php 
			 
			 /**
			  * 製作經base64編碼後的分頁
			  */
			 $tempDSC = '&s='.urlencode($_GET['s']).'&s2='.urlencode($_GET['s2']).'&s3='.urlencode($_GET['s3']);
				if($total_pg >0){
						if(($now_pg-1) > 0){
						echo '
						<a href="?pg='.base64_encode($now_pg-1).$tempDSC.'" > << </a>&nbsp;&nbsp;
						';
						}else{
						echo '
						<a href="?pg='.base64_encode("1").$tempDSC.'" > << </a>&nbsp;&nbsp;';
						}
					 
					  if(($now_pg-5)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-5).$tempDSC.'">'.($now_pg-5).'</a></u>&nbsp;&nbsp;';
					  }
					   if(($now_pg-4)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-4).$tempDSC.'">'.($now_pg-4).'</a></u>&nbsp;&nbsp;';
					  }
					   if(($now_pg-3)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-3).$tempDSC.'">'.($now_pg-3).'</a></u>&nbsp;&nbsp;';
					  }
					   if(($now_pg-2)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-2).$tempDSC.'">'.($now_pg-2).'</a></u>&nbsp;&nbsp;';
					  }
					   if(($now_pg-1)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-1).$tempDSC.'">'.($now_pg-1).'</a></u>&nbsp;&nbsp;';
					  }
					  
						echo $now_pg."&nbsp;&nbsp;";//目前頁面
						
					   if(($now_pg+1)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+1).$tempDSC.'">'.($now_pg+1).'</a></u>&nbsp;&nbsp;';
					  }			  
					   if(($now_pg+2)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+2).$tempDSC.'">'.($now_pg+2).'</a></u>&nbsp;&nbsp;';
					  }			  
					   if(($now_pg+3)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+3).$tempDSC.'">'.($now_pg+3).'</a></u>&nbsp;&nbsp;';
					  }			  
					   if(($now_pg+4)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+4).$tempDSC.'">'.($now_pg+4).'</a></u>&nbsp;&nbsp;';
					  }			  
					   if(($now_pg+5)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+5).$tempDSC.'">'.($now_pg+5).'</a></u>&nbsp;&nbsp;';
					  }			  
					  
						if(($total_pg-$now_pg) > 0){
						echo '&nbsp;&nbsp;
						<a href="?pg='.base64_encode($now_pg+1).$tempDSC.'" > >> </a>';
						}else{
						echo '&nbsp;&nbsp;
						<a href="?pg='.base64_encode($now_pg).$tempDSC.'" > >> </a>';
						}
				}	  
		  ?>
		</div>		
    </div>
  </div>
</div>
</div>
<script language="javascript">
<?php
if($mg !=''){
echo 'alert("存檔完畢！！");';
}
?>
</script> 

<?php include("./layout/footer.php");?>
</body></html>