<?php
  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN')
	{
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	$upFileFload = "./upFile/".date("Ymd",time());
	$upFile = $upFileFload."/";
	$upload_img_name_array = array('c_file_name');
	
	if($_POST['send_data']=='HasPostValue' )
	{
		$up_dsc ="insert into `school_data` set `school_type`     ='".$_POST['school_type']."',
												`school_id`       ='".$_POST['school_id']."',
											  	`school_name`     ='".$_POST['school_name']."',
											  	`tw_counties_num` ='".$_POST['tw_counties_num']."',
											  	`school_adress`   ='".$_POST['school_adress']."',
											  	`school_url`      ='".$_POST['school_url']."'";
		$res=$ODb->query($up_dsc) or die("更新資料出錯，請聯繫管理員。");
		ri_jump("school_data.php");
	}
	
	
	//查台灣縣市名稱
	$up_dsc ="select * from `tw_counties` order by `num` ";
	$result=$ODb->query($up_dsc) or die("更新資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($result))
	{
		$sql_array['num'] = $row['num'];				
		$sql_array['counties_name'] = $row['counties_name'];
		$type_array[] = $sql_array;
	}
?>
<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>一般會員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
  $('#ulcssmenu ul').hide();
	$('#ulcssmenu li a').click(
		function() {
			var openMe = $(this).next();
			var mySiblings = $(this).parent().siblings().find('ul');
			if (openMe.is(':visible')) {
				openMe.slideUp('normal');  
			} else {
				mySiblings.slideUp('normal');  
				openMe.slideDown('normal');
			}
	  }
	);
	  $('#ulcssmenu li[id="front_member"] ul').slideDown('fast');

});

function ck_value()
{
	var isGo = true;
	var err_dsc = '';
	var ck_array =  [ "school_type" ,"school_id"  ,"school_name"  ,"tw_counties_num"  ,"school_adress"  ,"school_url"];
	var err_array =  [ "請選擇學校類別!!"  ,"請輸入學校代碼!!"  ,"請輸入學校名稱!!"   ,"請選擇學校地點!!"        ,"請輸入學校地址!!"      ,"請輸入學校網址!!"];
	var type_array =  ["text"       ,"text"       ,"text"         ,"text"             ,"text"            ,"text"     ];
	
	for(var x=0;x< ck_array.length;x++)
	{
		switch(type_array[x])
		{
			case "text":
						if($('#'+ck_array[x]).val() =='')
						{
							err_dsc = err_dsc + err_array[x] +'\r\n';
							isGo = false;
						}
			break;
			
		}
	}	
	if(isGo)
	{
		$('#form').submit();
	}
	
	if(err_dsc !='')
	{
		alert(err_dsc);
	}
}
</script>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
	  <div class="breadcrumb">
			 <a href="">相關法令管理</a> 
	  </div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" /> 學校基本資料設定 :: 學校基本設定管理 :: 新增學校資料 </h1>
      <div class="buttons"><a onclick="ck_value()" class="button">存檔</a><a  class="button" onclick="history.back();">取消</a></div>
    </div>
     <div class="content">
			<form action="school_data_a.php" method="post" enctype="multipart/form-data" id="form">
			<div id="tab-general">
				<table class="form">
				<tr>
					<td>學校代碼</td>
					<td><input type="text" name="school_id" id="school_id" value="" ></td>
				</tr>
				<tr>
					<td>學校類別</td>
					<td>
					<input type="radio" name="school_type" id="school_type_0" value="0" checked ><label for="school_type_0">公立</label>
						<input type="radio" name="school_type" id="school_type_1" value="1" ><label for="school_type_1">私立</label>
						<input type="radio" name="school_type" id="school_type_2" value="2" ><label for="school_type_2">空中大學</label></td>
				</tr>				
				<tr>
					<td>學校名稱</td>
					<td><input type="text" name="school_name" id="school_name" ></td>
				</tr>
				<tr>
					<td>學校地點</td>
					<td>
						<select name="tw_counties_num" id="tw_counties_num" >
							<option value=""></option>
							<?php foreach($type_array as $value)
								{
									echo '<option value="'.$value['num'].'">'.$value['counties_name'].'</option>';
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td>學校地址</td>
					<td><input type="text" name="school_adress" id="school_adress" value="" ></td>
				</tr>
				<tr>
					<td>學校網址</td>
					<td><input type="text" name="school_url" id="school_url" value="" ></td>
				</tr>				
				</table>
			</div>
			<input type="hidden" name="send_data" value="HasPostValue">
		</form>	
    </div>
  </div>
</div>
</div>
<script language="javascript">
<?php
if($mg !=''){
echo 'alert("存檔完畢！！");';
}
?>
</script> 

<?php include("./layout/footer.php");?>
</body></html>