<?php
  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	$upFileFload = "./upFile/".date("Ymd",time());
	$upFile = $upFileFload."/";
	$upload_img_name_array = array('c_file_name');
	
	//解SQL Injection
	foreach($_GET as $key => $value){
		$_GET[$key] = decode_dowith_sql($value);
	}

	//解base64
	foreach($_GET as $key => $value)
	{
		$_GET[$key] = base64_decode($value);
	}
	
	if($_POST['send_data']=='HasPostValue' ){
		if (!is_dir($upFileFload)) {      //檢察upload資料夾是否存在
		  if (!mkdir($upFile)){ //不存在的話就創建upload資料夾
			//die ("上傳目錄不存在，並且創建失敗");
			}
		}
		$img_sql='';
		//處理上傳檔案
		foreach($upload_img_name_array as $weight_name){
		$file_type_dsc = explode(".",basename($_FILES[$weight_name]['name']));
		$mtime = explode(" ", microtime()); 
		$startTime = $mtime[1].substr($mtime[0],2);			
		$new_name =  $startTime.".".$file_type_dsc[1];
		$uploadfile = $upFile.$new_name;
			if (move_uploaded_file($_FILES[$weight_name]['tmp_name'], $uploadfile)) {
				$sql = "select `c_save_name`,`c_save_dir` FROM `pic_control_data` where `num`='".$_POST['num']."'";
				$res=$ODb->query($sql) or die("更新資料出錯，請聯繫管理員。");
				while($row = mysql_fetch_array($res)){
					unlink($row['c_save_dir'].$row['c_save_name']);
				}
				$img_sql .=",`c_save_dir`='".$upFile."',`c_save_name`='".$new_name."' ";
			}
		}
		
		$nowdate =  date("Y-m-d H:i",time());		
		$up_dsc ="update `pic_control_data` set `c_title`='".decode_dowith_sql($_POST['c_title'])."',`c_url`='".decode_dowith_sql($_POST['c_url'])."',`up_date`='".decode_dowith_sql($nowdate)."'".$img_sql." where `num`='".$_POST['num']."'";
		$res=$ODb->query($up_dsc) or die("更新資料出錯，請聯繫管理員。");
		ri_jump("c_pic_control_set.php?pg=".base64_encode($_POST['pg'])."&s=".base64_encode($_POST['s']));
	}
	
	//類別名稱
	$up_dsc ="select `num` from `pic_control_data` order by `num` ";
	$res=$ODb->query($up_dsc) or die("更新資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res)){
		$sql_array['num'] = $row['num'];				
		$sql_array['c_name'] = $row['c_name'];				
		$type_array[] = $sql_array;
	}
	
	if($_GET['num'] !=''){
		//取出相簿資料	
		$sql_dsc = "SELECT * FROM `pic_control_data` WHERE `num`='".$_GET['num']."'";
		$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
		while($row = mysql_fetch_array($res)){
			$user_array['num'] = $row['num'];			
			$user_array['c_title'] = $row['c_title'];
			$user_array['c_save_dir'] = $row['c_save_dir'];
			$user_array['c_save_name'] = $row['c_save_name'];
			$user_array['c_url'] = $row['c_url'];
			$user_array['up_date'] = $row['up_date'];
		}
	}else{
		ri_jump("c_pic_control_set.php");
	}
	
?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>一般會員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
  $('#ulcssmenu ul').hide();
	$('#ulcssmenu li a').click(
		function() {
			var openMe = $(this).next();
			var mySiblings = $(this).parent().siblings().find('ul');
			if (openMe.is(':visible')) {
				openMe.slideUp('normal');  
			} else {
				mySiblings.slideUp('normal');  
				openMe.slideDown('normal');
			}
	  }
	);
		  $('#ulcssmenu li[id="pic_control"] ul').slideDown('fast');

});

var has_del_oldFile = false;

function ck_value()
{
	var isGo = true;
	var err_dsc = '';
	var ck_array =  [ "c_title", "c_file_name","c_url"];
	var err_array =  [ "請輸入標題!", "請選擇檔案!","請輸入連結!"];
	var type_array =  ["text", "file","text"];
	
	for(var x=0;x< ck_array.length;x++)
		{
			switch(type_array[x])
			{
				case "text":
				case "file":
					if(ck_array[x] == 'c_file_name')
					{//針對c_file_name這個欄位做特別處理
							if( has_del_oldFile )
							{
								if($('#'+ck_array[x]).val() =='')
									{
										err_dsc = err_dsc + err_array[x] +'\r\n';
										isGo = false;
								    }
							}				
					}
					else
					{
						if($('#'+ck_array[x]).val() =='')
						{
							err_dsc = err_dsc + err_array[x] +'\r\n';
							isGo = false;
						}
					}
				break;
		   }
	}	
		if(isGo)
		{
			$('#form').submit();
		}
		
		if(err_dsc !='')
		{
			alert(err_dsc);
		}
}

function c_File()
{
	$('#oldfile').remove();
	$('#newfile').show();
	has_del_oldFile = true;
}
</script>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
	  <div class="breadcrumb">
			 <a href="">相關連結</a> 
	  </div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" /> 相關連結::<a href="c_pic_control_set.php">連結管理</a>:: <a href="c_pic_control_set_e.php?num=<?php echo $user_array['num'];?>">編輯連結</a></h1>
      <div class="buttons"><a onclick="ck_value()" class="button">存檔</a><a class="button" onclick="history.back();">取消</a></div>
    </div>
      <div class="content">
	    <form action="c_pic_control_set_e.php" method="post" enctype="multipart/form-data" id="form">
			<div id="tab-general">
				<table class="form">
				<tr>
					<td>標題</td>
					<td><input type="text" name="c_title" id="c_title" value="<?php echo  $user_array['c_title']; ?>" ></td>
				</tr>				
				<tr>
					<td>上傳圖片<br>180x136 px</td> 
					<td>
					<div id="oldfile">
					<a  href="<?php echo $user_array['c_save_dir'].$user_array['c_save_name'];?>" target="_blank"><img src="<?php echo $user_array['c_save_dir'].$user_array['c_save_name'];?>" width="50" width="50"></a><input type="button" onclick="c_File()" value="刪除">
					</div>
					<div id="newfile"  style="display:none" >
					<input type="file" name="c_file_name" id="c_file_name" accept="image/jpg,image/gif,image/jpeg,image/png" ><br><input type="button" value="重選" onclick="$('#c_file_name').val('')">
					</div>
					</td>
				</tr>
				<tr>
					<td>連結</td>
					<td><input type="text" name="c_url" id="c_url" value="<?php echo $user_array['c_url'];?>"></td>
				</tr>
				</table>
			</div>
			<input type="hidden" name="send_data" value="HasPostValue">
			<input type="hidden" name="num" value="<?php echo $user_array['num']; ?>">
		</form>	
    </div>
  </div>
</div>
</div>
<script language="javascript">
<?php
if($mg !=''){
echo 'alert("存檔完畢！！");';
}
?>
</script> 

<?php include("./layout/footer.php");?>
</body></html>