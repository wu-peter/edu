<?php
  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN')
	{
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	
	foreach($_GET as $key => $value){
		$_GET[$key] = decode_dowith_sql($value);
	}
	
	
	if($_GET['pg']=='' || !is_numeric(base64_decode($_GET['pg'])))
	{	
			ri_jump("?pg=".base64_encode("1")."&s=".$_GET['s']);
	}

	//Url解碼
	foreach($_GET as $key => $value)
	{
		$_GET[$key] = base64_decode($value);
	}
	
	
	
	$sql_dsc = "select * from `back_member` where `c_login_type` = '1'";
	$res = $ODb -> query($sql_dsc) or die("請與管理員聯絡!!");
	while( $row = mysql_fetch_array($res) )
	{
		$sql['num']          = $row['num'];
		$sql['c_login_name'] = $row['c_login_name'];
		$sql['c_pw']         = $row['c_pw'];
		$sql['c_group_num']  = $row['c_group_num'];
		$sql['c_school_num'] = $row['c_school_num'];
		$data_array[] = $sql;
	}
	
	$sql_dsc = "select * from `school_data`";
	$res = $ODb -> query($sql_dsc) or die("請與管理員聯絡!!");
	while( $row = mysql_fetch_array($res) )
	{
		$school_data[$row['num']] = $row['school_name'];
	}
	
	$sql_dsc = "select * from `back_group_data`";
	$res = $ODb -> query($sql_dsc) or die("請與管理員聯絡!!");
	while( $row = mysql_fetch_array($res) )
	{
		$back_group_data[$row['num']] = $row['c_name'];
	}
	//die(print_r($data_array));
	
	$show_data_num = 50;//一頁顯示多少筆資料
	if($_GET['pg']== '1' )
	{
		$now_pg = 1;
		$pg_dsc = ' limit '.$show_data_num;
	}
	else
	{
		$now_pg = $_GET['pg'];
		$pg_dsc = ' limit '.(($_GET['pg']-1) * $show_data_num) .','.$show_data_num ;
	}
	
	//計算筆數並製作分頁
	$total_pg = 0;
	$sql_dsc = "SELECT count(*) as `get_num` FROM `back_member` where `c_login_type` = '1'";
	$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
	while($row = mysql_fetch_array($res))
	{
		if($row['get_num'] > 0 )
		{
			$num1 = $row['get_num']/$show_data_num;
			$num1_array = explode('.',$num1);
			$total_pg = $num1_array[0];
			if(count($num1_array)>1)
			{
				$total_pg++;
			}
		}
	}
	
?>
<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>一般會員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function()
{
  $('#ulcssmenu ul').hide();
	$('#ulcssmenu li a').click
	(
		function() 
		{
			var openMe = $(this).next();
			var mySiblings = $(this).parent().siblings().find('ul');
			
			if (openMe.is(':visible')) 
			{
				openMe.slideUp('normal');  
			} 
			else 
			{
				mySiblings.slideUp('normal');  
				openMe.slideDown('normal');
			}
	  }
	);
	  $('#ulcssmenu li[id="rule_set"] ul').slideDown('fast');

});

function del(key_num,key_dsc)
{
	if(confirm("請確認是否刪除下列法規項目的資料!!\r\n"+key_dsc))
	{
	 $.ajax({
			    url: 'js_function/delfunction.php',
			    
				data: {keyNum:key_num,tables:""},
				
			    error: function(xhr) 
			    {
					alert('Ajax request 發生錯誤');
			    },
			    
			    success: function(response) 
			    {
					location.reload();
			    }
	       });
	}
}

function search_key_word()
{
	if($('#search_key_word').val() !="" )
	{	
		var key_word ="";
		if($('#search_key_word').val()!='')
		{
			key_word = $('#search_key_word').val();
		}	
		//將搜尋值編碼
		$.ajax({
					url: 'js_function/value_encode.php',
					
					data: {values:key_word},
					
					error: function(xhr)
					{
						alert('Ajax request 發生錯誤');
					},
					
					success: function(response)
					{
						//console.log(response);
						location.replace('?pg=<?php echo base64_encode("1");?>&s='+response); 
					}
		      });
	}
	else
	{
		alert("請輸入搜尋值!!");
	}
}
</script>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
	  <div class="breadcrumb">
	  <a href="">帳號鎖定清單</a> 
	  </div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" /> 帳號鎖定清單::<a href="">資料管理</a></h1>
    </div>
   
	<!-- 搜尋區域 end -->	
     <div class="content">
			<div id="tab-general">
				<table class="list">
				<tr>
					<td>登入帳號</td>
					<td>登入密碼</td>
					<td>學校名稱</td>
					<td>學校系所</td>
					<td align="left" width="150">功能操作</td>
				</tr>
				<?php 
					if(is_array($data_array)){	
					foreach($data_array as $my_data){	
				?>
				<tr>
					<td><?php echo $my_data['c_login_name'];?></td>
					<td><?php echo base64_decode($my_data['c_pw']);?></td>
					<td><?php echo $school_data[$my_data['c_school_num']];?></td>
					<td><?php echo $back_group_data[$my_data['c_group_num']];?></td>
					<td align="left">
					<a href="lack_account_e.php?num=<?php echo base64_encode($my_data['num']);?>&pg=<?php echo base64_encode($_GET['pg']);?>&s=<?php echo base64_encode($_GET['s']);?>" class="button">編輯</a>
					</td>
				</tr>
				<?php 
					}
				}else{	?>
				<td colspan="4" align="center">無資料！！</td>
				<?php } ?>
				</table>
			</div>
			<div class="page" align="center">
			 <?php 
			 
			 /**
			  * 製作經base64編碼後的分頁
			  */
			 
				if($total_pg >0){
						if(($now_pg-1) > 0){
						echo '
						<a href="?pg='.base64_encode($now_pg-1).'&s='.base64_encode($_GET['s']).'" > << </a>&nbsp;&nbsp;
						';
						}else{
						echo '
						<a href="?pg='.base64_encode("1").'&s='.base64_encode($_GET['s']).'" > << </a>&nbsp;&nbsp;';
						}
					 
					  if(($now_pg-5)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-5).'&s='.base64_encode($_GET['s']).'">'.($now_pg-5).'</a></u>&nbsp;&nbsp;';
					  }
					   if(($now_pg-4)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-4).'&s='.base64_encode($_GET['s']).'">'.($now_pg-4).'</a></u>&nbsp;&nbsp;';
					  }
					   if(($now_pg-3)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-3).'&s='.base64_encode($_GET['s']).'">'.($now_pg-3).'</a></u>&nbsp;&nbsp;';
					  }
					   if(($now_pg-2)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-2).'&s='.base64_encode($_GET['s']).'">'.($now_pg-2).'</a></u>&nbsp;&nbsp;';
					  }
					   if(($now_pg-1)>0){
						echo '<u><a href="?pg='.base64_encode($now_pg-1).'&s='.base64_encode($_GET['s']).'">'.($now_pg-1).'</a></u>&nbsp;&nbsp;';
					  }
					  
						echo $now_pg."&nbsp;&nbsp;";//目前頁面
						
					   if(($now_pg+1)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+1).'&s='.base64_encode($_GET['s']).'">'.($now_pg+1).'</a></u>&nbsp;&nbsp;';
					  }			  
					   if(($now_pg+2)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+2).'&s='.base64_encode($_GET['s']).'">'.($now_pg+2).'</a></u>&nbsp;&nbsp;';
					  }			  
					   if(($now_pg+3)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+3).'&s='.base64_encode($_GET['s']).'">'.($now_pg+3).'</a></u>&nbsp;&nbsp;';
					  }			  
					   if(($now_pg+4)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+4).'&s='.base64_encode($_GET['s']).'">'.($now_pg+4).'</a></u>&nbsp;&nbsp;';
					  }			  
					   if(($now_pg+5)<=$total_pg){
						echo '<u><a href="?pg='.base64_encode($now_pg+5).'&s='.base64_encode($_GET['s']).'">'.($now_pg+5).'</a></u>&nbsp;&nbsp;';
					  }			  
					  
						if(($total_pg-$now_pg) > 0){
						echo '&nbsp;&nbsp;
						<a href="?pg='.base64_encode($now_pg+1).'&s='.base64_encode($_GET['s']).'" > >> </a>';
						}else{
						echo '&nbsp;&nbsp;
						<a href="?pg='.base64_encode($now_pg).'&s='.base64_encode($_GET['s']).'" > >> </a>';
						}
				}	  
		  ?>
		</div>	
    </div>
  </div>
</div>
</div>
<script language="javascript">
<?php
if($mg !=''){
echo 'alert("存檔完畢！！");';
}
?>
</script> 

<?php include("./layout/footer.php");?>
</body></html>