<?php
  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件

?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>統計資料</title>

<!--月曆-->

<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script src="js/jquery/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/jquery/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery/custom_language_zh.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.16.custom.css" />

<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
	$(document).ready
	(
		function()
		{
			
		  $('#ulcssmenu ul').hide();
		  
		  $('#ulcssmenu li a').click
		  (
				function() 
				{
					var openMe = $(this).next();
					var mySiblings = $(this).parent().siblings().find('ul');
					if (openMe.is(':visible')) 
					{
						openMe.slideUp('normal');  
					} 
					else 
					{
						mySiblings.slideUp('normal');  
						openMe.slideDown('normal');
					}
			    }
			);
			
		  $('#ulcssmenu li[id="total"] ul').slideDown('fast');
			
		}
	);
	  
	  
	  jQuery(function($){
 
  $('#start_dt').datepicker({dateFormat: 'yy-mm-dd',changeYear : true,changeMonth : true});
  $('#end_dt').datepicker({dateFormat: 'yy-mm-dd',changeYear : true,changeMonth : true});
 
  
});

</script>
<?php //include '../template/header.inc'; ?>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
	  <div class="breadcrumb">
			
	  </div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" />統計資料:: 訪客瀏覽統計</h1>
     
    </div>
     <div class="content">
			<div id="tab-general">
			<form action="./dwExcel/dwExcel_3.php" method="post" id="form_1" target="_blank">
				<table class="list">
				<tr>
					<td>起始時間</td>
					<td>結束時間</td>					
					<td align="left" width="150">功能操作</td>
				</tr>
				
				<tr>
					<td><input type="text" name="start_dt" id="start_dt" value="<?php echo date("Y-m-d",time());?>" class="datepicker"></td>
					<td><input type="text" name="end_dt" id="end_dt" value="<?php echo date("Y-m-d",time());?>" class="datepicker"></td>
					<td align="left">
					<a href="#" class="button" onclick="$('#form_1').submit()">匯出</a>					
					
					</td>
				</tr>
				
				</table>
			</form>
            </div>
					
    </div>
  </div>
</div>
</div>


<?php include("./layout/footer.php");?>
</body></html>