<?php
  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	session_start();
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	
	//解SQL Injection
	foreach($_GET as $key => $value){
		$_GET[$key] = decode_dowith_sql($value);
	}

	//解base64
	foreach($_GET as $key => $value)
	{
		$_GET[$key] = base64_decode($value);
	}

	//更新資料
	if ($_POST['send_data']=='HasPostValue') 
	{
		$up_dsc ="update `que_and_ans_type` set `c_name`='".decode_dowith_sql($_POST['c_name'])."' where `num`='".$_GET['num']."'";
		//echo $up_dsc;
		$res=$ODb->query($up_dsc) or die("更新資料出錯，請聯繫管理員。");
		ri_jump("c_que_and_ans_type.php?pg=".base64_encode($_POST['pg'])."&s=".base64_encode($_POST['s']));
	}
	
	//網頁傳參數用
	if($_GET['num'] !=''){
		
		$sql_dsc = "SELECT * FROM `que_and_ans_type` WHERE `num`='".$_GET['num']."'";
		$res=$ODb->query($sql_dsc) or die("載入資料出錯，請聯繫管理員。");
		while($row = mysql_fetch_array($res)){
			
			$user_array['num'] = $row['num'];
			$user_array['c_name'] = $row['c_name'];
			$user_array['up_date'] = $row['up_date'];
			
		}
	}else{
		ri_jump("c_que_and_ans_type.php");
	}	
?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>一般會員管理</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
	//-----------------------------------------
	// Confirm Actions (delete, uninstall)
	//-----------------------------------------
	$(document).ready
	(
		function()
		{
			
		  $('#ulcssmenu ul').hide();
		  
		  $('#ulcssmenu li a').click
		  (
				function() 
				{
					var openMe = $(this).next();
					var mySiblings = $(this).parent().siblings().find('ul');
					if (openMe.is(':visible')) 
					{
						openMe.slideUp('normal');  
					} 
					else 
					{
						mySiblings.slideUp('normal');  
						openMe.slideDown('normal');
					}
			    }
			);
			
		  $('#ulcssmenu li[id="que_and_ans"] ul').slideDown('fast');
			
		}
	);
	
	function ck_value()
	{
		$('#form').submit();
	}
</script>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
	  <div class="breadcrumb">
			 <a href="c_que_and_ans_type.php">分類管理</a> 
	  </div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" /> 關於Q&A::<a href="c_que_and_ans_type.php">分類管理</a></h1>
     <div class="buttons"><a onclick="ck_value()" class="button">存檔</a><a class="button" onclick="history.back();">取消</a></div>
    </div>
     <div class="content">
	    <form action="" method="post" enctype="multipart/form-data" id="form">
			<div id="tab-general">
				<table class="form">
				<tr>
					<td>類別名稱</td>
					<td><input type="text" name="c_name" id="c_name" value="<?php echo  $user_array['c_name']; ?>" ></td>
				</tr>
				</table>
			</div>
			<input type="hidden" name="send_data" value="HasPostValue">
		</form>	
    </div>
  </div>
</div>
</div>
<script language="javascript">
<?php
if($mg !=''){
echo 'alert("存檔完畢！！");';
}
?>
</script> 

<?php include("./layout/footer.php");?>
</body></html>