<?php
	session_start();

  //包含需求檔案 ------------------------------------------------------------------------
	include("./class/common_lite.php");
	if($_SESSION['zeroteamzero'] != 'IS_LOGIN'){
		ri_jump("login.php");
	}
	
 //宣告變數 ----------------------------------------------------------------------------
	$ODb = new run_db("mysql",3306);      //建立資料庫物件
	$upFileFload = "./upFile/".date("Ymd",time());
	$upFile = $upFileFload."/";
	
	if($_POST['send_data']=='HasPostValue' )
	{
		$nowdate =  date("Y-m-d H:i",time());		
		$up_dsc ="insert into `lack_type` set `c_name`='".$_POST['c_name']."',`up_date`='".$nowdate."' ,`c_order` ='".$_POST['c_order']."'";
		$res=$ODb->query($up_dsc) or die("更新資料出錯，請聯繫管理員。");
		ri_jump("c_lack_type.php");
	}	
?>

<!DOCTYPE html>
<html dir="ltr" lang="zh-TW">
<head>
<meta charset="UTF-8" />
<title>職缺消息</title>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
<script src="js/jquery/jquery-1.11.0.min.js"></script>

<script type="text/javascript">
	//-----------------------------------------
	// Confirm Actions (delete, uninstall)
	//-----------------------------------------
	$(document).ready
	(
		function()
		{
			
		  $('#ulcssmenu ul').hide();
		  
		  $('#ulcssmenu li a').click
		  (
				function() 
				{
					var openMe = $(this).next();
					var mySiblings = $(this).parent().siblings().find('ul');
					if (openMe.is(':visible')) 
					{
						openMe.slideUp('normal');  
					} 
					else 
					{
						mySiblings.slideUp('normal');  
						openMe.slideDown('normal');
					}
			    }
			);
			
		  $('#ulcssmenu li[id="job"] ul').slideDown('fast');
			
		}
	);
	
	var nowFileNum=2;

function ck_value(){
var isGo = true;
var err_dsc = '';
var ck_array   =  ["c_name","c_order"];
var err_array  =  ["請輸入職缺類別名稱!!","請輸入數字"];
var type_array =  ["text","number"];

for(var x=0;x< ck_array.length;x++){
	switch(type_array[x]){
		case "ckedit":
				var dsc =encodeURI(editor.document.getBody().getText());
				if(dsc == '%0A'){//cdedit 無輸入時自動會補入%0A
					err_dsc = err_dsc + err_array[x] +'\r\n';
					isGo = false;
				}
		break;
		case "text":
			if($('#'+ck_array[x]).val() ==''){
			err_dsc = err_dsc + err_array[x] +'\r\n';
			isGo = false;
			}
		break;
		case "number":
			if(!$.isNumeric($('#'+ck_array[x]).val()) ){
				err_dsc = err_dsc + err_array[x] +'\r\n';
				isGo = false;				
			}		
		break;
	}
}	
	if(isGo){
		$('#form').submit();
	}
	
	if(err_dsc !=''){
		alert(err_dsc);
	}
}

//取消input 直接按Enter時就直接送出表單的功能
$(function() {
    $("#form input").keypress(function(event){       
        if (event.keyCode == 13) return false;
    });
});
</script>
</head>
<body>

<?php include 'layout/head.php' ?>
<div id="container">
<?php
include('layout/menu_left.php');//載入左邊選單
?> 
  <div id="content">
	  <div class="breadcrumb">
			 <a href="c_lack_type.php">職缺類別</a> 
	  </div>
    <div class="box">
    <div class="heading">
      <h1><img src="image/category.png" alt="" />職缺消息::<a href="c_lack_type.php">職缺類別</a></h1>
     <div class="buttons"><a onclick="ck_value()" class="button">存檔</a><a class="button" onclick="history.back();">取消</a></div>
    </div>
     <div class="content">
	    <form action="c_lack_type_a.php" method="post" enctype="multipart/form-data" id="form">
			<div id="tab-general">
				<table class="form">
				<tr>
					<td>排序</td>
					<td>
						<input type="text" name="c_order" id="c_order" size="100">						
					</td>
				</tr>
				<tr>
					<td>職缺類別名稱</td>
					<td>
						<input type="text" name="c_name" id="c_name" size="100">						
					</td>
				</tr>				
				</table>
			</div>
			<input type="hidden" name="send_data" value="HasPostValue">
		</form>
    </div>
  </div>
</div>
</div>
<?php include("./layout/footer.php");?>
</body></html>